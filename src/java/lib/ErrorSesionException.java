/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author root
 */
public class ErrorSesionException extends Exception {
    private static final long serialVersionUID = -1307493420921168255L;
    
    public ErrorSesionException(String message){
        super(message); 
    }
}
