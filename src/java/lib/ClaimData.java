/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author Daniel Ramos Limón
 */
public class ClaimData {
    
    private String ide; 
    private String email; 
    private String grants; 

    public ClaimData(String ide, String email, String grants) {
        this.ide = ide;
        this.email = email;
        this.grants = grants;
    }

    /**
     * @return the ide
     */
    public String getIde() {
        return ide;
    }

    /**
     * @param ide the ide to set
     */
    public void setIde(String ide) {
        this.ide = ide;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the grants
     */
    public String getGrants() {
        return grants;
    }

    /**
     * @param grants the grants to set
     */
    public void setGrants(String grants) {
        this.grants = grants;
    }

    
    
}
