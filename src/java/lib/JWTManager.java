/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Daniel Ramos Limón
 */

public class JWTManager {
    String secret = "3st53!23#";
   //https://github.com/auth0/java-jwt
    public String create_token(String idUsr, String email, String grants){
    String token="";    
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            token = JWT.create()
                .withClaim("id", idUsr)
                .withClaim("email", email)
                .withClaim("grants", grants)
                .withIssuer("auth0")
                .sign(algorithm);
        } catch (JWTCreationException ex){
            System.out.println(ex.getMessage());
        } catch (UnsupportedEncodingException ex){ 
            System.out.println(ex.getMessage());
        }
     return token;   
    }
    
    
    public boolean verifica_token(String token){
    boolean r = false;
    try {
    Algorithm algorithm = Algorithm.HMAC256(secret);
    JWTVerifier verifier = JWT.require(algorithm)
        .withIssuer("auth0")
        .build(); //Reusable verifier instance
    DecodedJWT jwt = verifier.verify(token);
    r = true;
    } catch (JWTVerificationException ex){
    System.out.println("JWTVerification:"+ex.getMessage());
    } catch (IllegalArgumentException ex){ 
    System.out.println("IllegalArgument:"+ex.getMessage());
    } catch (UnsupportedEncodingException ex){
    System.out.println("UnsupportedEncoding:"+ex.getMessage());
    }
    return true;
    }
}
