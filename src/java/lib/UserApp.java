/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author Daniel Ramos Limón
 */
public class UserApp {

    private String id; 
    private String idAvatar; 
    private String idApp; 
    private String idPerfil; 
    private String nombreUsuario; 
    private String apellidoUsuario; 
    private String fechaIngreso; 
    private String usuarioIngreso; 

    public UserApp(String id, String idAvatar, String idApp, String idPerfil, String nombreUsuario, String apellidoUsuario, String fechaIngreso, String usuarioIngreso) {
        this.id = id;
        this.idAvatar = idAvatar;
        this.idApp = idApp;
        this.idPerfil = idPerfil;
        this.nombreUsuario = nombreUsuario;
        this.apellidoUsuario = apellidoUsuario;
        this.fechaIngreso = fechaIngreso;
        this.usuarioIngreso = usuarioIngreso;
    }

    /**
     * @return the idAvatar
     */
    public String getIdAvatar() {
        return idAvatar;
    }

    /**
     * @param idAvatar the idAvatar to set
     */
    public void setIdAvatar(String idAvatar) {
        this.idAvatar = idAvatar;
    }

    /**
     * @return the idApp
     */
    public String getIdApp() {
        return idApp;
    }

    /**
     * @param idApp the idApp to set
     */
    public void setIdApp(String idApp) {
        this.idApp = idApp;
    }

    /**
     * @return the idPerfil
     */
    public String getIdPerfil() {
        return idPerfil;
    }

    /**
     * @param idPerfil the idPerfil to set
     */
    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    /**
     * @return the nombreUsuario
     */
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    /**
     * @param nombreUsuario the nombreUsuario to set
     */
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    /**
     * @return the apellidoUsuario
     */
    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    /**
     * @param apellidoUsuario the apellidoUsuario to set
     */
    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    /**
     * @return the fechaIngreso
     */
    public String getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * @param fechaIngreso the fechaIngreso to set
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * @return the usuarioIngreso
     */
    public String getUsuarioIngreso() {
        return usuarioIngreso;
    }

    /**
     * @param usuarioIngreso the usuarioIngreso to set
     */
    public void setUsuarioIngreso(String usuarioIngreso) {
        this.usuarioIngreso = usuarioIngreso;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    
    
    
}
