/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import mysql.mysql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import sentencias.Propiedades;
import sentencias.sqls;

/**
 *
 * @author root
 */
public class envia_mail_recovery {
    Properties props;
    String username = "desarrolloert@gmail.com";
    String password = "Desarrolloert1";
    String from = "no-responder@wfapp.com.mx"; 
    String to = "";
    String subject = "Solicitud de clave de acceso";
    JSONObject resultado = new JSONObject();
    
    mysql _msql = new mysql(); 
    sqls _sql = new sqls(); 
    
    sentencias.Propiedades _prop = new Propiedades();
    
    
    public JSONObject inicializa2(String new_pass, String mail, String usuario){ 
        JSONObject respuesta = new JSONObject(); 
        JSONObject param = new JSONObject(); 
        param.put("1", "1"); 
        JSONObject resp1 = _msql.select(_prop.getCommand("39").comando(), param); 
        JSONArray datos = (JSONArray) resp1.get("data");
        JSONObject elemento = (JSONObject) datos.get(0); 
        String mensaje_mail = elemento.get("mensaje").toString(); 
        
        String host = "smtp.gmail.com";
        String port = "465";
        String mailFrom = mail;
        
        String[] inlineids = {"01","11","21","31","41","51","61","71","81","91"}; 
        
        
        // inline images
        Map<String, String> inlineImages = new HashMap<String, String>();
        
        Document doc = Jsoup.parse(mensaje_mail);
        Elements ele = doc.select("img");
        for (int i=0; i<ele.size(); i++){
            String nombreImagen = ele.get(i).attr("data-filename"); 
            int number = i+1;
//            ele.get(i).attr("src","cid:image0"+ String.valueOf(number) );
            ele.get(i).attr("src","cid:image"+ inlineids[i] );
//            inlineImages.put("image"+i+1,"/home/daniel/storeimg/email/"+nombreImagen);
            inlineImages.put("image"+i+1,"/home/drlimon/storeimg/email/"+nombreImagen);
        }
        ele.removeAttr("data-filename");
        mensaje_mail = doc.toString();
        mensaje_mail =  mensaje_mail.replace("$[usuario]", usuario); 
        mensaje_mail =  mensaje_mail.replace("$[nuevo_password]", new_pass); 
        
        
        try {
            respuesta = EmbeddedImageEmailUtil.send(host, port, mailFrom, password, mail, subject, mensaje_mail, inlineImages);
            System.out.println("Email sent.");
        } catch (Exception ex) {
            System.out.println("Could not send email.");
            ex.printStackTrace();
        }
        return respuesta; 
    }
    
    
    public JSONObject inicializa(String new_pass, String mail, String usuario){ 
        props = System.getProperties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        
        JSONObject param = new JSONObject(); 
        param.put("1", "1"); 
       JSONObject resp1 = _msql.select(_prop.getCommand("39").comando(), param); 
        JSONArray datos = (JSONArray) resp1.get("data");
        JSONObject elemento = (JSONObject) datos.get(0); 
        String mensaje_mail = elemento.get("mensaje").toString(); 
        
        //preparar la cadena de email! 
        Document doc = Jsoup.parse(mensaje_mail);
        Elements ele = doc.select("img");
        ArrayList imagenes = new ArrayList(); 
        for (int i=0; i<ele.size(); i++){
            String nombreImagen = ele.get(i).attr("data-filename"); 
            ele.get(i).attr("src","cid:"+nombreImagen);
            imagenes.add(nombreImagen); 
        }
        mensaje_mail = doc.toString();
        
        mensaje_mail =  mensaje_mail.replace("$[usuario]", usuario); 
        mensaje_mail =  mensaje_mail.replace("$[nuevo_acceso]", new_pass); 
        
        JSONObject r = enviar(mensaje_mail,mail, imagenes); 
        return r;
        
    }
    
        public JSONObject enviar(String mensaje_email, String mail, ArrayList imagenes){ 
           
        try { 
            
            Session emailSession = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username,password); //usuario y contraseña de correo
                    }
            });
 
            emailSession.setDebug(true);

            MimeMessage message = new MimeMessage(emailSession);
            message.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(mail)};
            message.setRecipients(Message.RecipientType.TO, address);
            message.setSubject(subject);
            message.setSentDate(new Date());
            
            MimeMultipart multipart = new MimeMultipart();
            MimeBodyPart mbp = new MimeBodyPart(); 
            mbp.setContent(mensaje_email,"text/html");
            
            MimeBodyPart mbpa = new MimeBodyPart(); 
            
            try { 
                for (int x=0;x<imagenes.size();x++){
                String namef = "/home/daniel/storeimg/email/"+imagenes.get(x).toString();
                mbpa.attachFile(namef);
                }
            } catch(Exception e){
                System.out.println(e.getMessage());
            }
            
            
            multipart.addBodyPart(mbp); 
            multipart.addBodyPart(mbpa); 
            message.setContent(multipart); 

            Transport transport = emailSession.getTransport("smtps");
            transport.connect("smtp.gmail.com", username, password);
            transport.sendMessage(message, message.getAllRecipients());
            
            
            
            resultado.put("mensaje", "El Código de Autentificación ha sido enviado al correo registrado a este usuario");
            resultado.put("code", "1");
            
            } catch(MessagingException ex){
                System.out.println(ex.getMessage());
                resultado.put("mensaje", ex.getMessage() + ", causa: " + ex.getCause());
                resultado.put("code", "2");
            }    
            
            return  resultado; 
        }
}
