/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.io.IOException;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Daniel Ramos Limón
 */
public class PreviewFiles {
    
    public String convertFile(String filename){ 
        String path = "C:\\almacen\\"; 
        String patht = "C:\\almacen\\temporal\\"; 
        int respuesta; 
        String ext = FilenameUtils.getExtension(filename);
        String namef = FilenameUtils.removeExtension(filename); 
        
        String _ext = ext.toLowerCase(); 
        String command; 
        
        switch (_ext) {
            case "doc":
                command = "soffice --headless --convert-to pdf --outdir " + patht + " " + path + filename;
                respuesta = runCommand(command);
                break;
            case "docx":
                command = "soffice --headless --convert-to pdf --outdir " + patht + " " + path + filename;
                respuesta = runCommand(command);
                break;
            case "odt":
                command = "soffice --headless --convert-to pdf --outdir " + patht + " " + path + filename;
                respuesta = runCommand(command);
                break;
            case "xls":
                command = "scalc --headless --convert-to pdf --outdir " + patht + " " + path + filename;
                respuesta = runCommand(command);
                break;
            case "xlsx":
                command = "scalc --headless --convert-to pdf --outdir " + patht + " " + path + filename;
                respuesta = runCommand(command);
                break;
            case "ppt":
                command = "simpress --headless --convert-to pdf --outdir " + patht + " " + path + filename;
                respuesta = runCommand(command); 
                break;
            case "pptx":
                command = "simpress --headless --convert-to pdf --outdir " + patht + " " + path + filename;
                respuesta = runCommand(command);
                break;
            default:
                respuesta = -100;
                break;
        }
        return namef + ".pdf";
    }; 
    
    
    private int runCommand(String command) {
    int returnValue = -1;
    try {
        Process process = Runtime.getRuntime().exec( command );
        process.waitFor();
        returnValue = process.exitValue();
    } catch (InterruptedException e) {
        System.out.println(e.getMessage());
    } catch (IOException e) { 
        System.out.println(e.getMessage());
    }
    return returnValue;
}
    
    
}
