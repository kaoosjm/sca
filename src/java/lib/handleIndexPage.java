/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import mysql.mysql;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import sentencias.Propiedades;

/**
 *
 * @author Daniel Ramos Limón
 */
public class handleIndexPage {
        //----------------------------------------------
        JSONObject params = new JSONObject(); 
        JSONObject datasql;
        sentencias.Propiedades prop = new Propiedades();
        JWTManager jwm = new JWTManager();
        mysql db = new mysql();
        //----------------------------------------------
        
        public boolean isNotNullToken(String token){ 
          boolean r = false;
          if (token==null){
              r = false;
          } else { 
              r = true;
          }
          return r;
        };
        
        public boolean isSesInDB(String sesion){ 
          boolean respuesta=false;
          params.put("1", sesion);
          JSONObject r = db.select(prop.getCommand("106").comando(), params);
          JSONArray d = (JSONArray) r.get("data"); 
          JSONObject row = (JSONObject) d.get(0); 
          int cuantos = Integer.parseInt(row.get("cuantos").toString());
          if (cuantos==0){
              respuesta = false;
          } else { 
              respuesta = true;
          }
          return respuesta;
        };
        
        public void agregaSes(String ses){
            params.put("1",ses);
            JSONObject r = db.update(prop.getCommand("104").comando(), params);
        }
        
        public UserApp loadUserData(String idUsr){
            params.put("1", idUsr);
            JSONObject r = db.select(prop.getCommand("103").comando(), params);
            JSONArray d = (JSONArray) r.get("data");    
            JSONObject row = (JSONObject) d.get(0); 
            //id, email, id_perfil, id_app, id_avatar, nombre, apellidos
            String id = row.get("id").toString();
            String idPerfil = row.get("id_perfil").toString();
            String idApp = row.get("id_app").toString();
            String idAvatar = row.get("id_avatar").toString();
            String nombreUsuario = row.get("nombre").toString();
            String apellidoUsuario = row.get("apellidos").toString();
            String fechaIngreso = row.get("fecha_ingreso").toString();
            String usuarioIngreso = row.get("usuario_ingreso").toString();
            UserApp uap = new UserApp(id, idAvatar, idApp, idPerfil, nombreUsuario, apellidoUsuario, fechaIngreso, usuarioIngreso);
            return uap;
        }
        
        public boolean isValidToken(String token){
          return jwm.verifica_token(token);
        }
        
        public boolean isExpiredToken(){
          boolean respuesta=false;
          return respuesta;
        }
        
        public String createToken(){
            String tkn="";
            return tkn;
        }
        
    }
