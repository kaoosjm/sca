/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;

/**
 *
 * @author root
 */
public class utiles {
    
    
    public String obtenTableSentencia(String[] partes){ 
       String respuesta=""; 
       // select id,jkjds,jkdjskd from tablename ..... 
       for (int xc=0; xc < partes.length;  xc++){
           String item = partes[xc].toLowerCase(); 
           if (item.contains("from")){ 
               respuesta = partes[xc+1]; 
               break; 
           }
       }
       return respuesta; 
    }; 
    
    public String obtenTableFiltro(String[] partes){ 
       String respuesta=""; 
       // select id,jkjds,jkdjskd from tablename ..... 
       for (int xc=0; xc < partes.length;  xc++){
           String item = partes[xc].toLowerCase(); 
           if (item.contains("where")){ 
               String cad="";
               respuesta = partes[xc+1]; 
               int xi=0;
               for (int s=(xc+1); s<partes.length; s++) {
                   if (xi==0){
                    cad += partes[s];
                   } else { 
                    cad += " " + partes[s];
                   }
                   xi++;
               }
               respuesta=cad;
           }
       }
       return respuesta; 
    }; 
    
    
    public String inyectaParametro(JSONObject params, String cadena){
      String cad="";
        Set ind = params.keySet();
        Iterator<?> indices = ind.iterator();
        while (indices.hasNext()) {
            String key = (String) indices.next();
            cad = cadena.replace("?",params.get(key).toString());
        }
      return cad; 
    };
    
    
    public ArrayList obtenTableSentenciaL(String[] partes){ 
       ArrayList respuesta = new ArrayList();
       // select id,jkjds,jkdjskd from tablename ..... 
       for (int xc=0; xc < partes.length;  xc++){
           String item = partes[xc].toLowerCase(); 
           if (item.contains("from")){ 
               String cad="";
               int xi=0;
               for (int s=(xc+3); s<partes.length; s++) {
                   if (xi==0){
                    cad += partes[s];
                   } else { 
                    cad += " " + partes[s];
                   }
                   xi++;
               }
               
               respuesta.add(partes[xc+1]);
               respuesta.add(cad);
               
               break; 
           }
       }
       return respuesta; 
    }; 
    
    public void convertImage64ToFile(String imageEncode64, String path, String nameFile){ 
        String imageDataBytes = imageEncode64.substring(imageEncode64.indexOf(",")+1);
        byte[] cadbyte = Base64.decodeBase64(imageDataBytes);
        try { 
        File imgfile = new File(path, nameFile);
        BufferedImage img = ImageIO.read(new ByteArrayInputStream(cadbyte));
        
        String fnam = imgfile.getName(); 
        
        if (fnam.contains(".jpg")){
        ImageIO.write(img,"jpeg", imgfile); 
            
        } else if (fnam.contains(".jpeg")){
        ImageIO.write(img,"jpeg", imgfile); 
            
        } else if (fnam.contains(".png")){
        ImageIO.write(img,"png", imgfile); 
            
        } else if (fnam.contains(".bmp")){
        ImageIO.write(img,"bmp", imgfile); 
            
        }
        
        
        } catch(Exception e) { 
            System.out.println(e.getMessage());
        }
    }
}
