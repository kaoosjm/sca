/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author OPR 2020
 */
public class ErrorExisteSesionException extends Exception{
    private static final long serialVersionUID = -1407493420921168255L;
    
    public ErrorExisteSesionException(String message){
        super(message); 
    }
    
}
