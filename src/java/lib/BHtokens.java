/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.simple.JSONObject;
/**
 *
 * @author daniel.ramos
 */
public class BHtokens {
    final String secretKey = "$!#!";
    static String  cadena = "daniel";
    long periodo = 60;
    private static final String ALGO = "AES";
    
    public String generaToken(String id_empleado, String sess) {
        DateTime dt = new DateTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd-HH-mm-ss");
        String fechac = fmt.print(dt);
        String respuesta="";
        JSONObject tkn = new JSONObject();
        AES aes = new AES();
        
        try {
            tkn.put("id", id_empleado);
//            tkn.put("id_perfil", id_perfil);
//            tkn.put("id_app", id_app);
//            tkn.put("nickname", nickname);
//            tkn.put("avatar", id_avatar);
            tkn.put("token", aes.encrypt(fechac+"-"+sess, secretKey));
//            tkn.put("token", aes.encrypt(fechac, secretKey));
            respuesta = encrypt64(tkn.toJSONString());
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return respuesta;
    }

    public String encrypt64(String valor){
        byte[] encodedBytes = Base64.encodeBase64(valor.getBytes());
        return new String(encodedBytes);
    }
    
    public String decript64(String valor){
        byte[] decodedBytes = Base64.decodeBase64(valor);
        return new String(decodedBytes);
    }
    
    public boolean validaToken(String token, String ses_env) {
        boolean respuesta = false;
        AES aes = new AES();
        try {
            String dectkn = aes.decrypt(token, secretKey);
            String[] parts = dectkn.split("-");
            respuesta = validaFecha(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5],parts[6], ses_env);
        } catch (Exception ex ) { 
        }
        return respuesta;
    }

    private boolean validaFecha(String anio, String mes, String dia, String hora, String min, String seg,String ses_tkn, String ses_env) {
        boolean respuesta = false;
        int an = Integer.valueOf(anio);
        int me = Integer.valueOf(mes);
        int di = Integer.valueOf(dia);
        int ho = Integer.valueOf(hora);
        int mi = Integer.valueOf(min);
        int se = Integer.valueOf(seg);
        DateTime dt_now = new DateTime();
        DateTime dt_ref = new DateTime(an, me, di, ho, mi, se);
        long diferencia = dt_now.getMillis() - dt_ref.getMillis();
        long segundos = diferencia / 10000;
        int sss = (int) segundos;
        if (periodo > sss && ses_tkn.equals(ses_env) ) {
            respuesta = true;
        } else {
            respuesta = false;
        }
        return respuesta;
    }
}
