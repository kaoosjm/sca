/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysql;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author drlimon
 */
public class utiles { 
        public JSONArray tieneHijos(JSONArray datos, int indice){ 
        JSONArray respuesta = new JSONArray(); 
        for (int ax=0; ax< datos.size(); ax++){
            JSONObject element = (JSONObject) datos.get(ax); 
            int _id_padre = Integer.parseInt(element.get("id_padre").toString());
            String item = element.get("item").toString();
            String icon = element.get("icon").toString();
            String code = element.get("code").toString();
            
            if (_id_padre==indice){
                JSONObject r = new JSONObject(); 
                r.put("item", item);
                r.put("icon", icon);
                r.put("code", code);
                respuesta.add(r);
            }
        }
        return respuesta;     
        };
        
        
    }