/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Set;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import sentencias.Propiedades;
import sentencias.sqls;

/**
 *
 * @author daniel
 */
public class mysql {
    
    String jdbc01 = "jdbc/appbasesca";
    sqls _sqls = new sqls();
    sentencias.Propiedades _prop = new Propiedades();
    
    
    public Connection getConnection(){
        
        
        java.sql.Connection conn = null;
        java.sql.PreparedStatement pst = null;
        java.sql.Statement st = null;
        java.sql.ResultSet rs = null;
        
        try {
            
        InitialContext cxt = new InitialContext();
        DataSource ds = (DataSource) cxt.lookup(jdbc01);
        conn = ds.getConnection();
        st = conn.createStatement();
        
        } catch (SQLException | NamingException ex){
            System.out.println("Error");
        }
        return conn;
    }
    
    public Connection getConnectionDB() throws NamingException, SQLException {
        InitialContext cxt = new InitialContext();
        DataSource ds = (DataSource) cxt.lookup(jdbc01);
        java.sql.Connection conn;
        conn = ds.getConnection();
        return conn;
    }
    
    public JSONObject select(String sentencia, JSONObject parametros) {
        JSONArray respuesta = new JSONArray();
        JSONObject paquete = new JSONObject();
        Context initContext;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup(jdbc01);
            cn = ds.getConnection();

            PreparedStatement pst = null;
            pst = cn.prepareStatement(sentencia);

            if (parametros == null || parametros.isEmpty() == true) {
                //no hay parametros...
            } else {
                // si existen los parametros los inyecta

                Set ind = parametros.keySet();
                Iterator<?> indices = ind.iterator();
                int indice = 1;
                while (indices.hasNext()) {
                    String key = (String) indices.next();
                    pst.setString(indice, (String) parametros.get(key));
                    indice++;
                }
            }
            rs = pst.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            for (; rs.next();) {
                JSONObject registro = new JSONObject();
                for (int g = 0; g < rsmd.getColumnCount(); g++) {
                    String val = "";
                    if (rs.getString(g + 1) == null) {
                        val = "";
                    } else {
                        val = rs.getString(g + 1);
                    }
                    registro.put(rsmd.getColumnLabel(g + 1), val);
                }
                respuesta.add(registro);
            }
            paquete.put("error_msg", "");
            paquete.put("error_cod", "0");
            paquete.put("error_cau", "");
            paquete.put("data", respuesta);
        } catch (SQLException ex) {
            paquete = new JSONObject();
            paquete.put("error_msg", ex.getMessage());
            paquete.put("error_cod", ex.getErrorCode());
            paquete.put("error_cau", ex.getCause());
            paquete.put("data", null);
        } catch (NamingException ex) {
            
            paquete = new JSONObject();
            paquete.put("error_msg", ex.getMessage());
            paquete.put("error_cod", "");
            paquete.put("error_cau", ex.getCause());
            paquete.put("data", null);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {

                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
        }
        return paquete;
    };
    
    
    public JSONObject select(String sentencia) {
        JSONArray respuesta = new JSONArray();
        JSONObject paquete = new JSONObject();
        Context initContext;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup(jdbc01);
            cn = ds.getConnection();

            PreparedStatement pst = null;
            pst = cn.prepareStatement(sentencia);
            rs = pst.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            for (; rs.next();) {
                JSONObject registro = new JSONObject();
                for (int g = 0; g < rsmd.getColumnCount(); g++) {
                    String val = "";
                    if (rs.getString(g + 1) == null) {
                        val = "";
                    } else {
                        val = rs.getString(g + 1);
                    }
                    registro.put(rsmd.getColumnLabel(g + 1), val);
                }
                respuesta.add(registro);
            }
            paquete.put("error_msg", "");
            paquete.put("error_cod", "0");
            paquete.put("error_cau", "");
            paquete.put("data", respuesta);
        } catch (SQLException ex) {
            paquete = new JSONObject();
            paquete.put("error_msg", ex.getMessage());
            paquete.put("error_cod", ex.getErrorCode());
            paquete.put("error_cau", ex.getCause());
            paquete.put("data", null);
        } catch (NamingException ex) {
            
            paquete = new JSONObject();
            paquete.put("error_msg", ex.getMessage());
            paquete.put("error_cod", "");
            paquete.put("error_cau", ex.getCause());
            paquete.put("data", null);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {

                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
        }
        return paquete;
    };
    
    
    
    
    public JSONObject update_batch(JSONArray parametros, String sentencia_batch){
      JSONArray respuesta = new JSONArray(); 
      JSONObject paquete = new JSONObject(); 
      Context initContext;
      Connection cn = null;
      Statement st = null;
      ResultSet rs = null;
      try {
            initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup(jdbc01);
            cn = ds.getConnection();
            PreparedStatement pst = null;
            pst = cn.prepareStatement(sentencia_batch);
            cn.setAutoCommit(false);
            for (int xf = 0 ; xf < parametros.size(); xf++){
                JSONObject elemento_parametro = (JSONObject) parametros.get(xf);
                
                if (parametros == null || parametros.isEmpty() == true) {
                    //no hay parametros...
                } else {
                    // si existen los parametros los inyecta
                    Set ind = elemento_parametro.keySet();
                    Iterator<?> indices = ind.iterator();
                    int indice = 1;
                    while (indices.hasNext()) {
                        String key = (String) indices.next();
                        pst.setString(indice, (String) elemento_parametro.get(key));
                        indice++;
                    }
                }
                pst.addBatch();
            }
            int resultado_update[]  = pst.executeBatch();
            String resultado_cadena = ""; 
            for (int ad=0;ad<resultado_update.length;ad++){
                resultado_cadena+=resultado_update[ad];
            }
            cn.commit();
            JSONObject registro = new JSONObject(); 
            registro.put("resultado_update",resultado_cadena);
            respuesta.add(registro);
            paquete.put("error_msg", "");
            paquete.put("error_cod", "0");
            paquete.put("error_cau", "");
            paquete.put("data", respuesta);
        } catch (SQLException ex) {
            paquete = new JSONObject();
            paquete.put("error_msg", ex.getMessage());
            paquete.put("error_cod", ex.getErrorCode());
            paquete.put("error_cau", ex.getCause());
            paquete.put("data", null);
        } catch (NamingException ex) {
            paquete = new JSONObject();
            paquete.put("error_msg", ex.getMessage());
            paquete.put("error_cod", "");
            paquete.put("error_cau", ex.getCause());
            paquete.put("data", null);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                    
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                    
                }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                    
                }
            }
        }
      return paquete;
    };
    public JSONObject update(String sentencia, JSONObject parametros) {
        JSONArray respuesta = new JSONArray();
        JSONObject paquete = new JSONObject();
        Context initContext;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup(jdbc01);
            cn = ds.getConnection();
            PreparedStatement pst = null;
            pst = cn.prepareStatement(sentencia, Statement.RETURN_GENERATED_KEYS);
            if (parametros == null || parametros.isEmpty() == true) {
                //no hay parametros...
            } else {
                // si existen los parametros los inyecta
                Set ind = parametros.keySet();
                Iterator<?> indices = ind.iterator();
                int indice = 1;
                while (indices.hasNext()) {
                    String key = (String) indices.next();
                    pst.setString(indice, (String) parametros.get(key));
                    indice++;
                }
            }
            int resultado_update = pst.executeUpdate(); 
            ResultSet genk = pst.getGeneratedKeys();
            int genkey=0;
            for (;genk.next();){
                genkey += genk.getInt(1);
            }
            JSONObject registro = new JSONObject(); 
            registro.put("resultado_update", resultado_update);
            respuesta.add(registro);
            paquete.put("error_msg", "");
            paquete.put("error_cod", "0");
            paquete.put("error_cau", "");
            paquete.put("key_generado", genkey);
            paquete.put("data", respuesta);

        } catch (SQLException ex) {
            
            paquete = new JSONObject();
            paquete.put("error_msg", ex.getMessage());
            paquete.put("error_cod", ex.getErrorCode());
            paquete.put("error_cau", ex.getCause());
            paquete.put("data", null);
            
        } catch (NamingException ex) {
            
            paquete = new JSONObject();
            paquete.put("error_msg", ex.getMessage());
            paquete.put("error_cod", "");
            paquete.put("error_cau", ex.getCause());
            paquete.put("data", null);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException ex) {
                    paquete = new JSONObject();
                    paquete.put("error_msg", ex.getMessage());
                    paquete.put("error_cod", ex.getErrorCode());
                    paquete.put("error_cau", ex.getCause());
                    paquete.put("data", null);
                }
            }
        }
        return paquete;
    };
    
    // funciones particulares
    
    
    
    public String obtenMenuApp(){
    sentencias.sqls _sql = new sqls();
    String consulta = _prop.getCommand("40").comando();
    JSONObject respuesta; 
    utiles u = new utiles(); 
    String c="";
    respuesta = select(consulta, null);
    JSONArray datos = (JSONArray) respuesta.get("data"); 
    String cadena=""; 
    String withsub=""; 
    
    for (int a=0; a< datos.size(); a++){
        JSONObject dat = (JSONObject) datos.get(a); 
        String item = dat.get("item").toString();
        String code = dat.get("code").toString();
        String icon = dat.get("icon").toString();
        String id = dat.get("id").toString();
        String visible = dat.get("visible").toString();
        String id_padre = dat.get("id_padre").toString();
        int _id = Integer.parseInt(id);
        int _id_padre = Integer.parseInt(id_padre);
        int _visible= Integer.parseInt(visible);
        
        if (_id_padre==0){ 
        JSONArray elementos = u.tieneHijos(datos, _id);
        cadena = ""; 
        withsub = ""; 
        if (elementos.size()>0){
            cadena = "<ul class='br-menu-sub'>"; 
            for (int hf=0; hf<elementos.size(); hf++){ 
                JSONObject d = (JSONObject) elementos.get(hf);
                String ite = d.get("item").toString();
                String ico = d.get("icon").toString();
                String cod = d.get("code").toString();
                cadena += "<li data-item="+ cod +" class='sub-item itemmnu'><a  href='javascript:void(0)' class='sub-link'>"+ite+"</a></li>";
            }
            cadena+="</ul>";
            withsub = "with-sub"; 
        } 
        
        if (Integer.valueOf(id)==1){ 
            
        c += "<li class='br-menu-item'> <a id='panel' data-item='"+code +"' href='#' class='br-menu-link itemmnu"+ withsub +"'><i class='menu-item-icon "+ icon + " tx-20'></i>" +
            "<span class='menu-item-label'>" + item + "</span>" + 
            "</a>" + cadena + "</li>";
        
        } else { 
        
            
        if (_visible==1) {     
        c += "<li class='br-menu-item'> <a href='#' data-item='"+code+"' class='br-menu-link itemmnu"+ withsub +"'><i class='menu-item-icon "+ icon + " tx-20'></i>" +
            "<span class='menu-item-label'>" + item+ "</span>" + 
            "</a>" + cadena + "</li>";
        
        }
        }
        
        
        }
    }
    return c;
    }
    
    
}
