/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Daniel Ramos Limón
 */
public class sessionListener implements HttpSessionListener {
    
    
    
    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        System.out.println("sesion creada...");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        System.out.println("sesion destruida...");
    }
    
}
