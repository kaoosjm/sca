/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sentencias;

/**
 *
 * @author daniel
 */
public class estCommand {
    private String tipo; 
    private String comando; 

    public estCommand(String comando) {
        String[] temp = comando.split("\\|"); 
        this.tipo = temp[0];
        this.comando = temp[1];
    }
    
    public String tipo() { 
        return tipo;
    }
    
    public String comando() { 
        return comando;
    }
    
    
}
