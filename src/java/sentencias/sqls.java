/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sentencias;
import java.util.ArrayList;
/**
 *
 * @author daniel.ramos
 */
public class sqls {
    ArrayList<String> sentencias = new ArrayList(); 
    public sqls() {
        inicializa(); 
    }
    private void inicializa(){ 
        //aqui se dan de alta las sentencias! 
        sentencias.add("SELECT if (id is null, '', id ) as id, if (email is null, '', email) as email, if(id_perfil is null,'',id_perfil) as id_perfil, if(id_app is null,'',id_app) as id_app, count(*) as cuantos, id_avatar FROM app_usuarios WHERE email=? and password=? group by id"); //[0]
        sentencias.add("SELECT ruta1,ruta2 FROM app_rutas where codigo=?"); //[1]
        sentencias.add("SELECT*FROM app_menu where visible<>0 order by orden"); //[2]
        sentencias.add("SELECT*FROM app_menu_principal where visible<>0 order by orden"); //[3]
        
        
        sentencias.add("SELECT*FROM view_edo_mun WHERE id_estado=?"); //[4]
        sentencias.add("SELECT*FROM cat_embalaje where id_proveedor=?"); //[5]
        sentencias.add("SELECT*FROM cat_fabricante"); //[6]
        sentencias.add("DELETE FROM cat_marcas where id=?"); //[7] // revision de USO
        sentencias.add("SELECT*FROM cat_unidad_medida where id_proveedor=?"); //[8]
        sentencias.add("SELECT*FROM view_usuario_perfil"); //[9]
        sentencias.add("UPDATE app_usuarios SET email=?, id_perfil=?, nombre=?, apellidos=?, usuario_ingreso=? WHERE id=?"); //[10]
        sentencias.add("SELECT id, descripcion as valor FROM app_perfiles"); //[11]
        sentencias.add("SELECT * from view_cat_img"); //[12]
        sentencias.add("INSERT INTO app_imagenes SET id_tipo=?,nombre_archivo=?,nombre_sistema=?,checksum=?,fecha_ingreso=now(),usuario_ingreso=?, longitud=?"); //[13]
        sentencias.add("INSERT INTO cat_cat SET descripcion=?,img=?,fecha_ingreso=now(),usuario_ingreso=?"); //[14]
        sentencias.add("UPDATE cat_cat SET descripcion=?,img=?,fecha_ingreso=now(),usuario_ingreso=? where id=?"); //[15]
        sentencias.add("DELETE FROM cat_cat WHERE id=?"); //[16]
        sentencias.add("SELECT*FROM cat_subcat"); //[17]
        sentencias.add("SELECT id, descripcion as valor FROM cat_cat"); //[18]
        sentencias.add("SELECT*FROM view_subcat_img where id_cat=?"); //[19]
        sentencias.add("INSERT INTO cat_subcat SET id_cat=?,descripcion=?,img=?,fecha_ingreso=now(),usuario_ingreso=?"); //[20]
        sentencias.add("DELETE FROM cat_subcat WHERE id=?"); //[21]
        sentencias.add("UPDATE cat_subcat SET id_cat=?,descripcion=?,img=?,fecha_ingreso=now(),usuario_ingreso=? where id=?"); //[22]
        sentencias.add("INSERT INTO cat_unidad_medida SET id_proveedor=?, descripcion=?,fecha_ingreso=now(),usuario_ingreso=?"); //[23]
        sentencias.add("UPDATE cat_unidad_medida SET id_proveedor=?, descripcion=?, fecha_ingreso=now(),usuario_ingreso=? where id=?"); //[24]
        sentencias.add("DELETE FROM cat_unidad_medida WHERE id=?"); //[25]
        sentencias.add("INSERT INTO cat_fabricante SET denominacion=?,detalle=?,fecha_ingreso=now(),usuario_ingreso=?"); //[26]
        sentencias.add("DELETE FROM cat_fabricante WHERE id=?"); //[27]
        sentencias.add("UPDATE cat_fabricante SET denominacion=?, detalle=?, fecha_ingreso=now(),usuario_ingreso=? WHERE id=?"); //[28]
        sentencias.add("SELECT*FROM view_marca_fabricante where id_fabricante=?"); //[29]
        sentencias.add("SELECT id, denominacion as valor FROM cat_fabricante"); //[30]
        
        sentencias.add("INSERT INTO cat_marcas SET id_fabricante=?, denominacion=?, detalles=?, fecha_ingreso=now(),usuario_ingreso=?"); //[31]
        sentencias.add("UPDATE cat_marcas SET id_fabricante=?, denominacion=?, detalles=?, fecha_ingreso=now(),usuario_ingreso=? WHERE id=?"); //[32]
        
        sentencias.add("UPDATE cat_embalaje SET id_proveedor=?, presentacion=?, descripcion=?, fecha_ingreso=now(), usuario_ingreso=? WHERE id=?"); //[33]
        sentencias.add("INSERT INTO cat_embalaje SET id_proveedor=?, presentacion=?, descripcion=?, fecha_ingreso=now(), usuario_ingreso=?"); //[34]
        sentencias.add("DELETE FROM cat_embalaje WHERE id=?"); //[35]
        
        sentencias.add("UPDATE cat_estados SET denominacion=? WHERE id=?"); //[36]
        sentencias.add("INSERT INTO cat_estados SET denominacion=?"); //[37]
        sentencias.add("DELETE FROM cat_estados where id=?"); //[38]
        
        sentencias.add("UPDATE cat_municipios SET id_estado=?,denominacion=? WHERE id=?"); //[39]
        sentencias.add("INSERT INTO cat_municipios SET id_estado=?,denominacion=?"); //[40]
        sentencias.add("DELETE FROM cat_municipios WHERE id=?"); //[41]
        sentencias.add("SELECT id, denominacion as valor FROM cat_estados"); //[42]
        
        sentencias.add("UPDATE app_perfiles SET descripcion=?,fecha_ingreso=now(),usuario_ingreso=? WHERE id=?"); //[43]
        sentencias.add("INSERT INTO app_perfiles SET descripcion=?,fecha_ingreso=now(),usuario_ingreso=?"); //[44]
        sentencias.add("DELETE FROM app_perfiles WHERE id=?"); //[45]
        sentencias.add("SELECT*FROM app_perfiles"); //[46]
        
        sentencias.add("SELECT*FROM view_usuario_perfil"); //[47]
        
        sentencias.add("INSERT INTO app_usuarios SET email=?, id_perfil=?, nombre=?, apellidos=?, usuario_ingreso=?"); //[48]
        sentencias.add("DELETE FROM app_usuarios WHERE id=?"); //[49]
        sentencias.add("SELECT id, email, id_perfil, id_app, id_avatar,CONCAT(nombre,',',apellidos) as nombre_completo, nombre FROM app_usuarios where id=?"); //[50]
        
        sentencias.add("SELECT id, denominacion as valor FROM cat_municipios where id_estado=?"); //[51]
        sentencias.add("SELECT*FROM view_cp_mun_edo WHERE id_municipio=?"); //[52]
        
        sentencias.add("INSERT INTO cat_cod_postal SET id_municipio=?, cp=?"); //[53]
        sentencias.add("UPDATE cat_cod_postal SET id_municipio=?, cp=? WHERE id=?"); //[54]
        sentencias.add("DELETE FROM cat_cod_postal WHERE id=?"); //[55]
        
        sentencias.add("SELECT*FROM view_colonias WHERE id_codigo_postal=?"); //[56]
        sentencias.add("SELECT id, cp as valor FROM cat_cod_postal WHERE id_municipio=?"); //[57]
        sentencias.add("SELECT id, denominacion as valor FROM cat_tipo_asentamiento"); //[58]
        
        sentencias.add("INSERT INTO cat_asentamientos SET id_cp=?, tipo_asentamiento=?, denominacion=?"); //[59]
        sentencias.add("UPDATE cat_asentamientos SET id_cp=?, tipo_asentamiento=?, denominacion=? WHERE id=?"); //[60]
        sentencias.add("DELETE FROM cat_asentamientos WHERE id=?"); //[61]
        
        sentencias.add("SELECT*FROM view_colonias"); //[62]
        sentencias.add("select id, id_asentamiento, x, y from cat_puntos_asentamiento where id_asentamiento=?"); //[63]
        
        sentencias.add("select id, denominacion, rfc, nombre_contacto, email_contacto, estado from view_provedores"); //[64]
        sentencias.add("select id, denominacion as valor from cat_asentamientos where id_cp=?"); //[65]
        
        sentencias.add("INSERT INTO cat_instituciones SET id_tipo=2,denominacion=?,latitud=?,longitud=?,punto=GeometryFromText(CONCAT('POINT(',?,' ',?,')')),id_asentamiento=?,rfc=?,email_contacto=?,nombre_contacto=?,apellido_contacto=?,calle=?,num_exterior=?,num_interior=?,referencias=?,fecha_ingreso=now(),usuario_ingreso=?"); //[66]
        sentencias.add("UPDATE cat_instituciones SET denominacion=?,latitud=?,longitud=?,punto=GeometryFromText(CONCAT('POINT(',?,' ',?,')')),id_asentamiento=?,rfc=?,email_contacto=?,nombre_contacto=?,apellido_contacto=?,calle=?,num_exterior=?,num_interior=?,referencias=?,fecha_ingreso=now(),usuario_ingreso=? WHERE id=?"); //[67]
        sentencias.add("DELETE FROM cat_instituciones WHERE id=?"); //[68]
        
        sentencias.add("SELECT*FROM cat_instituciones WHERE id=?"); //[69]
        sentencias.add("SELECT*FROM view_inst_cp_mun_edo WHERE id=?"); //[70]
        
        
        sentencias.add("INSERT INTO cat_instituciones SET id_tipo=3,denominacion=?,latitud=?,longitud=?,punto=GeometryFromText(CONCAT('POINT(',?,' ',?,')')),id_asentamiento=?,rfc=?,email_contacto=?,nombre_contacto=?,apellido_contacto=?,calle=?,num_exterior=?,num_interior=?,referencias=?,fecha_ingreso=now(),usuario_ingreso=?"); //[71]
        sentencias.add("UPDATE cat_instituciones SET denominacion=?,latitud=?,longitud=?,punto=GeometryFromText(CONCAT('POINT(',?,' ',?,')')),id_asentamiento=?,rfc=?,email_contacto=?,nombre_contacto=?,apellido_contacto=?,calle=?,num_exterior=?,num_interior=?,referencias=?,fecha_ingreso=now(),usuario_ingreso=? WHERE id=?"); //[72]
        sentencias.add("DELETE FROM cat_instituciones WHERE id=?"); //[73]
        sentencias.add("SELECT id, denominacion, rfc, nombre_contacto, email_contacto from view_consumidores"); //[74]
        sentencias.add("SELECT id, denominacion as valor from view_provedores"); //[75]
        
        
        sentencias.add("SELECT id,desc_subcat,desc_catalogo,desc_producto,desc_embalaje,desc_umedida FROM view_productos_provedor where id_institucion=?"); //[76]
        sentencias.add("SELECT id, descripcion as valor from cat_subcat where id_cat = ?"); //[77]
        
        sentencias.add("INSERT INTO cat_productos SET id_institucion=?,id_subcat=?,descripcion=?,id_embalaje=?,id_unidad_medida=?,id_cate=?,precio=?,preciof=?,detalle=?,id_marca=?,codigo=?,peso=?,dimension_alto=?,dimension_ancho=?,dimension_profundidad=?,fecha_ingreso=now(),usuario_ingreso=?"); //[78]
        sentencias.add("UPDATE cat_productos SET id_institucion=?,id_subcat=?,descripcion=?,id_embalaje=?,id_unidad_medida=?,id_cate=?,precio=?,preciof=?,detalle=?,id_marca=?,codigo=?,peso=?,dimension_alto=?,dimension_ancho=?,dimension_profundidad=?,fecha_ingreso=now(),usuario_ingreso=? WHERE id=?"); //[79]
        sentencias.add("DELETE FROM cat_productos WHERE id=?"); //[80]
        
        sentencias.add("SELECT id, descripcion as valor FROM cat_embalaje"); //[81]
        sentencias.add("SELECT id, descripcion as valor FROM cat_unidad_medida"); //[82]
        sentencias.add("SELECT id, denominacion as valor FROM cat_fabricante"); //[83]
        
        sentencias.add("SELECT id, denominacion as valor FROM cat_marcas where id_fabricante = ?"); //[84]
        sentencias.add("SELECT*FROM view_productos_provedor WHERE id=?"); //[85]
        sentencias.add("INSERT INTO imagenes_producto SET id_imagen=?, id_producto=?, filename=?,fecha_ingreso=NOW(), usuario_ingreso=?"); //[86]
        sentencias.add("SELECT*FROM view_producto_imagen where id_producto=? order by fecha_ingreso"); //[87]
        sentencias.add("DELETE FROM imagenes_producto where id_producto=?"); //[88]
        sentencias.add("UPDATE app_usuarios SET password=? where email=?"); //[89]
        
        sentencias.add("SELECT count(*) as cuantos, nombre FROM app_usuarios where email=?"); //[90]
        
        sentencias.add("SELECT id, descripcion,mensaje from app_cmail"); //[91]
        sentencias.add("UPDATE app_cmail SET mensaje=? where id=?"); //[92]
        sentencias.add("SELECT mensaje from app_cmail where id=?"); //[93]
        sentencias.add("UPDATE app_usuarios SET id_avatar=? where id=?"); //[94]
        
        sentencias.add("SELECT count(*) as cuantos FROM app_usuarios where email=?"); //[95]
        sentencias.add("UPDATE app_usuarios SET email=? where id=?"); //[96]
        sentencias.add("UPDATE app_usuarios SET password=? where id=?"); //[97]
        sentencias.add("INSERT INTO app_sesiones SET sesion=?,fecha_ini=now(),id_usuario=?"); //[98]
        sentencias.add("SELECT count(*) as cuantos, if(estado is null,0,estado) as estado FROM app_sesiones where estado=0 and sesion=?"); //[99]
        sentencias.add("UPDATE app_sesiones set estado=1 where sesion=?"); //[100]
        sentencias.add("SELECT*FROM cat_areas_exc"); //[101]
        
        sentencias.add("INSERT INTO cat_areas_exc SET denominacion=?,fecha_ingreso=now(),usuario_ingreso=?"); //[102]
        sentencias.add("UPDATE cat_areas_exc SET denominacion=?,fecha_ingreso=now(),usuario_ingreso=? WHERE id=?"); //[103]
        sentencias.add("DELETE FROM cat_areas_exc WHERE id=?"); //[104]
        
        sentencias.add("INSERT INTO cat_puntos_area SET id_zona=?,lat=?,lng=?,punto=GeometryFromText(CONCAT('POINT(',?,' ',?,')')),fecha_ingreso=now(),id_usuario=?"); //[105]
        
        sentencias.add("SELECT lat,lng FROM cat_puntos_area where id_zona=?"); //[106]
        
        sentencias.add("DELETE FROM cat_puntos_area WHERE id=?"); //[107]
        
        sentencias.add("insert into app_usuarios (email,id_perfil,id_app,nombre,apellidos,fecha_ingreso,usuario_ingreso) select email_contacto, 1 as id_perfil, 2 as id_app, nombre_contacto, apellido_contacto,now(), usuario_ingreso from cat_instituciones where id=?"); //[108]
        sentencias.add("update cat_instituciones set estado=1 where id=?"); //[109]
        sentencias.add("select id, denominacion, rfc, nombre_contacto, email_contacto, estado from view_provedores_edo"); //[110]
        sentencias.add("select id, denominacion, rfc, nombre_contacto, email_contacto, estado from view_consumidores_edo"); //[111]
        sentencias.add("select*from view_pedidos"); //[112]
        sentencias.add("select b.codigo, b.desc_marca, b.detalle_producto, a.descripcion, sum(a.cantidad) as cantidad,sum(a.cantidad)*a.precio as total  from shop_pedidos a JOIN view_productos_provedor b on b.id=a.id_producto where a.id_pedido = ? group by a.id_producto"); //[113]
        sentencias.add("SELECT*FROM view_puntos_pedido"); //[114]
        sentencias.add("select id_zona from cat_puntos_area group by id_zona"); //[115]
        sentencias.add("select  lat,lng  from cat_puntos_area where id_zona=?"); //[116]
        sentencias.add("SELECT*FROM cat_categoria"); //[117]
        sentencias.add("INSERT INTO cat_categoria SET denominacion=?, fecha_ingreso=now(), id_usuario=?"); //[118]
        sentencias.add("UPDATE cat_categoria SET denominacion=?, fecha_ingreso=now(), id_usuario=? WHERE id=?"); //[119]
        sentencias.add("DELETE FROM cat_categoria  WHERE id=?"); //[120]
        sentencias.add("SELECT id, denominacion as valor FROM cat_categoria"); //[121]
        
        sentencias.add("SELECT count(*) as cuantos from cat_embalaje where presentacion =?"); //[122]
        sentencias.add("SELECT count(*) as cuantos from cat_embalaje where presentacion =? and id<>?"); //[123]
        
        sentencias.add("SELECT count(*) as cuantos from cat_instituciones where rfc = ?"); //[124]
        sentencias.add("SELECT count(*) as cuantos from cat_instituciones where email_contacto = ?"); //[125]
        
        sentencias.add("SELECT count(*) as cuantos from cat_instituciones where rfc = ? and id<>?"); //[126]
        sentencias.add("SELECT count(*) as cuantos from cat_instituciones where email_contacto = ? and id<>?"); //[127]
        ///////////////////////////////////////////////////////////////////////
        sentencias.add("SELECT*FROM cat_giros_ceda"); //[128]
        sentencias.add("INSERT INTO cat_giros_ceda SET id_sector=?, denominacion=?, fecha_ingreso=now(), usuario_ingreso=?"); //[129]
        sentencias.add("DELETE FROM cat_giros_ceda where id=?"); //[130]
        sentencias.add("UPDATE cat_giros_ceda SET id_sector=?, denominacion=?, fecha_ingreso=now(), usuario_ingreso=? WHERE id=?"); //[131]
        ///////////////////////////////////////////////////////////////////////
        sentencias.add("SELECT*FROM cat_sectores_ceda"); //[132]
        sentencias.add("INSERT INTO cat_sectores_ceda SET denominacion=?, fecha_ingreso=now(), usuario_ingreso=?"); //[133]
        sentencias.add("DELETE FROM cat_sectores_ceda where id=?"); //[134]
        sentencias.add("UPDATE cat_sectores_ceda SET denominacion=?, fecha_ingreso=now(), usuario_ingreso=? WHERE id=?"); //[135]
        ///////////////////////////////////////////////////////////////////////
        sentencias.add("SELECT*FROM cat_clasificador_ceda"); //[136]
        sentencias.add("INSERT INTO cat_clasificador_ceda SET denominacion=?, fecha_ingreso=now(), usuario_ingreso=?"); //[137]
        sentencias.add("DELETE FROM cat_clasificador_ceda where id=?"); //[138]
        sentencias.add("UPDATE cat_clasificador_ceda SET denominacion=?, fecha_ingreso=now(), usuario_ingreso=? WHERE id=?"); //[139]
        ///////////////////////////////////////////////////////////////////////
        sentencias.add("SELECT*FROM cat_tipo_estab_ceda"); //[140]
        sentencias.add("INSERT INTO cat_tipo_estab_ceda SET denominacion=?, fecha_ingreso=now(), usuario_ingreso=?"); //[141]
        sentencias.add("DELETE FROM cat_tipo_estab_ceda where id=?"); //[142]
        sentencias.add("UPDATE cat_tipo_estab_ceda SET denominacion=?, fecha_ingreso=now(), usuario_ingreso=? WHERE id=?"); //[143]
        ///////////////////////////////////////////////////////////////////////
        sentencias.add("SELECT*FROM cat_tipo_disp_ceda"); //[144]
        sentencias.add("INSERT INTO cat_tipo_disp_ceda SET denominacion=?, fecha_ingreso=now(), usuario_ingreso=?"); //[145]
        sentencias.add("DELETE FROM cat_tipo_disp_ceda where id=?"); //[146]
        sentencias.add("UPDATE cat_tipo_disp_ceda SET denominacion=?, fecha_ingreso=now(), usuario_ingreso=? WHERE id=?"); //[147]
        
        sentencias.add("SELECT id, denominacion as valor FROM cat_giros_ceda"); //[148]
        sentencias.add("SELECT id, denominacion as valor FROM cat_sectores_ceda"); //[149]
        sentencias.add("SELECT id, denominacion as valor FROM cat_clasificador_ceda"); //[150]
        sentencias.add("SELECT id, denominacion as valor FROM cat_tipo_estab_ceda"); //[151]
        sentencias.add("SELECT id, denominacion as valor FROM cat_tipo_disp_ceda"); //[152]
        sentencias.add("SELECT id, denominacion as valor FROM cat_sectores_ceda"); //[153]
        sentencias.add("SELECT*FROM cat_giros_ceda WHERE id=?"); //[154]
        sentencias.add("SELECT*FROM cat_sectores_ceda"); //[155]
        sentencias.add("select id, id_sector, x, y from cat_puntos_sectores where id_sector=?"); //[156]
        sentencias.add("select * from view_locales"); //[157]
        sentencias.add("select x, y from cat_puntos_locales where id=?"); //[158]
        sentencias.add("select convenio, x, y from cat_puntos_locales where id_sector<>?"); //[159]
        sentencias.add("SELECT * FROM cat_locales_ceda"); //[160]
        sentencias.add("SELECT if (id is null, '', id ) as id, if (email is null, '', email) as email, if(id_perfil is null,'',id_perfil) as id_perfil, if(id_app is null,'',id_app) as id_app, count(*) as cuantos, id_avatar,if(email like \"%@%\",\"1\",\"2\") as tipo_usr  FROM baseapp.app_usuarios WHERE email=? group by id"); //[161]
        
    }
    
   public String obten_sentencia(int index){
       return sentencias.get(index); 
   }
   
   
}




