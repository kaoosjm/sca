package info.sudr.file;

import java.awt.image.BufferedImage;
import java.io.*;
import java.security.MessageDigest;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mysql.mysql;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import sentencias.Propiedades;
import sentencias.sqls;

public class UploadServlet extends HttpServlet {

    private File filePath;
    mysql ms = new mysql();
    sentencias.sqls _sql = new sqls();
    private File fileCategoryPath;
    sentencias.Propiedades _prop = new Propiedades();

    @Override
    public void init(ServletConfig config) {
        filePath = new File(config.getInitParameter("upload_path"));
//        fileCategoryPath = new File(config.getInitParameter("upload_path") + "/productos/");
        fileCategoryPath = new File(config.getInitParameter("upload_path"));
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     *
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        //modo de llamar a las imagenes! 
        //  http://localhost:8080/appbase/upload?tipo=1&getfile=MEAL_04-11-2019_09-46-59.PNG
        //  http://localhost:8080/CACDMX/upload?tipo=1&getfile=MEAL_04-11-2019_09-46-59.PNG
        
        
        if (request.getParameter("getfile") != null && !request.getParameter("getfile").isEmpty()) {

            String tipo = request.getParameter("tipo");
            int tip = Integer.parseInt(tipo);

            switch (tip) {
                case 1:
                    filePath = fileCategoryPath;
                    break;

            }

            File file = new File(filePath, request.getParameter("getfile"));

            if (file.exists()) {
                int bytes = 0;
                ServletOutputStream op = response.getOutputStream();

                response.setContentType(getMimeType(file));
                response.setContentLength((int) file.length());
                response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");

                byte[] bbuf = new byte[1024];
                DataInputStream in = new DataInputStream(new FileInputStream(file));

                while ((in != null) && ((bytes = in.read(bbuf)) != -1)) {
                    op.write(bbuf, 0, bytes);
                }

                in.close();
                op.flush();
                op.close();
            }
        } else if (request.getParameter("delfile") != null && !request.getParameter("delfile").isEmpty()) {
//            File file = new File(request.getServletContext().getRealPath("/") + "imgs/" + request.getParameter("delfile"));
            String realPath = request.getServletPath();
            File file = new File(realPath + "imgs/" + request.getParameter("delfile"));
            if (file.exists()) {
                file.delete(); // TODO:check and report success
            }
        } else if (request.getParameter("getthumb") != null && !request.getParameter("getthumb").isEmpty()) {
             File file = new File(filePath, request.getParameter("getthumb"));
//            File file = new File(request.getServletContext().getRealPath("/") + "imgs/" + request.getParameter("getthumb"));
            if (file.exists()) {
                System.out.println(file.getAbsolutePath());
                String mimetype = getMimeType(file);
                if (mimetype.endsWith("png") || mimetype.endsWith("jpeg") || mimetype.endsWith("jpg") || mimetype.endsWith("gif")) {
                    BufferedImage im = ImageIO.read(file);
                    if (im != null) {
                        BufferedImage thumb = Scalr.resize(im, 75);
                        ByteArrayOutputStream os = new ByteArrayOutputStream();
                        if (mimetype.endsWith("png")) {
                            ImageIO.write(thumb, "PNG", os);
                            response.setContentType("image/png");
                        } else if (mimetype.endsWith("jpeg")) {
                            ImageIO.write(thumb, "jpg", os);
                            response.setContentType("image/jpeg");
                        } else if (mimetype.endsWith("jpg")) {
                            ImageIO.write(thumb, "jpg", os);
                            response.setContentType("image/jpeg");
                        } else {
                            ImageIO.write(thumb, "GIF", os);
                            response.setContentType("image/gif");
                        }
                        ServletOutputStream srvos = response.getOutputStream();
                        response.setContentLength(os.size());
                        response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
                        os.writeTo(srvos);
                        srvos.flush();
                        srvos.close();
                    }
                }
            } // TODO: check and report success
        } else {
            PrintWriter writer = response.getWriter();
            writer.write("call POST with multipart form data");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     *
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        
        if (!ServletFileUpload.isMultipartContent(request)) {
            throw new IllegalArgumentException("Request is not multipart, please 'multipart/form-data' enctype for your form.");
        }

        ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
        PrintWriter writer = response.getWriter();
        response.setContentType("application/json");
        JSONArray json = new JSONArray();
        try {
            List<FileItem> items = uploadHandler.parseRequest(request);
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss"); 
            
            String fieldname = "";
            String fieldvalue = "";
            String tipo_carga = "";
            String keycode = "";
            String idus = "";
            
            
            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    fieldname = item.getFieldName();
                    if (fieldname.equals("tipo_carga")){
                        fieldvalue = item.getString();
                        tipo_carga = fieldvalue;                        
                    }
                    if (fieldname.equals("idus")){
                        fieldvalue = item.getString();
                        idus = fieldvalue;                        
                    }
                    if (fieldname.equals("keycode")){
                        fieldvalue = item.getString();
                        keycode = fieldvalue;                        
                    }
                    
                }
            }
            
            for (FileItem item : items) {
                if (!item.isFormField()) {
                    
                    String itname = item.getName();
                    String exte = FilenameUtils.getExtension(itname);
                    String nfil = FilenameUtils.removeExtension(itname);
                    String fecha = format.format(new Date());
                    String carpeta = lpad(fecha.split("-")[0],2,'1')+"_"+lpad(fecha.split("-")[0],2,'2');
                    String namef = limpiarAcentos(  nfil.replace(" ", "_").replace("(", "").replace(")","")+"_"+fecha+ "." + exte  );
                    File file = new File(filePath, namef ); 
                    item.write(file);
                    
                    MessageDigest md5Digest = MessageDigest.getInstance("MD5");
                    String filename = file.getName();
                    String size = Long.toString(file.length());
                    String checksum = getFileChecksum(md5Digest, file);
                    
                    JSONObject params = new JSONObject();
                    params.put("1", tipo_carga);
                    params.put("2", itname);
                    params.put("3", namef);
                    params.put("4", checksum);
                    params.put("5", idus);
                    params.put("6", size);
                    params.put("7", keycode);
                      
                    JSONObject respuesta = ms.update(_prop.getCommand("38").comando(), params);
                    String keygenerado = respuesta.get("key_generado").toString();
                    System.out.println(respuesta.toJSONString());
                    
                    JSONObject jsono = new JSONObject();
                    jsono.put("name", item.getName());
                    jsono.put("size", item.getSize());
                    jsono.put("lastid", keygenerado);
                    jsono.put("nombre_sistema", namef);
                    jsono.put("url", "UploadServlet?getfile=" + item.getName());
                    jsono.put("thumbnail_url", "UploadServlet?getthumb=" + item.getName());
                    jsono.put("delete_url", "UploadServlet?delfile=" + item.getName());
                    jsono.put("delete_type", "GET");
                    json.add(jsono);
                    System.out.println(json.toString());
                }
            }
        } catch (FileUploadException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            writer.write(json.toString());
            writer.close();
        }

    }
    
    public static String lpad(String originalString, int length, char padCharacter) {
      String paddedString = originalString;
      while (paddedString.length() < length) {
         paddedString = padCharacter + paddedString;
      }
      return paddedString;
   }

    private String getMimeType(File file) {
        String mimetype = "";
        if (file.exists()) {
            if (getSuffix(file.getName()).equalsIgnoreCase("png")) {
                mimetype = "image/png";
            } else if (getSuffix(file.getName()).equalsIgnoreCase("jpg")) {
                mimetype = "image/jpg";
            } else if (getSuffix(file.getName()).equalsIgnoreCase("jpeg")) {
                mimetype = "image/jpeg";
            } else if (getSuffix(file.getName()).equalsIgnoreCase("gif")) {
                mimetype = "image/gif";
            } else {
                javax.activation.MimetypesFileTypeMap mtMap = new javax.activation.MimetypesFileTypeMap();
                mimetype = mtMap.getContentType(file);
            }
        }
        return mimetype;
    }

    private static String limpiarAcentos(String cadena) {
        String limpio = null;
        if (cadena != null) {
            String valor = cadena;
            valor = valor.toUpperCase();
            // Normalizar texto para eliminar acentos, dieresis, cedillas y tildes
            limpio = Normalizer.normalize(valor, Normalizer.Form.NFD);
            // Quitar caracteres no ASCII excepto la enie, interrogacion que abre, exclamacion que abre, grados, U con dieresis.
            limpio = limpio.replaceAll("[^\\p{ASCII}(N\u0303)(n\u0303)(\u00A1)(\u00BF)(\u00B0)(U\u0308)(u\u0308)]", "");
            // Regresar a la forma compuesta, para poder comparar la enie con la tabla de valores
            limpio = Normalizer.normalize(limpio, Normalizer.Form.NFC);
        }
        return limpio;
    }

    private static String getFileChecksum(MessageDigest digest, File file) throws IOException {
        //Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);
        //Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;
        //Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        };
        //close the stream; We don't need it now.
        fis.close();
        //Get the hash's bytes
        byte[] bytes = digest.digest();
        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        //return complete hash
        return sb.toString();
    }

    private String getSuffix(String filename) {
        String suffix = "";
        int pos = filename.lastIndexOf('.');
        if (pos > 0 && pos < filename.length() - 1) {
            suffix = filename.substring(pos + 1);
        }
        return suffix;
    }
}
