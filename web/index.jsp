<%-- 
    Document   : index
    Created on : 1/09/2019, 08:28:41 PM
    Author     : daniel
--%>

<%@page import="sentencias.estCommand"%>
<%@page import="sentencias.PropiedadesSistema"%>
<%
    
    sentencias.PropiedadesSistema _propS = new PropiedadesSistema();
    
    String titulo_tab_app = _propS.getProp("titulo_tab_app");
    String titulo_app_login = _propS.getProp("titulo_app_login");
    String subtitulo_app_login = _propS.getProp("subtitulo_app_login");
    
    
    //lista de valores obtenidos de propiedades....
    //fin de lista de valores obtenidos de propiedades....
    
    boolean respuesta = false; 
    if (respuesta){
    String redirectURL = "peri/mantenimiento.jsp";
    response.sendRedirect(redirectURL);
    } 
    
%>    

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title><%=titulo_tab_app%></title>

    <!-- vendor css -->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/sweetalert/sweetalert2.css" rel="stylesheet">
    <link href="lib/SpinKit/spinkit.css" rel="stylesheet">
    
    <!-- Bracket CSS -->
    <link rel="stylesheet" href="css/bracket.css">
    <link rel="stylesheet" href="css/own.css">
    <link rel="icon" href="img/favicon.ico"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <style>
        .alas-login{
            position: fixed;
            height:80%; 
            bottom: 0;    
            z-index: 0;
        }
        
        .textura-login{
            background-image: url('img/texturamx.png'); 
            background-repeat: no-repeat; 
            box-shadow: 2px 1px 15px #8c8888;
            z-index: 1;
        }
    </style>
  </head>

  <body>
    <!--<img class="alas-login" src="img/alas-v-50.svg" alt="Escudo">-->
    <svg width="1024" height="1024" style="position: fixed; bottom: -200px;"
        xmlns="http://www.w3.org/2000/svg">
        <image href="img/alas-v-75.svg" class="alas-login"/>
    </svg>
    
    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
        <div  class="textura-login login-wrapper wd-300 wd-xs-400 pd-25 pd-xs-40 bg-white rounded shadow-base">            
        
            
            <div class="signin-logo tx-center tx-28 tx-bold text-white"><span class="text-white"></span>
                <img width="50px" src="img/svg/nfc.svg">SCA<span class="tx-info"></span> 
                <span class="tx-normal"></span></div>
            <div class="tx-center mg-b-40 text-white">Sistema de Control de Acceso</div>
            <form id="frmloginin">
                <div class="form-group">
                    <input id="user" type="text" class="form-control" placeholder="E-mail" autocomplete="off">
                    <ul class="hide-error parsley-errors-list filled">
                        <li class="parsley-required">Este valor es requerido</li>
                    </ul>
                </div><!-- form-group -->
                <div class="form-group">
                    <input id="pass" type="password" class="form-control" placeholder="Contrase&ntilde;a" autocomplete="off">
                    <ul class="hide-error parsley-errors-list filled">
                        <li class="parsley-required">Este valor es requerido</li>
                    </ul>
                </div><!-- form-group -->
                <div class="form-group tx-12">Al ingresar usted acepta los terminos de uso y politicas de privacidad.</div>
                <button type="button" id="btn_login" class="btn btn-danger btn-block">Ingresar</button>
                <!--<button type="button" id="btn_testldap" class="btn btn-danger btn-block">testLDAP</button>-->
                <div style="text-align: center">
                    <label style="color: red; margin-top: 10px; display: none" id="alerterr">usuario/contraseña no válido</label>
                </div>
                <div id="loader" style="text-align: center; display: none">
                    <div class="sk-wave">
                        <div class="sk-rect sk-rect1 bg-gray-800"></div>
                        <div class="sk-rect sk-rect2 bg-gray-800"></div>
                        <div class="sk-rect sk-rect3 bg-gray-800"></div>
                        <div class="sk-rect sk-rect4 bg-gray-800"></div>
                        <div class="sk-rect sk-rect5 bg-gray-800"></div>
                        <label>Procesando solicitud</label>
                    </div>
                </div>
            </form>
            <hr>
            <!--<div class="mg-t-40 tx-center">¿Olvido su password? <a href="recovery.jsp" class="tx-info">ingrese aqui para recuperar</a></div>-->
            <!--div class="mg-t-40 tx-center">¿Desea ser proveedor, registrarse? <a href="registro.jsp" class="tx-info">Registrarse</a></div-->
            
        </div><!-- login-wrapper -->
    </div><!-- d-flex -->

    <script src="lib/jquery/jquery.js"></script>
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script src="js/own.js"></script>
    <script src="lib/sweetalert/sweetalert2.all.min.js"></script>
    <script src="js/login.js"></script>
  </body>
</html>

