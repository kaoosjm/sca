<%-- 
    Document   : 404
    Created on : 27 nov 2019, 6:05:41
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page isErrorPage="true" %>
<!DOCTYPE html>
<html lang="en" class="pos-relative">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>DGTI</title>

    <!-- vendor css -->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
  </head>

  <body class="pos-relative">

    <div class="ht-100v d-flex align-items-center justify-content-center">
      <div class="wd-lg-70p wd-xl-50p tx-center pd-x-40">
        <h1 class="tx-100 tx-xs-140 tx-normal tx-inverse tx-roboto mg-b-0">500!</h1>
        <h5 class="tx-xs-24 tx-normal tx-info mg-b-30 lh-5">Se ha encontrado un error en la página solicitada.</h5>
        <p class="tx-16 mg-b-30">Reporte este mensaje al administrador del sitio.</p>
        
        
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Error</td>
                        <td>${pageContext.exception}</td>
                    </tr>
                    <tr>
                        <td>URI</td>
                        <td>${pageContext.errorData.requestURI}</td>
                    </tr>
                </tbody>
            </table>
        
<!--        <div class="d-flex justify-content-center">
          <div class="input-group wd-xs-300">
            <input type="text" class="form-control" placeholder="Search...">
            <div class="input-group-btn">
              <button class="btn btn-info"><i class="fa fa-search"></i></button>
            </div> input-group-btn 
          </div> input-group 
        </div> d-flex -->
      </div>
    </div><!-- ht-100v -->

    <script src="../lib/jquery/jquery.js"></script>
    <script src="../lib/popper.js/popper.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>

  </body>
</html>
