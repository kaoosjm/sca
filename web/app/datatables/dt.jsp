<%-- 
    Document   : dt1
    Created on : 30/01/2019, 11:09:27 AM
    Author     : daniel.ramos
--%>

<%@page import="sentencias.Propiedades"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="sentencias.sqls"%>
<%@page import="mysql.mysql"%>
<%@page import="java.util.Map"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="sentencias.sqls"%>

<%
    
    sqls sqls = new sqls();
    mysql db = new mysql();
    sentencias.Propiedades _prop = new Propiedades();
    
    lib.utiles util = new lib.utiles(); 
    
    String busqueda = request.getParameter("search[value]");
    String longitud = request.getParameter("length");
    String paquete = request.getParameter("start");
    
    
    Map<String, String[]> parameters = request.getParameterMap();
    for(String parameter : parameters.keySet()) {
        if(parameter.toLowerCase().startsWith("question")) {
            String[] values = parameters.get(parameter);
        }
    }
    
    String[] columnas = parameters.get("columnas");
    String[] queryid = parameters.get("queryid");
    String[] params = parameters.get("parametros");
    
    String id_tabla = queryid[0];
    
    String _params = null;
    
    if (params!=null){
    _params = params[0]; 
    }
    
    JSONObject _p=null; 
    
    if (_params != null ){ 
    JSONParser parse = new JSONParser() ;
    _p = (JSONObject) parse.parse(_params);    
    }
    
    
    //  obtiene la tabla de la consulta...
//    String query = sqls.obten_sentencia( Integer.parseInt(id_tabla)); 
    String query = _prop.getCommand( id_tabla ).comando(); 
    String table = util.obtenTableSentencia(query.split(" ")); 
    
    
    
    JSONObject result = new JSONObject();
    JSONArray array = new JSONArray();
    int amount = 10;
    int start = 0;
    int echo = 0;
    int col = 0;

    String dir = "asc";
    String sStart = parameters.get("start")[0];
    String sAmount = parameters.get("length")[0];
    
    String sEcho = request.getParameter("sEcho");
    String sCol = request.getParameter("iSortCol_0");
    String sdir = request.getParameter("sSortDir_0");

    
    
    String[] cols = columnas[0].split(",");
//    String[] colt = col_type[0].split(",");
    
   List<String> sArray = new ArrayList();
    for (int y=0;y<cols.length;y++){
        sArray.add(" " + cols[y]+" like '%" + busqueda + "%'");
    }
    
    String individualSearch = "";
    if(sArray.size()==1){
        individualSearch = sArray.get(0);
    }else if(sArray.size()>1){
        for(int i=0;i<sArray.size()-1;i++){
            individualSearch += sArray.get(i)+ " or ";
        }
        individualSearch += sArray.get(sArray.size()-1);
    }
    
if (sStart != null) {
        start = Integer.parseInt(sStart);
        if (start < 0) {
            start = 0;
        }
    }
    if (sAmount != null) {
        amount = Integer.parseInt(sAmount);
        if (amount < 10 || amount > 100) {
            amount = 10;
        }
    }
    if (sEcho != null) {
        echo = Integer.parseInt(sEcho);
    }
    if (sCol != null) {
        col = Integer.parseInt(sCol);
        if (col < 0 || col > 6) {
            col = 0;
        }
    }
    if (sdir != null) {
        if (!sdir.equals("asc")) {
            dir = "desc";
        }
    }
    
    //String colName = cols[col];
    int total = 0;
    String sql;
//    String individualSearch="";
    
    //busqueda = "";
    
    if (busqueda.length()>0){
        sql = "SELECT count(*) cuantos FROM " + table + " where " + individualSearch;
    }else{
        sql = "SELECT count(*) cuantos FROM " + table;
    }
    
    JSONObject res;
    
    if (_p==null) { 
    res = db.select(query, new JSONObject()); 
        
    } else { 
    res = db.select(query, _p); 
        
    }
    
    
    JSONArray dat = (JSONArray) res.get("data");
    
    String ototal = String.valueOf(dat.size());
    
    int totalAfterFilter = Integer.parseInt(ototal);

    String searchSQL = "";
    
    JSONObject datos; 
            
    if (_p==null){ 
        
        if (busqueda.length()>0){
            sql = "SELECT * FROM " + table + " where " + individualSearch;
        } else { 
            sql = "SELECT * FROM " + table;
        }
        
    } else { 
        
        if (busqueda.length()>0){
            sql = query + " and " + individualSearch;
        } else { 
            sql = query;
        }
        
        
    }
    
    
    sql += " limit " + amount + " offset " +start;

    
//    JSONArray datos = db.getSQLComplxJSONArray(sql);
    if (_p==null){
    datos = db.select(sql, new JSONObject());
        
    } else { 
        
    datos = db.select(sql, _p);
    }
    
    
    JSONArray dtrs = (JSONArray) datos.get("data");
    
    String searchTerm = "";
    String sql2;
    if (busqueda.length()>1){
        sql2 = "SELECT count(*) cuantos FROM " + table + " where " + individualSearch;
    }else{
        sql2 = "SELECT count(*) cuantos FROM " + table;
    }
        JSONObject ress = db.select(sql2, new JSONObject());
        JSONArray datr = (JSONArray) ress.get("data");
        JSONObject cdt = (JSONObject) datr.get(0);
        
        
        totalAfterFilter = Integer.parseInt(cdt.get("cuantos").toString());
            
    
    result.put("draw", 0);
    result.put("recordsTotal", totalAfterFilter);
    result.put("recordsFiltered", totalAfterFilter);
    result.put("data", dtrs);
    response.setContentType("application/json");
    response.setHeader("Cache-Control", "no-store");
    out.print(result);


%>