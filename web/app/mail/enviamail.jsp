<%-- 
    Document   : enviamail
    Created on : 10/12/2018, 12:23:33 PM
    Author     : kaoos
--%>
<jsp:useBean id="db" class="database.db_mysql" scope="page"></jsp:useBean>
<jsp:useBean id="cstr" class="defincion.sqlCadenas" scope="page"></jsp:useBean>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="index.jsp">Inicio</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Panel</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Envio de Correo</span>
        </li>
    </ul>
</div>
<p></p>
<!-- END PAGE BAR -->
<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
        <!-- BEGIN PORTLET-->
        <div id="portletrpt" class="portlet light portlet-fit bordered" style="height: calc(100vh - 185px);overflow-y: auto;">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Envio de Correo</span>                    
                </div>                
            </div>
            <div class="portlet-body">
                <div class="inbox-content">
                <form class="inbox-compose form-horizontal" action="" id="frmsendmessage" novalidate="novalidate">
                    <div class="inbox-compose-btn">
                        <button class="btn green">
                            <i class="fa fa-check"></i>Enviar</button>
                        <button class="btn default inbox-discard-btn">Descartar</button>                        
                    </div>
                    <div class="inbox-form-group mail-to">
                        <label class="control-label">A:</label>
                        <div class="controls controls-to">
                            <select id="to" name="to" class="form-control select2-multiple" multiple>
                                <%= db.dameListaSelect(cstr._sql060a, 3, 2,1) %>
                            </select>
                            <span class="inbox-cc-bcc">
                                <span class="inbox-cc"> Cc </span>
                                <span class="inbox-bcc"> Cco </span>
                            </span>
                        </div>
                    </div>
                    <div class="inbox-form-group input-cc display-hide">
                        <a href="javascript:;" class="close"> </a>
                        <label class="control-label">Cc:</label>
                        <div class="controls controls-cc">
                            <!--<input type="text" name="cc" class="form-control">-->
                            <select id="cc" name="cc" class="form-control select2-multiple" multiple>
                                <%= db.dameListaSelect(cstr._sql060a, 3, 2,1) %>
                            </select>
                        </div>
                    </div>
                    <div class="inbox-form-group input-bcc display-hide">
                        <a href="javascript:;" class="close"> </a>
                        <label class="control-label">Cco:</label>
                        <div class="controls controls-bcc">
                            <!--<input type="text" name="bcc" class="form-control">-->
                            <select id="bcc" name="bcc" class="form-control select2-multiple" multiple>
                                <%= db.dameListaSelect(cstr._sql060a, 3, 2,1) %>
                            </select>
                        </div>
                    </div>
                    <div class="inbox-form-group">
                        <label class="control-label">Asunto:</label>
                        <div class="controls">
                            <input type="text" class="form-control" name="subject" id="subject"> </div>
                    </div>
                    <div class="inbox-form-group">
                        <textarea class="inbox-editor inbox-wysihtml5 form-control" name="message" id="message" rows="14"></textarea>
                    </div>                                                          
                    
                    <div class="inbox-compose-btn">
                        <button class="btn green">
                            <i class="fa fa-check"></i>Enviar</button>
                        <button class="btn default">Descartar</button>                        
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>