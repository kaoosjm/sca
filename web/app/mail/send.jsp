<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.io.File,java.util.*,javax.mail.*,javax.mail.internet.*,javax.activation.*" %>
<%
    response.setHeader("Cache-Control","no-cache");
    response.setHeader("Cache-Control","no-store");
    response.setHeader("Pragma","no-cache");
    response.setDateHeader ("Expires", 0);
    String footer = "Este es el footer";
    String username = "desarrolloert@gmail.com";
    String password = "Desarrolloert1";
    String from = "no-responder@wfapp.com.mx"; 
    String to = "";
    String subject = "Solicitud de clave de acceso";
    String nick = "jhon";
    String result = null;
    
    String[] recipientList = request.getParameterValues("to[]");
    String[] recipientListcc = null;
    if (request.getParameterValues("cc[]")!=null){
        recipientListcc = request.getParameterValues("cc[]");
    }
    
    
    String mailmessage= request.getParameter("mm");
    String asunto= request.getParameter("a");
    
    JSONObject parametros = new JSONObject();
    JSONObject respuesta = new JSONObject();
    
        try {
            Properties props = System.getProperties();
            props.setProperty("mail.transport.protocol", "smtp");
            props.setProperty("mail.host", "smtp.gmail.com");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");
            props.put("mail.debug", "true");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");
 
            Session emailSession = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("desarrolloert@gmail.com","Desarrolloert1"); //usuario y contraseña de correo
                    }
            });
 
            emailSession.setDebug(true);

            MimeMessage message = new MimeMessage(emailSession);
            message.setFrom(new InternetAddress(from));
            //------------------------------------------------------------------
            InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
            int counter = 0;
            for (String recipient : recipientList) {
                recipientAddress[counter] = new InternetAddress(recipient.trim());
                counter++;
            }
            message.setRecipients(Message.RecipientType.TO, recipientAddress);
            if (recipientListcc!=null){
                InternetAddress[] recipientAddresscc = new InternetAddress[recipientListcc.length];
                int countercc = 0;
                for (String recipientcc : recipientListcc) {
                    recipientAddresscc[countercc] = new InternetAddress(recipientcc.trim());
                    countercc++;
                }
                message.setRecipients(Message.RecipientType.CC, recipientAddresscc);
            }
            //------------------------------------------------------------------
//            InternetAddress[] address = {new InternetAddress(to)};            
//            message.setRecipients(Message.RecipientType.TO, address);
            //------------------------------------------------------------------
            message.setSubject(asunto);
            message.setSentDate(new Date());
            
            MimeMultipart multipart = new MimeMultipart();
            MimeBodyPart mbp = new MimeBodyPart(); 
            mbp.setContent(""
                    + "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" >"
                    + "<style type=\"text/css\"> "
                    +"html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}"
                    +"@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { "
                    +"*[class=\"table_width_100\"] {"
                    +"	width: 96% !important;"
                    +"}"
                    +"*[class=\"border-right_mob\"] {"
                    +"	border-right: 1px solid #dddddd;"
                    +"}"
                    +"*[class=\"mob_100\"] {"
                    +"	width: 100% !important;"
                    +"}"
                    +"*[class=\"mob_center\"] {"
                    +"	text-align: center !important;"
                    +"}"
                    +"*[class=\"mob_center_bl\"] {"
                    +"	float: none !important;"
                    +"	display: block !important;"
                    +"	margin: 0px auto;"
                    +"}	"
                    +".iage_footer a {"
                    +"	text-decoration: none;"
                    +"	color: #929ca8"
                    +"}"
                    +"img.mob_display_none {"
                    +"	width: 0px !important;"
                    +"	height: 0px !important;"
                    +"	display: none !important;"
                    +"}"
                    +"img.mob_width_50 {"
                    +"	width: 40% !important;"
                    +"	height: auto !important;"
                    +"}"
                    +"}"
                    +".table_width_100 {"
                    +"width: 680px;"
                    +"}"
                    +"</style>"
                    + "<body style=\"padding: 0px; margin: 0px;\">"
                    + "<div id=\"mailsub\" class=\"notification\" align=\"center\">"
                    + "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"min-width: 320px;\"><tr><td align=\"center\" bgcolor=\"#eff3f8\">"
                    + "<table width=\"680\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>"
                    + "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"table_width_100\" width=\"100%\" style=\"max-width: 680px; min-width: 300px;\"><tr><td>"
                    + "<div style=\"height: 80px; line-height: 80px; font-size: 10px;\">&nbsp;</div></td></tr>"
                    + "<tr><td align=\"center\" bgcolor=\"#364150\">" //-------------------------------------------- color azul barra
//                    + "<tr style='background-color: #364150;'><td align=\"center\" bgcolor=\"#ffffff\">"
                    + "<div style=\"height: 30px; line-height: 30px; font-size: 10px;\">&nbsp;</div>"
                    + "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td align=\"left\">"
                    + "<div class=\"mob_center_bl\" style=\"float: left; display: inline-block; width: 115px;\">"
                    + "<table class=\"mob_center\" width=\"115\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"border-collapse: collapse;\">"
                    + "<tr><td align=\"left\" valign=\"middle\">"
                    + "<div style=\"height: 20px; line-height: 20px; font-size: 10px; \">&nbsp;</div>"
                    + "<table width=\"315\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style='margin: 0 auto;'>"
                    + "<tr><td align=\"left\" valign=\"top\" class=\"mob_center\">"
                    +"<h1><b><span style='color: #ffffff;'>WORKFLOW</span> <span style='color:#0054a9;'>DANHOS</span></b></h1>" //----------- titulo en barra azul
//                    + "<a href=\"#\" target=\"_blank\" style=\"color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;\">"
//                    + "<font face=\"Arial, Helvetica, sans-seri; font-size: 13px;\" size=\"3\" color=\"#596167\">"                    
//                    + "<img src=\"http://localhost:8080/WFApp/sitio/assets/pages/img/logo-big-2.png\" alt=\"WFAPP\" border=\"0\" style=\"display: block;\" /></font></a>"
//                    + "<img src=\"logo_250x79.png\" alt=\"\" border=\"0\" style=\"display: block;\" /></font></a>"
                    + "</td></tr>"
                    + "</table>"
                    + "</td></tr>"
                    + "</table></div>"
                    + "</td></tr>"
                    + "</table>"
                    + "<div style=\"height: 50px; line-height: 50px; font-size: 10px;\">&nbsp;</div>"
                    + "</td></tr>"
                    //content
                    + "<tr><td align=\"center\" bgcolor=\"#fbfcfd\">"
                    + "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"
                    + "<tr><td align=\"\">"
                    + "<div style=\"height: 60px; line-height: 60px; font-size: 10px;\">&nbsp;</div>"
                    + ""+mailmessage
//                    + "<div style=\"line-height: 44px;\">"
//                    + "<font face=\"Arial, Helvetica, sans-serif\" size=\"5\" color=\"#57697e\" style=\"font-size: 36px;\">"
//                    + "<span style=\"font-family: Arial, Helvetica, sans-serif; font-size: 36px; color: #57697e;\">"
//                    + "Solicitud de C&oacute;digo" /////----------------------------------------------------------------
//                    + "</span></font></div>"
//                    + "<div style=\"height: 40px; line-height: 40px; font-size: 10px;\">&nbsp;</div>"
//                    + "</td></tr>"
//                    + "<tr><td align=\"center\">"
//                    + "<div style=\"line-height: 24px;\">"
//                    + "<font face=\"Arial, Helvetica, sans-serif\" size=\"4\" color=\"#57697e\" style=\"font-size: 17px;\">"
//                    + "<span style=\"font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #57697e;\">"
//                    + "Hola <b>"+nombre+" ( "+nick+" )</b><br>"
//                    + "Tu c&oacute;digo de acceso es: <b>"+codigo+"</b><br><br>"                    
//                    + "<h4>NOTA:</h4><h5>Tenga en cuenta que este correo electr&oacute;nico fue enviado de manera autom&aacute;tica, favor de no responder a este correo. </h5>"
                    
                    + "" ////--------------------
//                    + "</span></font>"
//                    + "</div>"
                    + "<div style=\"height: 40px; line-height: 40px; font-size: 10px;\">&nbsp;</div></td></tr>"
                    + "</table>"
                    + "</td></tr>"
                    + "<tr><td class=\"iage_footer\" align=\"center\" bgcolor=\"#364150\">"//-----footer
//                    + "<tr><td class=\"iage_footer\" align=\"center\" bgcolor=\"#ffffff\">"//-----footer
                    + "<div style=\"height: 80px; line-height: 80px; font-size: 10px;\">&nbsp;</div>	"
                    + "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"
                    + "<tr><td align=\"center\">"
                    + "<font face=\"Arial, Helvetica, sans-serif\" size=\"3\" color=\"#96a5b5\" style=\"font-size: 13px;\">"
                    + "<span style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #ffffff;\">"
//                    + "<span style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;\">"
                    + " "+ footer +" " //----------------------------
                    + "</span></font>	"
                    + "</td></tr>		"
                    + "</table>"
                    + "<div style=\"height: 30px; line-height: 30px; font-size: 10px;\">&nbsp;</div>"
                    + "</td></tr>"
                    + "<tr><td>"
                    + "<div style=\"height: 80px; line-height: 80px; font-size: 10px;\">&nbsp;</div>"
                    + "</td></tr>"
                    + "</table>"
                    + "</td></tr>"
                    + "</table>"
                    + "</td></tr>"
                    + "</table>"
                    + "</div>"
                    + "</body></html> ","text/html; charset=utf-8");
            multipart.addBodyPart(mbp); 
            message.setContent(multipart, "text/html; charset=utf-8"); 

            Transport transport = emailSession.getTransport("smtps");
            transport.connect("smtp.gmail.com", username, password);
            transport.sendMessage(message, message.getAllRecipients());
            result = "El Correo ha sido enviado";
//------   Registra Log de Usuario ----------------------------------------------------------------------
//            db.registraLog("E-mail Login",idu,"Envia código","Envio de código: \""+codigo+"\" de acceso al sistema a E-mail: "+to,"sis_codigos_verificacion",codigo_id); 
//------   Registra Log de Usuario ----------------------------------------------------------------------            
        } catch (MessagingException e) {
            result = "Unable to send email"+e.getMessage();
        }
        out.println(result);
%>