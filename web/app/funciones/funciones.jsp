<%-- 
    Document   : funciones
    Created on : 4/09/2019, 07:56:42 AM
    Author     : daniel
--%>

<%@page import="sentencias.Propiedades"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="javax.xml.bind.ParseConversionEvent"%>
<%@page import="sentencias.sqls"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="mysql.mysql"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<% 
    request.setCharacterEncoding("UTF-8");
    sentencias.sqls _sql = new sqls();
    JSONObject JSON_parameters = null;
    sentencias.Propiedades _prop = new Propiedades();
    //---------------------------------------------
    String _index = request.getParameter("i"); 
    String _tipo = request.getParameter("t");
    String _params = request.getParameter("p");
    //---------------------------------------------
    if (_params != null) { 
    JSONParser parser = new JSONParser();
    JSON_parameters = (JSONObject) parser.parse(_params);
    }
    //---------------------------------------------
//    String consulta = _sql.obten_sentencia( Integer.parseInt(_index));
    String consulta = _prop.getCommand(_index).comando();
    int tipo = Integer.parseInt(_tipo);
    JSONObject respuesta; 
    //---------------------------------------------
    mysql my = new mysql(); 
    //---------------------------------------------
    switch (tipo) { 
        case 1: 
        // casos para SELECT
            respuesta = my.select(consulta, JSON_parameters);
            out.print(respuesta.toString());
            break; 
        case 2:
        // case para INSERT, UPDATE
            respuesta = my.update(consulta, JSON_parameters);
            out.print(respuesta.toString());
            break; 
        case 3:
        // case para ingreso de batch
            break; 
    }
%>