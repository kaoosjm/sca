<%-- 
    Document   : usuarios
    Created on : 13 sep 2019, 14:32:55
    Author     : daniel.ramos
--%>
<%@page import="lib.envia_mail_recovery"%>
<%@page import="lib.PasswordGenerator"%>
<%@page import="lib.BHtokens"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="mysql.mysql"%>
<%@page import="sentencias.sqls"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
    
    mysql ms = new mysql();
    sentencias.sqls _sql = new sqls();
    envia_mail_recovery emr = new envia_mail_recovery(); 
    
    //se busca en base de datos si existe el email! 
    
    JSONObject respuesta_proceso = new JSONObject(); 
    
    String parametros = request.getParameter("p");
    JSONParser parser = new JSONParser();
    JSONObject JSON_parameters = (JSONObject) parser.parse(parametros);
    
    
    JSONObject pbusqueda = new JSONObject(); 
    String email = JSON_parameters.get("1").toString(); 
    
    pbusqueda.put("1", email); 
    JSONObject rbus = ms.select(_sql.obten_sentencia(90), pbusqueda); 
    JSONArray datbus = (JSONArray) rbus.get("data"); 
    JSONObject dato = (JSONObject) datbus.get(0); 
    
    String cuantos = dato.get("cuantos").toString();
    String usuario = dato.get("nombre").toString();
    int _cuantos = Integer.parseInt(cuantos); 
    
    if (_cuantos!=0) { 
        
        lib.PasswordGenerator pswgen = new PasswordGenerator(); 
        String password = pswgen.getPassword(8); 
        
        String sentencia = _sql.obten_sentencia(89);
        JSONObject paramupdate = new JSONObject(); 
        paramupdate.put("1",password);
        paramupdate.put("2",email);
        JSONObject respuesta_update = ms.update(sentencia, paramupdate); 
        
        boolean resp_update = true; 
        
        if (resp_update) {
            
            JSONObject r = emr.inicializa2(password, email, usuario);
            respuesta_proceso.put("respuesta", "1");
            respuesta_proceso.put("mensaje", r);
        } 
        
    } else { 
        respuesta_proceso.put("respuesta", "2");
    }
    
    out.print(respuesta_proceso.toJSONString());
    
    
%>