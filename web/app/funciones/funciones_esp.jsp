<%-- 
    Document   : funciones
    Created on : 4/09/2019, 07:56:42 AM
    Author     : daniel
--%>

<%@page import="sentencias.Propiedades"%>
<%@page import="lib.utiles"%>
<%@page import="org.jsoup.nodes.Element"%>
<%@page import="org.jsoup.select.Elements"%>
<%@page import="org.jsoup.nodes.Document"%>
<%@page import="org.jsoup.Jsoup"%>
<%@page import="lib.PasswordGenerator"%>
<%@page import="lib.envia_mail_recovery"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="javax.xml.bind.ParseConversionEvent"%>
<%@page import="sentencias.sqls"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="mysql.mysql"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<% 
    request.setCharacterEncoding("UTF-8");
    sentencias.sqls _sql = new sqls();
    JSONObject JSON_parameters = null;
     sentencias.Propiedades _prop = new Propiedades();
    //---------------------------------------------
    String _index = request.getParameter("i"); 
    String _tipo = request.getParameter("t");
    String _params = request.getParameter("p");
    //---------------------------------------------
    if (_params != null) { 
    JSONParser parser = new JSONParser();
    JSON_parameters = (JSONObject) parser.parse(_params);
    }
    //---------------------------------------------
    
//    String consulta = _sql.obten_sentencia( Integer.parseInt(_index));
    String consulta = _prop.getCommand(_index).comando();
    int tipo = Integer.parseInt(_tipo);
//    int tipo = Integer.valueOf(_prop.getCommand(_index).tipo());
    JSONObject respuesta = new JSONObject(); 
    //---------------------------------------------
    mysql my = new mysql(); 
    //---------------------------------------------
    switch (tipo) { 
        case 1: 
            String _s = request.getParameter("s"); 
            JSONArray s = new JSONArray(); 
        
            JSONParser parser1= new JSONParser(); 
            s = (JSONArray) parser1.parse(_s); 
            
            respuesta = my.update(consulta, JSON_parameters);
//            out.print(respuesta.toString());
            
            JSONArray dataresp = (JSONArray) respuesta.get("data"); 
            JSONObject resultado = (JSONObject) dataresp.get(0);
            String id_producto = respuesta.get("key_generado").toString();
            
            JSONObject dato = new JSONObject(); 
            JSONArray datos_ingreso = new JSONArray(); 
            
        if (_s != null) { 
            
            for (int y=0; y < s.size();  y++){
                dato = (JSONObject) s.get(y); 
                JSONObject registro = new JSONObject(); 
                String fle = dato.get("file").toString();
                String ide = dato.get("ide").toString();
                registro.put("1", ide);
                registro.put("2", id_producto);
                registro.put("3", fle);
                registro.put("4", JSON_parameters.get("p").toString());
                datos_ingreso.add(registro); 
            }
                respuesta = my.update_batch( datos_ingreso , _sql.obten_sentencia(86)); 
            }
            break; 
            
        case 2:
            _s = request.getParameter("s"); 
            s = new JSONArray(); 
            parser1= new JSONParser(); 
            s = (JSONArray) parser1.parse(_s); 
            
            respuesta = my.update(consulta, JSON_parameters);
            System.out.println(respuesta.toString());
//            out.print(respuesta.toString());
            id_producto = JSON_parameters.get("a").toString();
            
            dato = new JSONObject(); 
            datos_ingreso = new JSONArray(); 
            
            
            if (_s != null) { 
            
            JSONObject respuesta_del = new JSONObject();
            JSONObject params2 = new JSONObject(); 
            params2.put("1", id_producto); 
            respuesta_del =     my.update(_sql.obten_sentencia(88), params2);
                
            for (int y=0; y < s.size();  y++){
                dato = (JSONObject) s.get(y); 
                JSONObject registro = new JSONObject(); 
                String fle = dato.get("file").toString();
                String ide = dato.get("ide").toString();
                registro.put("1", ide);
                registro.put("2", id_producto);
                registro.put("3", fle);
                registro.put("4", JSON_parameters.get("p").toString());
                datos_ingreso.add(registro); 
            }
                respuesta = my.update_batch( datos_ingreso , _sql.obten_sentencia(86)); 
            }
            
            break; 
        case 3:
            
            respuesta = my.update(consulta, JSON_parameters);
            session.invalidate();
            break; 
        case 4:
            
            _s = request.getParameter("s"); 
            s = new JSONArray(); 
            parser1= new JSONParser(); 
            s = (JSONArray) parser1.parse(_s); 
            
            respuesta = my.update(consulta, JSON_parameters);
            String id_usuario = JSON_parameters.get("2").toString();
            
            dataresp = (JSONArray) respuesta.get("data"); 
            resultado = (JSONObject) dataresp.get(0);
            String id_zona = respuesta.get("key_generado").toString();
            
            dato = new JSONObject(); 
            datos_ingreso = new JSONArray(); 
            
            
            if (_s != null) { 
            
            /*
            //aqui borra en UPDATE    
            JSONObject respuesta_del = new JSONObject();
            JSONObject params2 = new JSONObject(); 
            params2.put("1", id_zona); 
            respuesta_del =     my.update(_sql.obten_sentencia(88), params2);
            */
            
            for (int y=0; y < s.size();  y++){
                dato = (JSONObject) s.get(y); 
                JSONObject registro = new JSONObject(); 
                String lat = dato.get("lat").toString();
                String lng = dato.get("lng").toString();
                registro.put("1", id_zona);
                registro.put("2", lat);
                registro.put("3", lng);
                registro.put("4", lat);
                registro.put("5", lng);
                registro.put("6", id_usuario);
                datos_ingreso.add(registro); 
            }
                respuesta = my.update_batch( datos_ingreso , _sql.obten_sentencia(105)); 
            }
            
            
            break;
        case 5:
            
            _s = request.getParameter("s"); 
            s = new JSONArray(); 
            parser1= new JSONParser(); 
            s = (JSONArray) parser1.parse(_s); 
            
            respuesta = my.update(consulta, JSON_parameters);
            id_usuario = JSON_parameters.get("2").toString();
            id_zona = JSON_parameters.get("3").toString();
            
            dato = new JSONObject(); 
            datos_ingreso = new JSONArray(); 
            
            
            if (_s != null) { 
            
            
            //aqui borra en UPDATE    
            JSONObject respuesta_del = new JSONObject();
            JSONObject params2 = new JSONObject(); 
            params2.put("1", id_zona); 
            respuesta_del =     my.update(_sql.obten_sentencia(105), params2);
                        
            for (int y=0; y < s.size();  y++){
                dato = (JSONObject) s.get(y); 
                JSONObject registro = new JSONObject(); 
                String lat = dato.get("lat").toString();
                String lng = dato.get("lng").toString();
                registro.put("1", id_zona);
                registro.put("2", lat);
                registro.put("3", lng);
                registro.put("4", lat);
                registro.put("5", lng);
                registro.put("6", id_usuario);
                datos_ingreso.add(registro); 
            }
                respuesta = my.update_batch( datos_ingreso , _sql.obten_sentencia(105)); 
            }
            
            
            break;
            
        case 6: 
            //aqui se envia para la creación del usuario. 

            String id = JSON_parameters.get("1").toString();
            String email_contacto = JSON_parameters.get("2").toString();
            
            
            JSONObject params2 = new JSONObject(); 
            params2.put("1", id); 
            JSONObject pinfo = my.select(_sql.obten_sentencia(69), params2);
            
            JSONArray daty = (JSONArray) pinfo.get("data"); 
            JSONObject recinfo = (JSONObject) daty.get(0); 
            String email = recinfo.get("email_contacto").toString(); 
            String denominacion = recinfo.get("denominacion").toString(); 
            
            
            JSONObject params3 = new JSONObject(); 
            params3.put("1", id); 
            //aqui ingresa el usuario en la tabla de usuarios.
            JSONObject proceso = my.update(_sql.obten_sentencia(108), params3);
            
            //aqui actualiza el estado de la tabla de cat_instituciones.
            respuesta = my.update(_sql.obten_sentencia(109),params3); 
            //una vez ingresado envia por correo el nuevo password! 
            
            envia_mail_recovery emr = new envia_mail_recovery(); 
            lib.PasswordGenerator pswgen = new PasswordGenerator(); 
            String password = pswgen.getPassword(8); 

            String sentencia = _sql.obten_sentencia(89);
            JSONObject paramupdate = new JSONObject(); 
            paramupdate.put("1",password);
            paramupdate.put("2",email);
            JSONObject respuesta_update = my.update(sentencia, paramupdate); 

            boolean resp_update = true; 

            if (resp_update) {
                JSONObject r = emr.inicializa(password, email, denominacion);
                respuesta.put("respuesta", "1");
                respuesta.put("mensaje", r);
            } 
            
            break; 
        case 7: 
            //aqui se envia para la creación del usuario. 

            id = JSON_parameters.get("1").toString();
            email_contacto = JSON_parameters.get("2").toString();
            
            
            params2 = new JSONObject(); 
            params2.put("1", id); 
            pinfo = my.select(_sql.obten_sentencia(69), params2);
            
            daty = (JSONArray) pinfo.get("data"); 
            recinfo = (JSONObject) daty.get(0); 
            email = recinfo.get("email_contacto").toString(); 
            denominacion = recinfo.get("denominacion").toString(); 
            
            
            params3 = new JSONObject(); 
            params3.put("1", id); 
            //aqui ingresa el usuario en la tabla de usuarios.
            proceso = my.update(_sql.obten_sentencia(108), params3);
            
            //aqui actualiza el estado de la tabla de cat_instituciones.
            respuesta = my.update(_sql.obten_sentencia(109),params3); 
            //una vez ingresado envia por correo el nuevo password! 
            
            emr = new envia_mail_recovery(); 
            pswgen = new PasswordGenerator(); 
            password = pswgen.getPassword(8); 

            sentencia = _sql.obten_sentencia(89);
            paramupdate = new JSONObject(); 
            paramupdate.put("1",password);
            paramupdate.put("2",email);
            respuesta_update = my.update(sentencia, paramupdate); 

            resp_update = true; 

            if (resp_update) {
                JSONObject r = emr.inicializa(password, email, denominacion);
                respuesta.put("respuesta", "1");
                respuesta.put("mensaje", r);
            } 
            
            break; 
        case 8:
            JSONObject datosz = my.select(_sql.obten_sentencia(115), null);
            JSONArray zons = (JSONArray) datosz.get("data"); 
            
            JSONArray results = new JSONArray();
            
            for (int fx=0; fx < zons.size(); fx++){
                JSONObject elemento = (JSONObject) zons.get(fx);
                String id_zon = elemento.get("id_zona").toString();
                JSONObject parmszon = new JSONObject();
                parmszon.put("id_zona", id_zon);
                JSONObject puntozon = my.select(_sql.obten_sentencia(116), parmszon);
                JSONArray puntosda = (JSONArray) puntozon.get("data"); 
                JSONObject pieza = new JSONObject();
                pieza.put("id_zon", puntosda);
                results.add(pieza);
            }
            respuesta.put("data", results);
            break;
        case 9: 
            String htmlcad = JSON_parameters.get("1").toString(); 
            Document doc = Jsoup.parse(htmlcad);
            Elements eles = doc.select("img");
            lib.utiles utls = new utiles(); 
            for (int h=0;h<eles.size();h++){
                String imgval = eles.get(h).attr("src");
                String imgName = eles.get(h).attr("data-filename"); 
//                utls.convertImage64ToFile(imgval, "/home/daniel/storeimg/email",imgName);
                utls.convertImage64ToFile(imgval, "/home/drlimon/storeimg/email",imgName);
            }
            respuesta = my.update(consulta, JSON_parameters);
            break; 
    }
    
    out.print(respuesta.toJSONString());

%>