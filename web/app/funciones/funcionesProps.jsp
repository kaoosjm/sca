<%-- 
    Document   : funciones
    Created on : 4/09/2019, 07:56:42 AM
    Author     : daniel
--%>

<%@page import="java.util.Iterator"%>
<%@page import="sentencias.PropiedadesSistema"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="javax.xml.bind.ParseConversionEvent"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<% 
    
    request.setCharacterEncoding("UTF-8");
    
    sentencias.PropiedadesSistema _propS = new PropiedadesSistema();
    
    
    
    String tipo = request.getParameter("tipo");
    String para = request.getParameter("parametros");
    JSONArray JSON_parameters = null;
    JSONArray JSON_resultados = null;
    //-------------------------------------------
    if (para != null) { 
    JSONParser parser = new JSONParser();
    JSON_parameters = (JSONArray) parser.parse(para);
    }
    //-------------------------------------------
    
    int _tipo = Integer.parseInt(tipo); 

    switch(_tipo) { 
        
        case 1: 
            JSON_resultados = new JSONArray(); 
            // la estructura de parametros debe de ser: 
            // {"propiedad": valor},{"propiedad": valor}
            int a=JSON_parameters.size(); 
            for (int x=0; x<a; x++){ 
                JSONObject jso = new JSONObject(); 
                JSONObject item = (JSONObject) JSON_parameters.get(x);
                
                for(Iterator iterator = item.keySet().iterator(); iterator.hasNext();) {
                String key = (String) iterator.next();
//                System.out.println(jso.get(key));
//                String parametro = item.get(key).toString();
//                String propiedad = item.get("propiedad").toString();
                jso.put(key,_propS.getProp(key));
                }
                JSON_resultados.add(jso);
            }
            out.print(JSON_resultados);
            break; 
        
        case 2: 
            a=JSON_parameters.size(); 
            for (int x=0; x<a; x++){ 
                JSONObject jso = new JSONObject(); 
                JSONObject item = (JSONObject) JSON_parameters.get(x);
                
                String propiedad = item.get("propiedad").toString();
                String valor = item.get("valor").toString();
                    _propS.setPropiedad(propiedad, valor);
            }
            JSONObject r= new JSONObject();
            r.put("propiedad", "valor");
            JSON_resultados.add(r);
            out.print(JSON_resultados);
            break;
        
        
    }
    
    



%>