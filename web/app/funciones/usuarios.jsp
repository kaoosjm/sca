<%-- 
    Document   : usuarios
    Created on : 13 sep 2019, 14:32:55
    Author     : daniel.ramos
--%>
<%@page import="lib.JWTManager"%>
<%@page import="lib.JWT_implement"%>
<%@page import="sentencias.Propiedades"%>
<%@page import="lib.BHtokens"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="mysql.mysql"%>
<%@page import="sentencias.sqls"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="javax.naming.directory.Attributes"%>
<%@page import="javax.naming.directory.SearchControls"%>
<%@page import="javax.naming.directory.InitialDirContext"%>
<%@page import="javax.naming.directory.DirContext"%>
<%@page import="javax.naming.directory.SearchResult"%>
<%@page import="javax.naming.NamingEnumeration"%>
<%@page import="javax.naming.NamingException"%>
<%@page import="javax.naming.Context"%>
<%@page import="java.util.Hashtable"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>

<%
    request.setCharacterEncoding("UTF-8");
    JSONObject resp = new JSONObject();    
    lib.BHtokens bht = new BHtokens(); 
    sentencias.sqls _sql = new sqls();
    mysql ms = new mysql();
    
    
    JWTManager jwm = new JWTManager();
    
    String parametros = request.getParameter("p");
    String parametro = request.getParameter("p1");
    String t = request.getParameter("t");
    
    JSONParser parser = new JSONParser();
    JSONObject JSON_parameters = (JSONObject) parser.parse(parametros);
    
    String _ses = request.getSession().getId(); 
    
    sentencias.Propiedades _prop = new Propiedades();
    HttpSession sesion;
    
    
    /***************************************************************************/
    int _tipo_usr = Integer.parseInt(t);
    if (_tipo_usr==1){//externo
    //http://dis.um.es/~lopezquesada/documentos/IES_1213/IAW/curso/UT5/ActividadesAlumnos/12/sesiones.html
        
        JSONObject respuesta = ms.select(_prop.getCommand("101").comando(), JSON_parameters); 
        JSONArray dat = (JSONArray) respuesta.get("data");

        if (dat.size()>0){
            JSONObject dato = (JSONObject) dat.get(0); 
            String cuantos = dato.get("cuantos").toString();
            int _cuantos = Integer.parseInt(cuantos);
            String id = dato.get("id").toString();
            String tkn=""; 
            if (_cuantos>0) {
                //genera el token. 
                //revisa si existe una session
                JSONObject _parms = new JSONObject(); 
                _parms.put("1", _ses); 
                JSONObject r1 = ms.select(_prop.getCommand("102").comando(), _parms); 
                JSONArray d1 = (JSONArray) r1.get("data"); 
                 JSONObject d11 = (JSONObject) d1.get(0); 
                String _c1 = d11.get("cuantos").toString(); 
                String _e1 = d11.get("estado").toString(); 
                String _t1 = d11.get("token").toString(); 
                
                int c1 = Integer.parseInt(_c1); 
                int e1 = Integer.parseInt(_e1);
                
                if (c1>0){
                resp.put("token", _t1);
                out.print(resp.toString());
                } else { 
                    
                tkn = bht.generaToken(id, _ses); 
                resp.put("token", tkn);
                sesion = request.getSession(); 
                out.print(resp.toString());
                }
            }
            
        } else { 
            respuesta = new JSONObject(); 
            respuesta.put("token", "0");
            out.print(respuesta.toString());
        }
    }else if (_tipo_usr==2) {//ldap
        
        
        String user=JSON_parameters.get("1").toString();
        String pass=JSON_parameters.get("2").toString();
        String ldapUri = "ldap://10.15.200.28";
        String usersContainer = "cn="+user+"@presidencia.gob.mx,ou=Presidencia de la República,dc=presidencia,dc=gob,dc=mx";
        String username = user+"@presidencia.gob.mx";
        String password = pass;

        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapUri);
        env.put(Context.SECURITY_PRINCIPAL, username);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            DirContext ctx = new InitialDirContext(env);
            if(ctx != null){
                JSONObject JSON_parameter = (JSONObject) parser.parse(parametro);
                String sentencia = _sql.obten_sentencia(161);
                JSONObject respuesta = ms.select(sentencia, JSON_parameter); 
                JSONArray dat = (JSONArray) respuesta.get("data");
                String tkn=""; 
                
                if (dat.size()>0){
                    JSONObject dato = (JSONObject) dat.get(0);                     
                    String id = dato.get("id").toString();
                    
                    
                    JSONObject _parms = new JSONObject(); 
                    _parms.put("1", _ses); 
                    JSONObject r1 = ms.select(_prop.getCommand("102").comando(), _parms); 
                    JSONArray d1 = (JSONArray) r1.get("data"); 
                    JSONObject d11 = (JSONObject) d1.get(0); 
                    String _c1 = d11.get("cuantos").toString(); 
                    String _e1 = d11.get("estado").toString(); 
                    String _t1 = d11.get("token").toString();
                    
                    int c1 = Integer.parseInt(_c1); 
                    int e1 = Integer.parseInt(_e1); 

                    if (c1>0){
                        
                    resp.put("token", _t1);
                    out.print(resp.toString());
                    
                    } else { 
                    
                    tkn = bht.generaToken(id, _ses); 
                    //tkn = jwm.create_token("6a", "danielramoslimon@gmai.com", "asdrd");
                    
                    
                    resp.put("token", tkn);
                    out.print(resp.toString());
                    
                    }
                    
                    
                } else { 
                    respuesta = new JSONObject(); 
                    respuesta.put("token", "0");
                    out.print(respuesta.toString());
                }
            }else {
                resp = new JSONObject(); 
                resp.put("token", "0");
                out.print(resp.toString());
            }
            
//            tkn = bht.generaToken(id, request.getSession().getId()); 
        }
        catch(Exception e) {
            resp = new JSONObject(); 
            resp.put("token", "-1");
            resp.put("mg", "Par&aacute;metros inv&aacute;lidos, por favor verifiqu&eacute;");
            resp.put("type", "warning");
            out.print(resp.toString());
            e.printStackTrace();
        }
    }
    
%>