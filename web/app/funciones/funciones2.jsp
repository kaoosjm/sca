<%-- 
    Document   : funciones
    Created on : 4/09/2019, 07:56:42 AM
    Author     : daniel
--%>

<%@page import="sentencias.estCommand"%>
<%@page import="sentencias.Propiedades"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="javax.xml.bind.ParseConversionEvent"%>
<%@page import="sentencias.sqls"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="mysql.mysql"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<% 
    request.setCharacterEncoding("UTF-8");
    
    sentencias.Propiedades _prop = new Propiedades();

    JSONObject JSON_parameters = null;
    JSONArray JSON_aparameters = null;
    //---------------------------------------------
    String _index = request.getParameter("i"); 
    estCommand sc = _prop.getCommand(_index);
    String _tipo = sc.tipo(); 
    int tipo = Integer.parseInt(_tipo);
    String consulta = sc.comando(); 
    
    
    String _params = request.getParameter("p");
    
    //---------------------------------------------
    switch(tipo){
        case 1:
            if (_params != null && _params != "" ) { 
            JSONParser parser = new JSONParser();
            JSON_parameters = (JSONObject) parser.parse(_params);
            } 
            break;
        case 2:
            if (_params != null && _params != "" ) { 
            JSONParser parser = new JSONParser();
            JSON_parameters = (JSONObject) parser.parse(_params);
            }
            break; 
        case 3:
            if (_params != null && _params != "" ) { 
            JSONParser parser = new JSONParser();
            JSON_aparameters = (JSONArray) parser.parse(_params);
            }
            break;     
    }; 
        
        
    
    
    //---------------------------------------------
    
    
    JSONObject respuesta; 
    //---------------------------------------------
    mysql my = new mysql(); 
    //---------------------------------------------
    switch (tipo) { 
        case 1: 
        // casos para SELECT
            respuesta = my.select(consulta, JSON_parameters);
            out.print(respuesta.toString());
            break; 
        case 2:
        // case para INSERT, UPDATE
            respuesta = my.update(consulta, JSON_parameters);
            out.print(respuesta.toString());
            break; 
        case 3:
        // case para ingreso de batch
            respuesta = my.update_batch(JSON_aparameters, consulta);
            out.print(respuesta.toString());
            break; 
        case 4: 
        // casos para enviar la cadena de SQL- 
            consulta = consulta.replace("[]", JSON_parameters.get("1").toString());
            respuesta = my.select(consulta);
            out.print(respuesta.toString());
            break;
    }
%>