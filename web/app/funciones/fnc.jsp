<%@page import="sentencias.Propiedades"%>
<%@page import="lib.BHtokens"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="mysql.mysql"%>
<%@page import="sentencias.sqls"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
    request.setCharacterEncoding("UTF-8");
    JSONObject resp = new JSONObject();    
    lib.BHtokens bht = new BHtokens(); 
    sentencias.sqls _sql = new sqls();
    mysql ms = new mysql();
    
    String parametros = request.getParameter("p");
    JSONParser parser = new JSONParser();
    JSONObject JSON_parameters = (JSONObject) parser.parse(parametros);
    String _tipo = request.getParameter("t");
    int tipo = Integer.parseInt(_tipo);
    int tu=0;
    String mensaje="";
    
    sentencias.Propiedades _prop = new Propiedades();
    
    try {
    switch (tipo) {
        case 1:          
            
            JSONObject respuesta = ms.select(_prop.getCommand("100").comando(), JSON_parameters); 
            
            JSONArray dat = (JSONArray) respuesta.get("data"); 
            if (dat.size()>0){
                JSONObject dato = (JSONObject) dat.get(0); 
                String cuantos = dato.get("cuantos").toString();
                String tipo_usr = dato.get("tipo_usr").toString();
                tu = Integer.parseInt(tipo_usr);                
                resp.put("tu", tipo_usr);
                out.print(resp.toString());
            } else {
                respuesta = new JSONObject();               
                if (tu==0){
                    respuesta.put("tu", "0");
                    respuesta.put("mg", "Usuario no registrado");
                    respuesta.put("type", "warning");
                }
                out.print(respuesta.toString());
            }
            break;
    }
    } catch (Exception e){
        resp.put("tu", "-1");
        resp.put("mg", "Disculpe las molestias sitio en mantenimiento");
        resp.put("type", "error");
        out.print(resp.toString());
    }
%>