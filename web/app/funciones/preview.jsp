<%-- 
    Document   : preview
    Created on : 27/04/2021, 07:13:10 PM
    Author     : Daniel Ramos Limón
--%>

<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@page import="lib.PreviewFiles"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
String filename = request.getParameter("filename");
PreviewFiles p = new PreviewFiles();
String r = p.convertFile(filename);
out.print(r.trim());
%>