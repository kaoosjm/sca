<%@page import="lib.handleIndexPage"%>
<%@page import="com.sun.mail.handlers.handler_base"%>
<%@page import="lib.UserApp"%>
<%@page import="lib.JWTManager"%>
<%@page import="lib.ErrorExisteSesionException"%>
<%@page import="sentencias.estCommand"%>
<%@page import="sentencias.Propiedades"%>
<%@page import="lib.ErrorSesionException"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="lib.BHtokens"%>
<%@page session="true"%>
<jsp:useBean id="db" class="mysql.mysql" scope="page"></jsp:useBean>
<jsp:useBean id="_sql" class="sentencias.sqls" scope="page"></jsp:useBean>
<%@page session="true" %>

<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
%>
 
<%
    handleIndexPage p = new handleIndexPage();
    JSONObject parametros = new JSONObject(); 
    JSONObject respuesta = new JSONObject();
    sentencias.Propiedades _prop = new Propiedades();
    
    //si no existe el token manda el error de ErrorSessionException
    if (request.getParameter("tkn")==null){
        throw new ErrorSesionException("No existe manejo de sesi�n"); 
    }; 
    
    //revisa si la session esta presente en base de datos y esta activa (estado = 0)
    parametros.put("1",session);
    
    //si existe el token ... 
    String tkn=request.getParameter("tkn");
    String ses = request.getSession().getId(); 
    
    JSONObject parms0 = new JSONObject();
    JSONArray datos = new JSONArray(); 
    JSONObject registro = new JSONObject();
    //---------------------------------------------------------------------------
    
    BHtokens bht = new BHtokens(); 
    String token = bht.decript64(tkn);
    JSONParser parser = new JSONParser();
    JSONObject partstkn = new JSONObject();
    partstkn = (JSONObject) parser.parse(token);
    
    //obtiene los datos del usario directamente del token! 
    String idusr = partstkn.get("id").toString();
    String _tken = partstkn.get("token").toString();
    
    parametros.put("1", idusr);
    respuesta = db.select(_prop.getCommand("103").comando(), parametros);   
    datos = (JSONArray) respuesta.get("data"); 
    //---------------------------------------------------------------------------
    
    String id_avatar =  "";
    String id_app =     "";
    String id_perfil =  "";
    String id_usr =     "";
    String nombre_completo =  "";
    String nombre =  "";
    String email =   ""; 
    HttpSession sesion = request.getSession();
    
    // aqui verifica que el token es v�lido
    boolean validaToken = bht.validaToken(_tken, ses);
    validaToken = true;
    if (validaToken == false) { 
        throw new ErrorSesionException("La sesi�n ha caducado"); 
    } else { 
        //aqui es valido el token 
    // -------------------------- si es valido el token  ------------------------
         
        registro = (JSONObject) datos.get(0); 
        id_avatar =  registro.get("id_avatar").toString(); 
        id_app =     registro.get("id_app").toString(); 
        id_perfil =  registro.get("id_perfil").toString(); 
        id_usr =     registro.get("id").toString(); 
        nombre_completo =     registro.get("nombre").toString()+registro.get("apellidos").toString(); 
        nombre =     registro.get("nombre").toString(); 
        email =      registro.get("email").toString(); 

        _tken = bht.generaToken(id_usr, ses);

        JSONObject parmifses = new JSONObject(); 
        parmifses.put("0", ses); 
        JSONObject rspiss = db.select(_prop.getCommand("102").comando(), parmifses);
        JSONArray datos_c = (JSONArray) rspiss.get("data"); 
        JSONObject d = (JSONObject) datos_c.get(0); 

        int cuantos = Integer.parseInt(d.get("cuantos").toString());
        int estado = Integer.parseInt(d.get("estado").toString());

        if (cuantos==0 && estado==0) {
            JSONObject parmst = new JSONObject(); 
            parmst.put("1", ses);
            parmst.put("2", id_usr);
            parmst.put("3", tkn);
            JSONObject rspupd = db.update(_prop.getCommand("104").comando(), parmst);
        } else if (cuantos>0 && estado==1){ 
            throw new ErrorExisteSesionException("Ya existe una session Activa"); 
        }

        //---------------------------------------------------------------------------
        parms0.put("1", ses);
        respuesta = db.select(_prop.getCommand("105").comando(), parms0);
        datos = (JSONArray) respuesta.get("data"); 
        //---------------------------------------------------------------------------
        if (datos.size()==0) { 
            //codigo para cuando no existe sesion
        } else { 
            //codigo para cuando si existe session
        };        
    }// --------------------------- fin del bloque donde es valido el token ------------------    
%>    
<!DOCTYPE html>
<html lang="es">
    <head>
      <!-- Required meta tags -->
      <!--<meta charset="utf-8">-->
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
      <meta http-equiv="Script-Content-Type" content="text/javascript; charset=iso-8859-1"> 
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <!-- Twitter -->
      <meta name="twitter:site" content="@themepixels">
      <meta name="twitter:creator" content="@themepixels">
      <meta name="twitter:card" content="summary_large_image">
      <meta name="twitter:title" content="Bracket Plus">
      <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
      <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

      <!-- Facebook -->
      <meta property="og:url" content="http://themepixels.me/bracketplus">
      <meta property="og:title" content="Bracket Plus">
      <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

      <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
      <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
      <meta property="og:image:type" content="image/png">
      <meta property="og:image:width" content="1200">
      <meta property="og:image:height" content="600">

      <!-- Meta -->
      <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
      <meta name="author" content="ThemePixels">

      <title>SCA - OPR/DGTI</title>

      <!-- vendor css -->
      <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
      <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">
      <link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
      <link href="../lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
      <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
      <link href="../lib/highlightjs/github.css" rel="stylesheet">
      <link href="../lib/sweetalert/sweetalert2.css" rel="stylesheet">
      <link href="../lib/jquery-toggles/toggles-full.css" rel="stylesheet">

      <!-- pivottable css -->
      <link href="../css/pivot.min.css" rel="stylesheet">
      <!-- datatables css -->
      <link href="../lib/datatables/jquery.dataTables.css" rel="stylesheet">
      <link href="../lib/datatables-responsive/responsive.dataTables.scss" rel="stylesheet">

      <!-- fileupload css -->
      <link href="../js/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
      <link href="../js/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" />

      <!-- Steps CSS -->
      <link href="../lib/jquery.steps/jquery.steps.css" rel="stylesheet">
      <link href="../lib/summernote/summernote-bs4.css" rel="stylesheet">
      <link rel="stylesheet" href="../css/tree.css">
      <link href="../lib/jt.timepicker/jquery.timepicker.css" rel="stylesheet">
      <!-- Bracket CSS -->
      <link rel="stylesheet" href="../css/bracket.css">
      <link rel="stylesheet" href="../css/own.css">
      <!--begin:: Flipbook -->
      <link href="../assets/flipbook/css/flipbook.style.css" rel="stylesheet" type="text/css" >
      <link href="../assets/flipbook/css/font-awesome.css" rel="stylesheet" type="text/css" >
      <!--end:: Flipbook -->
      <link rel="icon" href="../img/favicon.ico"/>
      <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    </head>

    <body data-avatar="<%=id_avatar%>" data-app="<%=id_app%>" data-perfil="<%=id_perfil%>" data-id="<%=id_usr%>" data-ses="<%=ses%>" data-email="<%=email%>">
        <!-- ########## START: LEFT PANEL ########## -->
        <!--<div class="br-logo"><a href=""><span>[</span>SII<i>presidencia</i><span>]</span></a></div>-->
        <div class="br-logo">
            <a href="">
                <!--<span>[</span>SIININ<span>]</span>-->
                <img src="../img/logo-mexico-blanco.png" alt="" style="width: 177.5px; height: auto">
            </a>
        </div>
        <div class="br-sideleft overflow-y-auto">
            <label class="sidebar-label pd-x-10 mg-t-20 op-3">Navegaci�n</label>
            <ul id="menuc" class="br-sideleft-menu">            
                <%= db.obtenMenuApp() %>
            </ul><!-- br-sideleft-menu -->
            <br>
        </div><!-- br-sideleft -->
        <!-- ########## END: LEFT PANEL ########## -->

        <!-- ########## START: HEAD PANEL ########## -->
        <div class="br-header">
          <div class="br-header-left">
            <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
            <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
            <div class="wd-380 transition" style="display: flex; align-items: center; justify-content: center;padding-left: 10px;">
                <h4>Sistema de Control de Acceso <i><u><%= sesion.getId() %></u></i></h4>
            </div>
          </div><!-- br-header-left -->
          <div class="br-header-right">
            <nav class="nav">
              <div class="dropdown"></div><!-- dropdown -->
              <div class="dropdown"></div><!-- dropdown -->
              <div class="dropdown">
                <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                  <span class="logged-name hidden-md-down"><%= nombre %></span>
                  <img id="usrav" src="../avatar?getthumb=<%=id_avatar%>.png" class="wd-32 rounded-circle" alt="">
                  <span class="square-10 bg-success"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-header wd-250">
                    <div class="tx-center">
                        <a href=""><img id="imgavtb" src="../avatar?getthumb=<%=id_avatar%>.png" class="wd-80 rounded-circle" alt=""></a>
                        <h6 class="logged-fullname"><%=nombre_completo%></h6>
                        <p><%=email%></p>
                    </div>
                    <hr>
                    <ul class="list-unstyled user-profile-nav">
                        <li><a id="finses" href="javascript:void(0)"><i class="icon ion-power"></i> Salir</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
              </div><!-- dropdown -->
            </nav>
          </div><!-- br-header-right -->
        </div><!-- br-header -->
        <!-- ########## END: HEAD PANEL ########## -->

        <!-- ########## START: RIGHT PANEL ########## -->
        <div class="br-sideright">
          <ul class="nav nav-tabs sidebar-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" role="tab" href="#contacts"><i class="icon ion-ios-contact-outline tx-24"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" role="tab" href="#attachments"><i class="icon ion-ios-folder-outline tx-22"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" role="tab" href="#calendar"><i class="icon ion-ios-calendar-outline tx-24"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" role="tab" href="#settings"><i class="icon ion-ios-gear-outline tx-24"></i></a>
            </li>
          </ul><!-- sidebar-tabs -->           
        </div><!-- br-sideright -->
        <!-- ########## END: RIGHT PANEL ########## --->

        <!-- ########## START: MAIN PANEL ########## -->
        <div class="br-mainpanel">
            <div class="br-pageheader">
                <nav class="breadcrumb pd-0 mg-0 tx-12">
                  <a id="breadcru1" class="breadcrumb-item" href="javascript:void(0)"></a>
                  <span id="breadcru2" class="breadcrumb-item active"></span>
                </nav>
            </div><!-- br-pageheader -->     
            <!-- start you own content here -->
            <div id="principalc" class="br-pagebody">
            </div><!-- br-pagebody -->
            <!-- end you own content here -->
        </div><!-- br-mainpanel -->
        <!-- ########## END: MAIN PANEL ########## -->

        <div id="modalfinses" class="modal fade">
            <div class="modal-dialog modal-dialog-vertical-center" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Alerta</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-25">
                  <h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">�Desea cerrar la sesi�n?</a></h4>
                  <p class="mg-b-5">En caso de no ser asi, pulse cancelar para mantener la session activa. </p>
                </div>
                <div class="modal-footer">
                  <button type="button" id="btnendses" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Cerrar sesi�n</button>
                  <button type="button" id="btnclosem" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Cancelar</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->

        <div id="modalinicio" class="modal fade">
            <div class="modal-dialog modal-dialog-vertical-center" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Bienvenido</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-25">
                    <div class="row">
                        <div class="col-4">
                            <img class="img-thumbnail" src="../img/svg/analytics.svg">
                        </div>
                        <div class="col-8">
                            <h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Sistema de Control de Acceso SCA</a></h4>
                            <p class="mg-b-5">Aplicaci�n para el registro de accesos a edificios de Palacio Nacional con la finalidad de obtener informaci�n estadistica</p>                    
                        </div>                    
                    </div>            
                </div>
                <div class="modal-footer">              
                  <button type="button" id="btnclosem" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">cerrar</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
        </div><!-- modal -->

        <div id="modaldisclaimer" class="modal fade">
            <div class="modal-dialog modal-dialog-vertical-center" role="document">
              <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                  <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Disclaimer</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body pd-25">
                    <div class="row">
                        <div class="col-4">
                            <img class="img-thumbnail" src="../img/disclaimer.gif">
                        </div>
                        <div class="col-8">
                            <h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">Sistema de Control de Acceso SCA</a></h4>
                            <p class="mg-b-5">Aplicaci�n para el registro de accesos a edificios de Palacio Nacional con la finalidad de obtener informaci�n estadistica</p>                    
                        </div>                   
                    </div>            
                </div>
                <div class="modal-footer">              
                  <button type="button" id="btnclosem" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Aceptar</button>
                </div>
              </div>
            </div><!-- modal-dialog -->
          </div><!-- modal -->            

        <script src="../lib/jquery/jquery.js"></script>
        <script src="../lib/pooper/popper.min.js"></script>
        <script src="../lib/bootstrap/js/bootstrap.js"></script>
        <script src="../lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
        <script src="../lib/moment/moment.js"></script>
        <script src="../lib/jquery-ui/jquery-ui.js"></script>
        <script src="../lib/jquery-switchbutton/jquery.switchButton.js"></script>
        <script src="../lib/peity/jquery.peity.js"></script>
        <script src="../lib/highlightjs/highlight.pack.js"></script>

        <!-- fileupload  -->
        <script src="../js/jquery-file-upload/jquery.ui.widget.js" type="text/javascript"></script>
        <script src="../js/jquery-file-upload/jquery.fileupload.js" type="text/javascript"></script>
        <script src="../js/jquery-file-upload/jquery.fileupload.ui.js" type="text/javascript"></script>
        <script src="../js/jquery-file-upload/jquery.iframe-transport.js" type="text/javascript"></script>

        <!-- steps  -->
        <script src="../lib/jquery.steps/jquery.steps.js"></script>
        <script src="../lib/summernote/summernote-bs4.js"></script>

        <!-- raphaeljs  -->
        <script src="../js/raphael.min.js"></script>
        <script src="../lib/jquery-toggles/toggles.min.js"></script>

        <script src="../lib/select2/js/select2.min.js"></script>
        <script src="../lib/datatables/jquery.dataTables.js"></script>
        <script src="../lib/datatables-responsive/dataTables.responsive.js"></script>
        <script src="../lib/parsleyjs/parsley.js"></script>
        <script src="../lib/papaparse/papaparse.min.js"></script>
        <script src="../lib/sweetalert/sweetalert2.all.min.js"></script>
        <script src="../js/imask.js"></script>
        <script src="../lib/chart.js/Chart.js"></script>
        <script src="../js/es.js"></script>

        <script src="../js/pivot.min.js"></script>

        <script src="../js/jquery.blockUI.js"></script>
        <script src="../js/raphael.min.js"></script>
        <script src="../lib/jt.timepicker/jquery.timepicker.js"></script>
        <script src="../js/tree.js"></script>
        <script src="../js/bracket.js"></script>
        <script src="../js/own.js"></script>
    </body>
</html>