<%-- 
    Document   : global
    Created on : 16/03/2021, 06:57:19 PM
    Author     : OPR 2020
--%>
<%@page import="sun.misc.BASE64Decoder"%>
<%@page import="lib.BHtokens"%>
<jsp:useBean id="db" class="mysql.mysql" scope="page"></jsp:useBean>
<jsp:useBean id="_sql" class="sentencias.sqls" scope="page"></jsp:useBean>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%

%>
<!DOCTYPE html>
<div class="row">

    <div class="col-lg-12 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div id="portletrpt" class="portlet light portlet-fit bordered" style="height: calc(100vh - 182px);overflow-y: auto;">
<!--            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Reporte de Usuarios</span>                    
                </div>
                <div class="actions">
                    <a id="content-map-export" href="javascript:;" class="btn green-soft btn-sm"><i class="fa fa fa-file-excel-o"></i> Exportar Excel </a>
                    <a href="javascript:;" class="btn btn-circle btn-default" id="btnreload">
                        <i class="fa fa-reload"></i> Recargar reporte </a>                    
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="" title="" > </a>
                </div>
            </div>-->
            <div class="portlet-body">
                <div class="col-lg-12 col-xs-12 col-sm-12">
                    <object id="content-map" class="mh-100 d-inline-block" style="overflow: hidden; width: 100%;" type="application/pdf"></object>
<!--                    <object id="content-map" class="mh-100 d-inline-block" style="overflow: hidden; width: 100%;" type="application/pdf" onload="finublock();"></object>-->
                    <div id="contenidopdf" style="width: 100%;"></div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>