var newURL = window.location.protocol + "//" + window.location.host + "/";
var pathArray = window.location.pathname.split( '/' );
var secondLevelLocation = pathArray[1];
var ubicacion = newURL+secondLevelLocation;

var urlrpt;
var permxs;
var rpts = function () {
    
    var iniciaublock = function(){
        $.blockUI({
            target: '#portletrpt',
            boxed: true,
            message: 'Generando reporte...'
        });
    };
    
    var inicializa = function () {
        let _us = sessionStorage.getItem("email");
        let p={"1": _us};
        
        get("32",p,function(data){
            let np = data.data.length;
            if (np===0) { 
                Swal.fire(
                    'El usuario no tiene áreas asignadas',
                    '',
                    'error'
                );
            } else {
                permxs = data.data[0].permisos;
                iniciaublock();
            }
        });
        
        var urlrpt = ubicacion+"/app/reportes/pdf.jsp?t=1&i="+sessionStorage.getItem("idu")+"&x="+permxs;
        iniciaublock();
        $("#content-map").css("visibility", "visible");
        $("#contenidopdf").css("visibility", "hidden");               
        $('#content-map').attr('data', urlrpt+"&a=p#page=1&zoom=130");

        currentHeight = $('#portletrpt').outerHeight();
        $("#content-map").height(currentHeight-10);//122

        $( window ).resize(function() {                        
            currentHeight = $('#portletrpt').outerHeight();
            $("#content-map").height(currentHeight-10);//122
        });
        
        $('#content-map-export').attr('href', urlrpt+"&a=x");        
    };
    
    return {
        init: function () {
            inicializa();            
        }
    };
    
}();

jQuery(document).ready(function () {
    rpts.init();
});