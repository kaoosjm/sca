<%@page contentType="application/pdf" pageEncoding="UTF-8"%>
<jsp:useBean id="db" class="mysql.mysql" scope="page"></jsp:useBean>
<jsp:useBean id="_sql" class="sentencias.sqls" scope="page"></jsp:useBean>
<%@page import="com.lowagie.text.pdf.PdfWriter"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="net.sf.jasperreports.engine.util.JRStyledTextParser"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRXlsExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRPdfExporter"%>
<%@page import="net.sf.jasperreports.engine.export.JRPdfExporterParameter"%>
<%--<%@page import="net.sourceforge.barbecue.*"%>  codigo de barras--%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Vector" %>
<%--<%@page import="java.io.*"%>--%>
<%--<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.*"%>--%>
<%--<%@page import="org.jfree.chart.*"%>
<%@page import="org.jfree.data.*"%>--%>
<%
try {
        //----------------------------------------------------------------------
        String path = request.getContextPath();  
        String getProtocol=request.getScheme();
        String getDomain=request.getServerName();
        String getPort=Integer.toString(request.getServerPort());
        String getPath = getProtocol+"://"+getDomain+":"+getPort+path+"/";
        //----------------------------------------------------------------------
        Connection con = db.getConnectionDB();
        File reportFile = null;
        Map parameters = new HashMap();
        //----------------------------------------------------------------------
        String img1 = getPath+"img/mexico-presidencia.png";
        String img2 = getPath+"img/alas-50.png";
        
        String tipo = "";
        String arch = "";
        int permxs = 0;
        String name_file = "";
        
        if (request.getParameter("t")!=null ){
            tipo = request.getParameter("t");
        }
        
        if (request.getParameter("a")!=null ){
            arch = request.getParameter("a");
        }
        
        if (request.getParameter("x")!=null ){
            permxs = Integer.parseInt(request.getParameter("x"));
        }
        
        switch (Integer.parseInt(tipo)){
            case 1: //Global
                name_file = "Global";
                String filtro = " where a.id_subarea="+permxs+" ";
                //***************************
                reportFile = new File(application.getRealPath("app//reportes//global//Global.jasper"));
                parameters.put("filtro",filtro);
                parameters.put("imagen1",img1);
                parameters.put("imagen2",img2);
                //***************************
            break;
        }
        //**********************************************************************
        JasperPrint print = JasperFillManager.fillReport(reportFile.getPath(), parameters, con);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        //----------------------------------------------------------------------
        if (arch.equals("x")){
            JRXlsExporter exporterXLS = new JRXlsExporter(); //tipo archivo excel
            exporterXLS.setParameter(JRPdfExporterParameter.JASPER_PRINT, print);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
    //        exporterPDF.setParameter(JRXlsExporterParameter.PASSWORD,"ireri1118");
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
            exporterXLS.exportReport();
            ServletOutputStream outputStream = response.getOutputStream(); 
                
            response.setContentType("application/xls");
            response.setHeader("Content-Disposition","inline; filename=\""+name_file+".xls\"");
            response.setContentLength(byteArrayOutputStream.toByteArray().length);   
            outputStream.write(byteArrayOutputStream.toByteArray());    
            outputStream.flush();
            outputStream.close();
        }else if (arch.equals("p")) {
            JRPdfExporter exporterPDF = new JRPdfExporter(); //tipo archivo pdf
            exporterPDF.setParameter(JRPdfExporterParameter.JASPER_PRINT, print);
            exporterPDF.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterPDF.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
    //        exporterPDF.setParameter(JRXlsExporterParameter.PASSWORD,"ireri1118");
            exporterPDF.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
            exporterPDF.exportReport();
            ServletOutputStream outputStream = response.getOutputStream(); 

            response.setContentType("application/pdf");
//            response.setHeader("Content-Disposition","inline; filename=\""+name_file+".pdf\"");
//            response.setContentLength(byteArrayOutputStream.toByteArray().length);   
//            outputStream.write(byteArrayOutputStream.toByteArray());    
//            outputStream.flush();
//            outputStream.close();
//            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition","inline; filename=Prueba.pdf");
//            response.setHeader("Content-Disposition","attachment; filename=PruebaExcel.xls");
            response.setContentLength(byteArrayOutputStream.toByteArray().length);
            response.getOutputStream().write(byteArrayOutputStream.toByteArray());
            response.getOutputStream().flush();
            response.getOutputStream().close();
            
        }
        con.close();
    }catch (Exception e){
        String error = e.getMessage();
        out.println(e.getMessage());
    }
%>