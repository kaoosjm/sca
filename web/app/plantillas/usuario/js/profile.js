try { 
var profile = function(){
    
    var avt_ini; 
    var avt_sel; 
    var _idus; 
    
    
    
    var modifica_avatar = function(){
        var param = { "1":avt_sel,"2":_idus};
        $.get("funciones/funciones.jsp", {'i':94, 't':2,'p': JSON.stringify(param) }, function(data){
            if (data.error_msg.length<1){ 
                    document.querySelector('body').dataset.avatar = avt_sel; 
                    document.getElementById('usrav').src = '../avatar?getthumb='+avt_sel+'.png'; 
                    document.getElementById('imgavtb').src = '../avatar?getthumb='+avt_sel+'.png'; 
                    avt_ini = avt_sel; 
                    Swal.fire({
                    type: 'success',
                    text: 'Avatar modificado',
                    })
                
            } else { 
                
                Swal.fire({
                type: 'error',
                text: data.error_msg
                }); 
            }
        });
        
    };
    
    var modifica_pass = function(){
        var passwd = document.getElementById("pass1").value; 
        var param = { "1":passwd,"2":_idus};
        $.get("funciones/funciones.jsp", {'i':97, 't':2,'p': JSON.stringify(param) }, function(data){
            if (data.error_msg.length<1){ 
                    document.getElementById('pass1').value = null; 
                    document.getElementById('pass2').value = null; 
                    Swal.fire({
                    type: 'success',
                    text: 'password modificado',
                    })
            } else { 
                Swal.fire({
                type: 'error',
                text: data.error_msg
                }); 
            }
        });
    };  
    
    var modifica_email = function(){
        var email = document.getElementById("email1").value; 
        var param = { "1":email,"2":_idus};
        $.get("funciones/funciones.jsp", {'i':96, 't':2,'p': JSON.stringify(param) }, function(data){
            if (data.error_msg.length<1){ 
                    document.querySelector('body').dataset.email = email; 
                    document.getElementById('labelemail').innerHTML = email; 
                    
                    document.getElementById('email1').value = null; 
                    document.getElementById('email2').value = null; 
                    
                    
                    
                    Swal.fire({
                    type: 'success',
                    text: 'email modificado',
                    })
                
            } else { 
                Swal.fire({
                type: 'error',
                text: data.error_msg
                }); 
            }
        });
    };  
        
    var valida_forma_avatar = function(){ 
                if (parseInt(avt_ini,10) === parseInt(avt_sel,10)){
                    Swal.fire({
                    type: 'error',
                    text: 'No se ha seleccionado un avatar diferente',
                    })
                } else { 
                    modifica_avatar();
                }
        }; 
    
    var mail_existe = function(email, callback){
        var param = { "1":email};
        var cuantos; 
        $.get("funciones/funciones.jsp", {'i':95, 't':1,'p': JSON.stringify(param) }, function(data){
            cuantos = data.data[0].cuantos; 
            callback(cuantos);
        });
    }; 
    
    var valida_forma_email = function(){ 
        
                var m1 = document.getElementById("email1");
                var m2 = document.getElementById("email2");
                
                var c=0; 
                c += valida_equal(m1,m2, "Las cuentas de correo no coinciden"); 
                c += valida_mail(m1,"La cuenta de correo es invalida"); 
                c += valida_req(m2,"La cuenta de correo es requerida"); 
                
                var email1 = document.getElementById("email1").value;
                
                if (c>0){ 
                    setTimeout(function(){
                        oculta("formaemail");
                    },800); 
                } else{ 
                       mail_existe(email1, function(cuantos){ 
                        if (parseInt(cuantos,10)>0) { 
                            
                            
                            valida_msg(m1,"la cuenta de correo ya existe en sistema");
                            setTimeout(function(){
                                oculta("formaemail");
                            },800); 
                               
                           
                        } else { 
                            modifica_email();
                        }
                   });
                }; 
        }; 
        
    var valida_forma_pass = function(){ 
        
                var p1 = document.getElementById("pass1");
                var p2 = document.getElementById("pass2");
                
                var c=0; 
                c += valida_req(p1,"El valor es requerido"); 
                c += valida_req(p2,"El valor es requerido"); 
                c += valida_equal(p1,p2, "Las valores no coinciden"); 
                
                if (c>0){ 
                    setTimeout(function(){
                        oculta("formpassc");
                    },800); 
                } else{ 
                        modifica_pass(); 
                }; 
        }; 
    
    
    var eventos = function(){ 
        var item = document.querySelectorAll(".avatar-items"); 
        item.forEach(function(ele){ 
            ele.addEventListener('click',function(){
               avt_sel = ele.dataset.valor;  
               document.getElementById("imgsrcc").src = "../avatar?getthumb="+ele.dataset.valor+".png"; 
               document.getElementById("dropdown-selector").checked = false;
            });
        });
        
        var tx = document.getElementById("btn_cambia_avatar"); 
        tx.addEventListener('click',function(){ 
                valida_forma_avatar();
        });
        
        var tx = document.getElementById("btn_cambia_email"); 
        tx.addEventListener('click',function(){ 
                valida_forma_email();
        });
        
        var tx = document.getElementById("btn_cambia_pass"); 
        tx.addEventListener('click',function(){ 
                valida_forma_pass();
        });
        
        
        
        
    };
    
    var inicia = function(){ 
        var _avatar_ = document.querySelector('body').dataset.avatar;  
        var _email_ = document.querySelector('body').dataset.email;  
        _idus = document.querySelector('body').dataset.id;  
        document.getElementById("labelemail").innerHTML = _email_;
        avt_ini =  _avatar_;
        avt_sel =  _avatar_;
        document.getElementById("imgsrcc").src = "../avatar?getthumb="+_avatar_+".png";
    };
    
    return { 
        init: function() { 
        inicia();
        eventos();
        }
    };
}(); 

jQuery(document).ready(function () {
    profile.init();
});

} catch(error){
    console.log(error.message);
}    

        