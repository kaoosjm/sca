<%-- 
    Document   : profile
    Created on : 6 nov 2019, 16:46:19
    Author     : drlimon
--%>

<%
  
    
    
    
    
%>    


<style>
    
.containercontainerxsd {
  padding: 10px;
}

.avatar-user {
  width: 30px;
  height: 30px;
  background-color: #A8D083;
  border-radius: 3px;
  margin-right: 10px;
  margin-bottom: 20px;
}

.avatar-user, .dropdown {
  display: inline-block;
  vertical-align: top;
}

.dropdown {
  position: relative;
}

.dropdown-input {
  display: none;
}
.dropdown-items {
  list-style: none;
  border-bottom: 1px solid #999999;
  
  &:last-child {
    border-bottom: none;
  }
}

.dropdown-content, .dropdown-avatar {
  margin: 0;
  padding: 0;
}

.dropdown-content {
  box-shadow: 0 0 0 1px #999 inset;
  width: 164px;
  max-height: 0;
  overflow: hidden;
  position: absolute;
  left: 0;
  top: 100%;
  border-radius: 0 3px 3px 3px;
  transition: max-height 0.5s ease;
  margin-top: -1px;
  overflow: auto;
  background-color: #FFFFFF;
}

.dropdown-avatar {
  padding-left: 10px;
  padding-top: 5px;
  padding-bottom: 5px;
}

.p10 {
  display: inline-block;
  padding: 10px;
  margin: 5px 0;

}

.label-dropdown {
  padding: 5px 10px;
  border-radius: 3px;
  background-image: linear-gradient(-179deg, #FFFFFF 0%, #EBEBEB 100%);
  border: 1px solid #999999; 
  cursor: pointer;
  display:block;
}

.dropdown-input:checked ~ .dropdown-content {
  max-height: 160px;
}

.dropdown-input:checked ~.label-dropdown {
  border-radius: 3px 3px 0 0;
  background: #DDDDDD;
  border: 1px solid #999999;
  box-shadow: inset 0px 2px 0px 0px #CCCCCC;
}

.avatar-items {
  display: inline-block;
  list-style: none;
  width: 30px;
  height: 30px;
  float: left;
  margin-right: 10px;
  margin-bottom: 10px;
  cursor: pointer;
  border-radius: 2px;
  
  &:hover {
    border: 2px solid #3A92DC;
  }
}

.dropdown-avatar:after, .dropdown-avatar:before {
  content:'';
  display: table
}
.dropdown-avatar:after {
  clear: both;
}

.green {
  background-color: #FFFFFF;
}

    
</style>
<!--https://codepen.io/gemaderus/pen/xagDv-->

<div class="col-lg-12 mg-t-30 mg-lg-t-0">
              <div class="card pd-20 pd-xs-30 shadow-base bd-0">
                <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-13 mg-b-25">Información de contacto</h6>
                
                
                
                <div class="form-layout form-layout-4">    
                    
                   <div class="containerxsd">
                    <div  id="avt" class="avatar-user" style="width: 75px; height: 75px"><img  id="imgsrcc" src=""></div>
                <div class="dropdown">
                    <input type="checkbox" id="dropdown-selector" class="dropdown-input"/>

                  <label style="margin-left: 10px" for="dropdown-selector" class="label-dropdown">Cambiar avatar <span class="fa fa-caret-down"></span></label>

                  <ul class="dropdown-content">
                      <ul id="asdcx" class="dropdown-avatar">
                          <li class="avatar-items green" data-valor="0"><img width="30px" height="30px" src='../avatar?getthumb=0.png'</li>
                          <li class="avatar-items green" data-valor="1"><img width="30px" height="30px" src='../avatar?getthumb=1.png'</li>
                          <li class="avatar-items green" data-valor="2"><img width="30px" height="30px" src='../avatar?getthumb=2.png'</li>
                          <li class="avatar-items green" data-valor="3"><img width="30px" height="30px" src='../avatar?getthumb=3.png'</li>
                          <li class="avatar-items green" data-valor="4"><img width="30px" height="30px" src='../avatar?getthumb=4.png'</li>
                          <li class="avatar-items green" data-valor="5"><img width="30px" height="30px" src='../avatar?getthumb=5.png'</li>
                          <li class="avatar-items green" data-valor="6"><img width="30px" height="30px" src='../avatar?getthumb=6.png'</li>
                          <li class="avatar-items green" data-valor="7"><img width="30px" height="30px" src='../avatar?getthumb=7.png'</li>
                          <li class="avatar-items green" data-valor="8"><img width="30px" height="30px" src='../avatar?getthumb=8.png'</li>
                          <li class="avatar-items green" data-valor="9"><img width="30px" height="30px" src='../avatar?getthumb=9.png'</li>
                          <li class="avatar-items green" data-valor="10"><img width="30px" height="30px" src='../avatar?getthumb=10.png'</li>
                          <li class="avatar-items green" data-valor="11"><img width="30px" height="30px" src='../avatar?getthumb=11.png'</li>
                          <li class="avatar-items green" data-valor="12"><img width="30px" height="30px" src='../avatar?getthumb=12.png'</li>
                          <li class="avatar-items green" data-valor="13"><img width="30px" height="30px" src='../avatar?getthumb=13.png'</li>
                          <li class="avatar-items green" data-valor="14"><img width="30px" height="30px" src='../avatar?getthumb=14.png'</li>
                          <li class="avatar-items green" data-valor="15"><img width="30px" height="30px" src='../avatar?getthumb=15.png'</li>
                          <li class="avatar-items green" data-valor="16"><img width="30px" height="30px" src='../avatar?getthumb=16.png'</li>
                          <li class="avatar-items green" data-valor="17"><img width="30px" height="30px" src='../avatar?getthumb=17.png'</li>
                          <li class="avatar-items green" data-valor="18"><img width="30px" height="30px" src='../avatar?getthumb=18.png'</li>
                          <li class="avatar-items green" data-valor="19"><img width="30px" height="30px" src='../avatar?getthumb=19.png'</li>
                          <li class="avatar-items green" data-valor="20"><img width="30px" height="30px" src='../avatar?getthumb=20.png'</li>
                          <li class="avatar-items green" data-valor="21"><img width="30px" height="30px" src='../avatar?getthumb=21.png'</li>
                          <li class="avatar-items green" data-valor="22"><img width="30px" height="30px" src='../avatar?getthumb=22.png'</li>
                          <li class="avatar-items green" data-valor="23"><img width="30px" height="30px" src='../avatar?getthumb=23.png'</li>
                          <li class="avatar-items green" data-valor="24"><img width="30px" height="30px" src='../avatar?getthumb=24.png'</li>
                          <li class="avatar-items green" data-valor="25"><img width="30px" height="30px" src='../avatar?getthumb=25.png'</li>
                          <li class="avatar-items green" data-valor="26"><img width="30px" height="30px" src='../avatar?getthumb=26.png'</li>
                          <li class="avatar-items green" data-valor="27"><img width="30px" height="30px" src='../avatar?getthumb=27.png'</li>
                          <li class="avatar-items green" data-valor="28"><img width="30px" height="30px" src='../avatar?getthumb=28.png'</li>
                          <li class="avatar-items green" data-valor="29"><img width="30px" height="30px" src='../avatar?getthumb=29.png'</li>
                          <li class="avatar-items green" data-valor="30"><img width="30px" height="30px" src='../avatar?getthumb=30.png'</li>
                          <li class="avatar-items green" data-valor="31"><img width="30px" height="30px" src='../avatar?getthumb=31.png'</li>
                          <li class="avatar-items green" data-valor="32"><img width="30px" height="30px" src='../avatar?getthumb=32.png'</li>
                          <li class="avatar-items green" data-valor="33"><img width="30px" height="30px" src='../avatar?getthumb=33.png'</li>
                          <li class="avatar-items green" data-valor="34"><img width="30px" height="30px" src='../avatar?getthumb=34.png'</li>
                          <li class="avatar-items green" data-valor="35"><img width="30px" height="30px" src='../avatar?getthumb=35.png'</li>
                          <li class="avatar-items green" data-valor="36"><img width="30px" height="30px" src='../avatar?getthumb=36.png'</li>
                          <li class="avatar-items green" data-valor="37"><img width="30px" height="30px" src='../avatar?getthumb=37.png'</li>
                          <li class="avatar-items green" data-valor="38"><img width="30px" height="30px" src='../avatar?getthumb=38.png'</li>
                          <li class="avatar-items green" data-valor="39"><img width="30px" height="30px" src='../avatar?getthumb=39.png'</li>
                          <li class="avatar-items green" data-valor="40"><img width="30px" height="30px" src='../avatar?getthumb=40.png'</li>
                          <li class="avatar-items green" data-valor="41"><img width="30px" height="30px" src='../avatar?getthumb=41.png'</li>
                          <li class="avatar-items green" data-valor="42"><img width="30px" height="30px" src='../avatar?getthumb=42.png'</li>
                          <li class="avatar-items green" data-valor="43"><img width="30px" height="30px" src='../avatar?getthumb=43.png'</li>
                          <li class="avatar-items green" data-valor="44"><img width="30px" height="30px" src='../avatar?getthumb=44.png'</li>
                          <li class="avatar-items green" data-valor="45"><img width="30px" height="30px" src='../avatar?getthumb=45.png'</li>
                          <li class="avatar-items green" data-valor="46"><img width="30px" height="30px" src='../avatar?getthumb=46.png'</li>
                          <li class="avatar-items green" data-valor="47"><img width="30px" height="30px" src='../avatar?getthumb=47.png'</li>
                          <li class="avatar-items green" data-valor="48"><img width="30px" height="30px" src='../avatar?getthumb=48.png'</li>
                          <li class="avatar-items green" data-valor="49"><img width="30px" height="30px" src='../avatar?getthumb=49.png'</li>
                          <li class="avatar-items green" data-valor="50"><img width="30px" height="30px" src='../avatar?getthumb=50.png'</li>
                        
                      </li>
                    </ul>
                  </ul>
                </div>
                </div> 
                    
                  <button type="button" id="btn_cambia_avatar" class="btn btn-indigo">Cambiar avatar</button>
                </div>
                
                <p></p>
                
                <label class="tx-10 tx-uppercase tx-mont tx-medium tx-spacing-1 mg-b-2">Dirección de correo electrónico</label>
                <p id="labelemail" class="tx-inverse mg-b-25">katherine.pechon@themepixels.me</p>
                
                <div class="form-layout form-layout-4">             
                            <form id="formaemail">
                                
                                <label class="col-sm-4 form-control-label">Cambio de cuenta de correo</label>
                                <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label">email<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='email1' type="text" name="email1" placeholder="email">
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                </div>
                                <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label">Reingrese email<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='email2' type="text" name="email2" placeholder="email">
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                </div>
                                <br>
                                <div class="row no-gutters">
                                    <button type="button" id="btn_cambia_email" class="btn btn-info">Modificar email</button>
                                </div>
                            </form>
                        </div>
                
                <p></p>
                
                <div class="form-layout form-layout-4">             
                            <form id="formpassc">
                                
                                <label class="col-sm-4 form-control-label">Cambio de contraseña</label>
                                <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label">Password<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='pass1' type="text" name="pass1" placeholder="Password">
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                </div>
                                <div class="row mg-t-20">
                                <label class="col-sm-4 form-control-label">Reingrese Password<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='pass2' type="text" name="pass2" placeholder="Password">
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                </div>
                                <br>
                                <div class="row no-gutters">
                                    <button type="button" id="btn_cambia_pass" class="btn btn-purple">Modificar contraseña</button>
                                </div>
                            </form>
                        </div>
                
              </div><!-- card -->

            </div>