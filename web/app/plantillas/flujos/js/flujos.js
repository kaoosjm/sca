var fileaudit = function () {  
//   ------------------------------------------------------------------
    let tabla = function () {
        let tbl = "tblimgs"; 
        let  oTable = $('#'+tbl).DataTable({
            responsive: true,
            "pagingType": "full_numbers",
            "destroy": true,
            "processing": true,
            "paging": true,
            "searching": true,
            "serverSide": true,
            language: {
                searchPlaceholder: 'Buscar...',
                sSearch: '',
                lengthMenu: '_MENU_ items/pag',
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros"
            },
            "ajax": {
                "url": "datatables/dt2.jsp",
                "data": {
                    "columnas": "id_cat_credenciales_visitante,fecha_ingreso,fecha_salida,nombre_completo,institucion,edificio,puerta,justificacion",
                    "queryid": "301"
                }
            },
            "columns": [
                {   
                    "data": "id_cat_credenciales_visitante",
                    "className": "dt-center",
                    "visible": true                    
                },
                {"data": "fecha_ingreso", 
                    "visible": true
                },
                {"data": "fecha_salida", 
                    "visible": true
                },
                {"data": "nombre_completo", 
                    "visible": true
                },
                {"data": "institucion", 
                    "visible": true
                },
                {"data": "edificio", 
                    "visible": true
                },
                {"data": "puerta", 
                    "visible": true
                },
                {"data": "justificacion", 
                    "visible": true
                }
                ,
                {   
                    "data": null,
                    "visible": false,
                    "className": "dt-center",
                    "orderable": false,
                    "defaultContent": 
                    "<button tipo='1' type='button' class='btn btn-danger m-1' data-toggle='tooltip-info' data-placement='top' title='Eliminar'><i class='fa fa-eraser'></i></button>" +
                    "<button tipo='2' type='button' class='btn btn-primary m-1' data-toggle='tooltip-info' data-placement='top' title='Eliminar'><i class='fa fa-home'></i></button>"
                }
            ],
            "drawCallback": function (settings) {

                $('#'+tbl+'_filter input').off();

                $('#'+tbl+'_filter input').on('keyup', function (e) {
                    if (e.keyCode === 13) {
                        oTable.search(this.value).draw();
                    }
                });
                
                $('#'+tbl+' tbody tr').css('color','black');
                $('#'+tbl+' tbody tr').css('font-size','14px');              
                
                $('#'+tbl+' tbody').off('click').on('click', 'tr button', function () {

                    var data = oTable.row($(this).parents('tr')).data();
                    var ttt = JSON.stringify($(this).attr("tipo"));
                    
                    if (ttt.indexOf("1")>0){
                        iniciaublock();
                        getPrev(data.nombre_sistema,function(datar){
                                let callcad =  "http://localhost:8080/appDGTI/imagetemporal?getfile="+ datar.trim();
                                //"../imagetemporal?getfile="+ datar.trim();
                                console.log(callcad);
                                finublock();
                                $("#container-pdf").flipBook({
                                    pdfUrl:callcad,
                                    btnPrint:false,
                                    btnShare:false,
                                    btnDownloadPages:false,
                                    btnSelect:false,
                                    btnBookmark:false,
                                    btnToc:false,
                                    btnDownloadPdf:false,
                                    btnSound:false,
                                    btnAutoplay:false,
                                    sound:false,
                                    skin: "dark"
                                });
                                $("#namefile").html(data.nombre_sistema);
                                $("#pnamefile").html(data.nombre_sistema);
                                $("#pnamefile2").html(data.nombre_sistema);
//                                $("#filetemp").attr("data", "https://depts.washington.edu/owrc/Handouts/Hacker-Sample%20MLA%20Formatted%20Paper.pdf" );
                                $("#modaldemo4").modal("show");
                                return this; 
                         });
                         
                    } else if (ttt.indexOf("2")>0){                         
                        alert("dos");
                        $("#modaldemo4").modal("show");
                    } else if (ttt.indexOf("3")>0){                       
                        alert("tres");                        
                    } else if (ttt.indexOf("3")>0){                        
                        alert("cuatro");                        
                    }                    
                });
            }
        });
    };

    let inicializa = function() {
        let bol=1;
        let boly=1;
        $("#btncollapse").click(function(){
            let x = document.getElementById("btncollapse");    
            if (bol===1){
                x.style.transform = "rotate(180deg)";
                $("#crdfiltro").hide(500);
                bol=0;
            } else {
                x.style.transform = "rotate(0deg)";
                $("#crdfiltro").show(500);
                bol=1;
            }
            console.log(x.style.transform," "+bol);           
        });
        
        let y = document.getElementById("btnexpand");    
        y.style.transform = "rotate(90deg)";
        
        $("#btnexpand").click(function(){
            if (boly===1){
                y.style.transform = "rotate(270deg)";
                $("#dvcolfil").hide();
                $("#dvcoltbl").addClass("col-lg-12");
                boly=0;
            } else {
                y.style.transform = "rotate(90deg)";
                $("#dvcolfil").show();                
                $("#dvcolfil").addClass("col-lg-3");                
                $("#dvcoltbl").removeClass('col-lg-12').addClass('col-lg-9');                
                boly=1;
            }            
        });
//        console.clear();
    };
    
    let iniciaublock = function(){
        $.blockUI({
            target: '#ftview',
            boxed: true,
            message: 'Generando Vista...'
        });
    };

    let finublock = function(){
        window.setTimeout(function() {
            $.unblockUI('#ftview');
        }, 200);
    };

   
    return {
        init: function () {
            inicializa();
            tabla();
//            obtieneDatos();
        }
    };
}();

jQuery(document).ready(function () {
    fileaudit.init();
});