var formulariot1 = function () {

    let archivos=[];   
    
    let makeRowFile = function(size, name){
        let html = `
            <div class="list-group-item pd-y-15 pd-x-20 d-xs-flex align-items-center justify-content-start">
            <img width="30px" src="../img/tasks/files.svg"  alt="">
            <div class="mg-xs-l-15 mg-t-10 mg-xs-t-0 mg-r-auto">
                <p class="mg-b-0 tx-inverse tx-medium">`+name+`</p>
                <span class="d-block tx-13">`+size+`</span>
            </div>
            <div class="d-flex align-items-center mg-t-10 mg-xs-t-0">
                <a href="#" class="btn btn-outline-light btn-icon">
                    <div class="tx-20"><i class="icon ion-android-chat"></i></div>
                </a>
                <a href="#" class="btn btn-outline-light btn-icon mg-l-5">
                    <div class="tx-20"><i class="icon ion-android-remove"></i></div>
                </a>
                <a href="#" class="btn btn-outline-light btn-icon mg-l-5">
                    <div class="tx-20"><i class="icon ion-android-more-vertical"></i></div>
                </a>
            </div>
            </div>
        `;
        return html;
    };
    
    
    let adm_bprogreso = function(valor, componente){ 
        let bp = document.getElementById(componente); 
        let pclas = bp.className.split(' '); 
        bp.className = bp.className.replace( pclas[2] , '' );
        bp.innerHTML = valor + '%' ;     
        bp.setAttribute("aria-valuenow", valor)
        bp.className = bp.className + "wd-"+valor+"p";
    }; 
    
    let FileUp = function (tipo_carga, keycode) {
        
        $('#fileupload').fileupload({
            sequentialUploads: true,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png|)$/i,
            dataType: 'json',
            add: function (e, data) {
                data.formData = {tipo_carga: tipo_carga, "idus":"7", keycode: keycode};
                data.submit();
            },
            done: function (e, data) {    
                var fn = data.result[0].name;
                var li = data.result[0].lastid;
                var si = data.result[0].size;
                archivos.push({'file':fn,'ide':li,'filen':data.result[0].nombre_sistema});
                $("#listafload").append(makeRowFile(si,fn));
            },
            progressall: function (e, data) {
                let progress = parseInt(data.loaded / data.total * 100, 10);
                let valr1 = progress % 5;
                if (valr1 === 0) { 
                    adm_bprogreso(progress,'progresobar');
                };
            }
        }).off('fileuploadstart', function (e) {
            
        });
    };
    
    let eventos = function () {
        $(".dropzone").change(function () {
            readFile(this);
        });

        $('.dropzone-wrapper').on('dragover', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).addClass('dragover');
            let key =$("#key").val();
            FileUp(2, key);
        });

        $('.dropzone-wrapper').on('dragleave', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).removeClass('dragover');
        });

        $('.remove-preview').on('click', function () {
            var boxZone = $(this).parents('.preview-zone').find('.box-body');
            var previewZone = $(this).parents('.preview-zone');
            var dropzone = $(this).parents('.form-group').find('.dropzone');
            boxZone.empty();
            previewZone.addClass('hidden');
            reset(dropzone);
        });
    };

    let readFile = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var wrapperZone = $(input).parent();
                var previewZone = $(input).parent().parent().find('.preview-zone');
                var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

                wrapperZone.removeClass('dragover');
                previewZone.removeClass('hidden');
            };

            reader.readAsDataURL(input.files[0]);
        }
    };

    let reset = function (e) {
        e.wrap('<form>').closest('form').get(0).reset();
        e.unwrap();
    };
    
    let inicializa = function(){ 
        
        $('#usuarios').select2();
        $('#modalidad').select2();
        $('#prioridad').select2();
        $('#ocupacion').select2();
        
        $('#fecha_a').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        numberOfMonths: 1,
        dateFormat : 'yy/mm/dd'
        });
        
        $('#fecha_b').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        numberOfMonths: 1,
        dateFormat : 'yy/mm/dd'
        });
        
        $('#fecha_c').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        numberOfMonths: 1,
        dateFormat : 'yy/mm/dd'
        });
        
        let up = document.getElementById("fileupload"); 
        up.addEventListener('click', function(){
          let key =$("#key").val();  
          FileUp(2, key);
        });
    };
    
//    funciones para la carga de datos en los selects
    
    let obtenDatos = function(indice, callback){ 
        $.get("funciones/select2.jsp",{"indice":indice}, function(data){ 
            let d = JSON.parse(data);
            let listad = d.data; 
            let r = [];
            r.push({"id":"0","text":"..."});
            listad.forEach( el => { 
                r.push({"id" : el.id, "text": el.nombre});
            });
            let respuesta = {"results": r, "pagination": {"more": true}}; 
            callback(respuesta);
        });
    };
    
//    fin de las funciones para la carga de datos de selects
    
    let cargaSelectUsuarios = function(){
        
        obtenDatos("200", function(data){
        let items = data.results;
        let itm;
        items.forEach( el  => { 
            itm += "<option value='"+el.id+"'>"+el.text+"</option>";
        });
        document.getElementById("usuarios").innerHTML = itm;
        $('#usuarios').select2();
        });
        
        obtenDatos("201", function(data){
        let items = data.results;
        let itm;
        items.forEach( el  => { 
            itm += "<option value='"+el.id+"'>"+el.text+"</option>";
        });
        document.getElementById("prioridad").innerHTML = itm;
        $('#prioridad').select2();
        });
        
        obtenDatos("202", function(data){
        let items = data.results;
        let itm;
        items.forEach( el  => { 
            itm += "<option value='"+el.id+"'>"+el.text+"</option>";
        });
        document.getElementById("modalidad").innerHTML = itm;
        $('#modalidad').select2();
        });
        
        obtenDatos("203", function(data){
        let items = data.results;
        let itm;
        items.forEach( el  => { 
            itm += "<option value='"+el.id+"'>"+el.text+"</option>";
        });
        document.getElementById("ocupacion").innerHTML = itm;
        $('#ocupacion').select2();
        });
    };
    
    return {
        init: function () {
            eventos();
            inicializa();
            cargaSelectUsuarios();
        }
    };
}();

jQuery(document).ready(function () {
    formulariot1.init();
});


