<%-- 
    Document   : formulariot1
    Created on : 26/04/2021, 11:18:08 AM
    Author     : Daniel Ramos Lim�n
--%>

<%@page import="java.util.Random"%>
<%
  
    int leftLimit = 97; // letter 'a'
    int rightLimit = 122; // letter 'z'
    int targetStringLength = 5;
//    Random random = new Random();

    /*
    String generatedString = random.ints(leftLimit, rightLimit + 1)
      .limit(targetStringLength)
      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
      .toString();
    */

    String generatedString = "jkjidas";
%>    


<style>



    .box {
        position: relative;
        background: #ffffff;
        width: 100%;
    }

    .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative;
        border-bottom: 1px solid #f4f4f4;
        margin-bottom: 10px;
    }

    .box-tools {
        position: absolute;
        right: 10px;
        top: 5px;
    }

    .dropzone-wrapper {
        border: 2px dashed #91b0b3;
        color: #92b0b3;
        position: relative;
        height: 150px;
    }

    .dropzone-desc {
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        text-align: center;
        width: 40%;
        top: 50px;
        font-size: 16px;
    }

    .dropzone,
    .dropzone:focus {
        position: absolute;
        outline: none !important;
        width: 100%;
        height: 150px;
        cursor: pointer;
        opacity: 0;
    }

    .dropzone-wrapper:hover,
    .dropzone-wrapper.dragover {
        background: #ecf0f5;
    }

    .preview-zone {
        text-align: center;
    }

    .preview-zone .box {
        box-shadow: none;
        border-radius: 0;
        margin-bottom: 0;
    }

</style>

<div class="br-section-wrapper">
    <div class="row mg-t-0">
        <div class="col-xl-12 mg-t-20 mg-xl-t-0">
            <div class="form-layout form-layout-5">
                <h6 class="br-section-label">Flujo de Trabajo: <span style="font-weight: bold">Nueva Tarea (Full 3.1)</span></h6>
                <p class="br-section-text">* Campos requeridos</p>
                <div class="row">
                    <label class="col-sm-4 form-control-label"><span class="tx-danger">*&nbsp;</span> Key:</label>
                    <div class="col-sm-6 col-md-3 col-xl-2 mg-t-10 mg-sm-t-0">
                        <input id="key" disabled="" readonly="" type="text" class="form-control" placeholder="Enter firstname" value="<%=generatedString%>">
                    </div>
                </div><!-- row -->
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"><span class="tx-danger">*&nbsp;</span> N�mero de Folio:</label>
                    <div class="col-sm-8 col-md-6 col-xl-4 mg-t-10 mg-sm-t-0">
                        <input type="number" maxlength="13" minlength="13" class="form-control" placeholder="Ingrese N�mero de Folio">
                    </div>
                </div>                
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"> Texto de Solicitud:</label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <textarea rows="2" class="form-control" placeholder="Ingrese Texto de la solicitud"></textarea>
                    </div>
                </div><!-- row -->
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"> Fecha No. Existencia:</label>
                    <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 mg-t-10 mg-sm-t-0">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                              </div>
                            </div>
                            <input type="text" class="form-control fc-datepicker" placeholder="AAAA/MM/DD" id="fecha_a">
                        </div>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"> Fecha Clasificaci�n:</label>
                    <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 mg-t-10 mg-sm-t-0">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                              </div>
                            </div>
                            <input type="text" class="form-control fc-datepicker" placeholder="AAAA/MM/DD" id="fecha_b">
                        </div>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"> Fecha Entrega Informaci�n:</label>
                    <div class="col-sm-6 col-md-4 col-lg-4 col-xl-3 mg-t-10 mg-sm-t-0">
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                              </div>
                            </div>
                            <input type="text" class="form-control fc-datepicker" placeholder="AAAA/MM/DD" id="fecha_c">
                        </div>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"> Prioridad:</label>
                    <div class="col-sm-5 col-md-3 col-lg-3 col-xl-2 mg-t-10 mg-sm-t-0">
                        <select style="width: 100%" class="select2" id="prioridad">
                            <option value="0">...</option>
                        </select>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"><span class="tx-danger">*&nbsp;</span> Usuarios a asignar la tarea:</label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <select style="width: 100%" class="select2" multiple="" id="usuarios">
                            <option value="0">...</option>
                        </select>
                    </div>
                </div>                
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"> Elementos:</label>
                    <div class="col-sm-12 col-md-10 col-lg-10 col-xl-8 mg-t-10 mg-sm-t-0">
                        <form action="" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <!--<label class="control-label">Agregar archivo</label>-->
                                        <div class="dropzone-wrapper">
                                            <div class="dropzone-desc">
                                                <i class="glyphicon glyphicon-download-alt"></i>
                                                <img src="../img/tasks/upload.svg" width="60px">
                                                <p>Elija un archivo o arrastrelo aqui</p>
                                            </div>                                            
                                            <input type="file" name="files[]" id="fileupload" class="dropzone" data-multiple-caption="{count} files selected" data-url="../upload">
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="progress mg-b-20">
                        <div class="progress-bar progress-bar-striped wd-0p" role="progressbar" id="progresobar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>

                    <div class="col-12">
                        <div id="listafload" class="list-group">                            
                        </div><!-- list-group -->
                    </div>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"> Modalidad:</label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <select style="width: 100%" class="select2" id="modalidad">
                            <option value="0">...</option>
                        </select>
                    </div>
                </div>
                <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label"> Ocupaci�n:</label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                        <select style="width: 100%" class="select2" id="ocupacion">
                            <option value="0">...</option>
                        </select>
                    </div>
                </div>
                <div class="row mg-t-30">
                    <div class="col-sm-8 mg-l-auto">
                        <div class="form-layout-footer">
                            <button class="btn btn-primary">Guardar</button>
                            <button class="btn btn-secondary">Cancelar</button>
                        </div><!-- form-layout-footer -->
                    </div><!-- col-8 -->
                </div>
            </div><!-- form-layout -->
        </div><!-- col-6 -->
    </div>
</div>