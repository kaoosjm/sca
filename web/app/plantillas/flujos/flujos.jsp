<%-- 
    Document   : flujos
    Created on : 6/05/2021, 05:45:39 PM
    Author     : Jonathan Lopez Colin
--%>
<%-- 
    Document   : fileaudit
    Created on : 27/04/2021, 05:29:56 PM
    Author     : Daniel Ramos Limón
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script type="text/javascript">
    $(document).ready(function () {
        

    });
</script>
<style>
/*
Full screen Modal 
*/
.fullscreen-modal .modal-dialog {
  margin: 0;
  margin-right: auto;
  margin-left: auto;
  width: 100%;
}
@media (min-width: 768px) {
  .fullscreen-modal .modal-dialog {
    width: 750px;
  }
}
@media (min-width: 992px) {
  .fullscreen-modal .modal-dialog {
    width: 970px;
  }
}
@media (min-width: 1200px) {
  .fullscreen-modal .modal-dialog {
     width: 1340px;
  }
}

.modal-dialog{
    max-width: 100%
}

#container-pdf{
    height: 850px;
}
</style>
<div class="row" id="ftview">
    <div class="col-lg-3" id="dvcolfil">
        <div class="card">
            <h5 class="card-header">Filtros
            <div class="pull-right">
                <button class="btn btn-sm" id="btncollapse"><i class="fa fa-angle-down"></i></button>                   
            </div>
            </h5>            
            <div class="card-body" id="crdfiltro">
                <form>
                    <fieldset class="form-group">
                        <div class="row mg-t-5">
                            <legend class="col-form-label col-sm-12 pt-0"><b>Flujos de Trabajo:</b></legend>
                            <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radft" id="radft1" value="option1" checked>
                                    <label for="radft1">
                                      Activo
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radft" id="radft2" value="option2">
                                    <label for="radft2">
                                      Completados
                                    </label>
                                </div>                                
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                        <div class="row mg-t-5">
                            <legend class="col-form-label col-sm-12 pt-0"><b>Vencimiento:</b></legend>
                            <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radvenc" id="radvenc1" value="option1" checked>
                                    <label for="radvenc1">
                                      Hoy
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radvenc" id="radvenc2" value="option2">
                                    <label for="radvenc2">
                                      Mañana
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radvenc" id="radvenc3" value="option3">
                                    <label or="radvenc3">
                                      Pr&oacute;ximos 7 d&iacute;as
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radvenc" id="radvenc4" value="option4">
                                    <label for="radvenc4">
                                      Con retraso
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radvenc" id="radvenc5" value="option5">
                                    <label for="radvenc5">
                                      Sin Fecha
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                        <div class="row mg-t-5">
                            <legend class="col-form-label col-sm-12 pt-0"><b>Iniciado:</b></legend>
                            <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radini" id="radini1" value="option1" checked>
                                    <label for="radini1">
                                        Los &uacute;ltimos 7 d&iacute;as
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radini" id="radini2" value="option2">
                                    <label for="radini2">
                                      Los &uacute;ltimos 14 d&iacute;as
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radini" id="radini3" value="option3">
                                    <label for="radini3">
                                      Los &uacute;ltimos 28 d&iacute;as
                                    </label>
                                </div>                                
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                        <div class="row mg-t-5">
                            <legend class="col-form-label col-sm-12 pt-0"><b>Prioridad:</b></legend>
                            <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radpri" id="radpri1" value="option1" checked>
                                    <label for="radpri1">
                                      Alta
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radpri" id="radpri2" value="option2">
                                    <label for="radpri2">
                                      Media
                                    </label>
                                </div>
                                <div class="col-lg-12 mg-t-20 mg-lg-t-0">
                                    <input type="radio" name="radpri3" id="radpri3" value="option3">
                                    <label for="radpri3">
                                      Baja
                                    </label>
                                </div>                                
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
          </div>
    </div>
    <div class="col-lg-9" id="dvcoltbl">
        <div class="card">
            <h5 class="card-header">Accesos            
                <div class="pull-right">
                    <button class="btn btn-sm" id="btnexpand"><i class="fa fa-angle-down"></i></button>                   
                </div>
            </h5>
            <div class="card-body" id="crdtable">
                <div class="table-wrapper" >                                  
                    <table id="tblimgs" class="table display responsive nowrap" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Fecha Ingreso</th>
                                <th>Fecha Salida</th>
                                <th>Visitante</th>
                                <th>Empresa</th>
                                <th>Edificio</th>
                                <th>Puerta de Acceso</th>
                                <th>Justificaci&oacute;n</th>                               
                                <th style="width: 100px">...</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Fecha Ingreso</th>
                                <th>Fecha Salida</th>
                                <th>Visitante</th>
                                <th>Empresa</th>
                                <th>Edificio</th>
                                <th>Puerta de Acceso</th>
                                <th>Justificaci&oacute;n</th>
                                <th style="width: 100px" >...</th>
                            </tr>
                        </tfoot>
                    </table>                    
                </div>
            </div>
          </div>
    </div>
</div>
<script src="http://localhost:8080/appDGTI/assets/flipbook/js/flipbook.js"></script>