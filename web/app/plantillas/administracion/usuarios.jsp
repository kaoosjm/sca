<%-- 
    Document   : municipios
    Created on : 26 sep 2019, 9:10:09
    Author     : dramos
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="row">

    <div class="col-lg-12">
        <div class="widget-2">
            <div class="card shadow-base overflow-hidden">
                <div class="card-header">
                    <h6 class="card-title">Lista de usuarios</h6>
                    <a href="" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-toggle="modal" data-target="#modaldemo3">Agregar usuario</a>
                </div><!-- card-header -->
                <div class="card-body pd-0 bd-color-gray-lighter">

                    <div style="margin: 10px; overflow: auto">

                        
                        <table id="vusers" class="display dataTable">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>avatar</th>
                                    <th>nombre</th>
                                    <th>apellidos</th>
                                    <th>email</th>
                                    <th>perfil</th>
                                    <th style="width: 100px" >...</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>avatar</th>
                                    <th>nombre</th>
                                    <th>apellidos</th>
                                    <th>email</th>
                                    <th>perfil</th>
                                    <th style="width: 100px" >...</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- widget-2 -->
    </div><!-- col-6 -->
</div>

<!--Modal-->
<div id="modaldemo3" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Ingresar nuevo usuario</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">



                <form id="nuevousr">

                    <div class="row" style="display: none">
                        <label class="col-sm-6 form-control-label">ide: <span class="tx-danger">*</span></label>
                        <input class="form-control" id='ide' type="text" name="ide" placeholder="IDE">
                        <ul class="hide-error parsley-errors-list filled">
                            <li class="parsley-required">Este valor es requerido</li>
                        </ul>
                    </div>

                    <div class="row">

                        <div class="col-12" style="margin-bottom: 10px">
                            <label class="col-sm-4 form-control-label text-dark">Nombre<span class="tx-danger">*</span></label>
                            <input class="form-control" id='nombre' type="text" name="nombre" placeholder="">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>

                        <div class="col-12" style="margin-bottom: 10px">
                            <label class="col-sm-4 form-control-label text-dark">Apellidos<span class="tx-danger">*</span></label>
                            <input class="form-control" id='apellidos' type="text" name="apellidos" placeholder="">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>
                        
                        <div class="col-6 mg-t-10">
                            <label class="col-sm-4 form-control-label text-dark">email<span class="tx-danger">*</span></label>
                            <input class="form-control" id='email' type="text" name="email" placeholder="">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>
                        
                        <div class="col-6 mg-t-10">
                            <label class="col-sm-4 form-control-label text-dark">valida email<span class="tx-danger">*</span></label>
                            <input class="form-control" id='email' type="text" name="email" placeholder="">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>
                    
                    
                    </div>    

                    <div class="row">
                        <div class="col-6 mg-t-10">
                            <label class="col-sm-4 form-control-label">Perfil<span class="tx-danger">*</span></label>
                            <select id="perfilsel" name="perfilsel" class="form-control select2" data-placeholder="Elegir categoria">
                            </select>  
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                            <ul>
                                <li style="color: #ffffff; margin-top: -10px" class="parsley-required">Este valor es requerido Este valor es requeridoEste valor es requerido</li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-sm-4 form-control-label">&nbsp;<span class="tx-danger">&nbsp;</span></label>
                            <label class="ckbox">
                            <input name="mailact" id="mailact" type="checkbox">
                            <span>Enviar correo de activacion</span>
                            </label>  
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                            <ul>
                                <li style="color: #ffffff; margin-top: -10px" class="parsley-required">Este valor es requerido Este valor es requeridoEste valor es requerido</li>
                            </ul>
                        </div>    
                    </div>    



                    <!--<div class="row no-gutters" style="border-top: 1px solid #ced4da;"> </div>-->

                    <br>
                    <div class="row no-gutters">
                        <button type="button" style="margin-right: 5px" id="btn_guardar_usr" class="btn btn-info tx-size-xs">Guardar</button>
                        <!--<button type="button" id="btn_cancel_cap" style="margin-left: 5px" class="btn btn-secondary">Cancelar</button>-->
                        <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">Cancelar</button>
                    </div>

                </form>


            </div><!-- modal-body -->
            <!--            <div class="modal-footer">
                            <button type="button" class="btn btn-primary tx-size-xs">Save changes</button>
                            <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">Close</button>
                        </div>-->
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
<!--formulario de edicion-->
<div id="modaldemo4" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Modificar registro</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">


                <div class="modal-body pd-20">

                    <form id="editar">

                        <div class="row" style="display: none">
                            <label class="col-sm-4 form-control-label">ide: <span class="tx-danger">*</span></label>
                            <input class="form-control" id='ide' type="text" name="ide" placeholder="IDE">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Nombre<span class="tx-danger">*</span></label>
                            <input class="form-control" id='nombre' type="text" name="nombre" placeholder="Descripción">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Apellidos<span class="tx-danger">*</span></label>
                            <input class="form-control" id='apellidos' type="text" name="apellidos" placeholder="Descripción">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">email<span class="tx-danger">*</span></label>
                            <input class="form-control" id='email' type="text" name="email" placeholder="email">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">valida email<span class="tx-danger">*</span></label>
                            <input class="form-control" id='re_email' type="text" name="re_email" placeholder="email">
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                        </div>

                        <div class="row mg-t-20">
                            <label class="col-sm-4 form-control-label">Perfil de usuario<span class="tx-danger">*</span></label>
                            <select id="perfilsel" class="form-control select2" data-placeholder="Elegir categoria">
                            </select>  
                            <ul class="hide-error parsley-errors-list filled">
                                <li class="parsley-required">Este valor es requerido</li>
                            </ul>
                            <ul>
                                <li style="color: #ffffff; margin-top: -10px" class="parsley-required">Este valor es requerido Este valor es requeridoEste valor es requerido</li>
                            </ul>
                        </div>



                        <!--<div class="row no-gutters" style="border-top: 1px solid #ced4da;"> </div>-->

                        <br>
                        <div class="row no-gutters">
                            <button type="button" style="margin-right: 5px" id="btn_guardar" class="btn btn-info tx-size-xs">Guardar</button>
                            <!--<button type="button" id="btn_cancel_cap" style="margin-left: 5px" class="btn btn-secondary">Cancelar</button>-->
                            <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>

                </div>

            </div><!-- modal-body -->
            <!--            <div class="modal-footer">
                            <button type="button" class="btn btn-primary tx-size-xs">Save changes</button>
                            <button type="button" class="btn btn-secondary tx-size-xs" data-dismiss="modal">Close</button>
                        </div>-->
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->

