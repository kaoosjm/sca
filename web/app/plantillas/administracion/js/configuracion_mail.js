try { 
var cmail = function(){
    var idus = "1"; 
    var form = "formmail"; 
    
   
    
    var modifica_reg = function(){
        var id = document.getElementById("ide").value; 
        var codemsg = $('#summernote1').summernote('code');
        
        var param = { "1":codemsg,"2":id};
            $.post("funciones/funciones_esp.jsp", {'i':92, 't':9,'p': JSON.stringify(param) }, function(data){
            if (data.error_msg.length<1){ 
                Swal.fire({
                type: 'success',
                text: 'Registro modificado'
                });
                tabla();
            } else { 
                Swal.fire({
                type: 'error',
                text: data.error_msg
                }); 
            
            }
            });
    }; 
    
    var cambia_etiquetas = function(data){

    var frm = document.getElementById("formmail");
        frm.dataset.ide = data.id; 
        document.getElementById("ide").value = data.id;
        document.getElementById("descripcion").value = data.descripcion;
        $('#summernote1').summernote('code', data.mensaje);
        deshabilita_form(false);
    };
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    var inicia_estado = function(){ 
       estado = 0;  
    }; 
    
    var deshabilita_form = function(valor) { 
        var forma = document.getElementById(form);
        var elements = forma.elements;
        
        if (valor===true){ 
            for (var i = 0, len = elements.length; i < len; ++i) {
                elements[i].readOnly = true;
            }
            $('#summernote1').summernote('disable');
        } else if (valor===false){ 
            $('#summernote1').summernote('enable');
        }
    };
    
    var tabla = function () {
        var tbl = "cmail"; 
        var oTable = $('#'+tbl).DataTable({
            "destroy": true,
            "processing": true,
            "paging": true,
            "searching": true,
            "serverSide": true,
            "language": datatables(),
            "ajax": {
                "url": "datatables/dt.jsp",
                "data": {
                    "columnas": "id,descripcion,mensaje",
                    "queryid": "91"
                }
            },
            "columns": [
                {"data": "id",
                    "visible": false
                },
                {"data": "descripcion", 
                    "visible": true
                },
                {"data": "mensaje", 
                    "visible": false
                },
                {"data": null,
                    "defaultContent": 
                            
                "<a tipo=1 style='margin:1px' href='javascript:void(0)' class='btn btn-teal btn-icon'><div><i class='fa fa-pencil'></i></div></a>" 
                
                }
            ],
            "drawCallback": function (settings) {

                $('#'+tbl+'_filter input').off();

                $('#'+tbl+'_filter input').on('keyup', function (e) {
                    if (e.keyCode === 13) {
                        oTable.search(this.value).draw();
                    }
                });
                
                $('#'+tbl+' tbody tr').css('color','black');
                $('#'+tbl+' tbody tr').css('font-size','16px');

                $('#'+tbl+' tbody').off('click').on('click', 'tr a', function () {

                    var data = oTable.row($(this).parents('tr')).data();
                    var ttt = JSON.stringify($(this).attr("tipo"));
                    
                    if (ttt.indexOf("1")>0){
                        var desc = data.descripcion;
                        Swal.fire({
                            title: '¿desea modificar el registro: '+desc+'?',
                            text: "Esta acción no podra ser revertida",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, modificar'
                          }).then((result) => {
                            if (result.value) {
//                            -------------------------------
                            
                            let frm = document.getElementById("formmail");
                            frm.dataset.ide = data.id; 
                            document.getElementById("ide").value = data.id;
                            document.getElementById("descripcion").value = data.descripcion;
                            $('#summernote1').summernote('code', data.mensaje);
                            deshabilita_form(false);
                            document.getElementById("maileditor").style.display = "block";
                            document.getElementById("listamails").style.display = "none";
//                            -------------------------------
                            }
                          })
                        
                        
                    } else if (ttt.indexOf("2")>0){ 
                        
                        var iden = data.id;
                        var desc = data.descripcion;
                        
                        limpia_form_cat();
                        cambia_etiquetas(3,"",null);
                        deshabilita_form(true);
                            
                        Swal.fire({
                            title: '¿desea eliminar el registro: '+desc+'?',
                            text: "Esta acción no podra ser revertida",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, borrar'
                          }).then((result) => {
                            if (result.value) {
                            
                                borra_reg(iden,function(dat){
                                tabla();
                                inicia_estado();
                                Swal.fire({
                                type: 'success',
                                text: 'Registro borrado',
                                })
                            })  
                            
                            }
                          })
                    }
                    
                });

            }
        });
    };
    
    
    var inicializa = function() { 
        var d = document.getElementById("descripcion")
        d.value = null;
        d.readOnly = true;  
        $('#summernote1').summernote('disable');
        
    };
    
    var eventos = function(){
      
        
        
        
        
        $('#summernote1').summernote({
        height: 350,
        lang: "gl-ES"
        });
      
      
      var ds = document.getElementById("btn_guardar"); 
        ds.addEventListener('click', function(event){
            event.preventDefault();
            modifica_reg();
        });
        
      var dx = document.getElementById("btn_cancel_cap"); 
        dx.addEventListener('click', function(event){
            event.preventDefault();
            $('#summernote1').summernote('code','');
            var d = document.getElementById("descripcion")
            d.value = null;
            d.readOnly = true;
            $('#summernote1').summernote('disable');
            document.getElementById("maileditor").style.display = "none";
            document.getElementById("listamails").style.display = "block";
            
        });
    };
    
    return { 
        init: function() { 
        inicializa();
        tabla(); 
        eventos();
        }
    };
}(); 

jQuery(document).ready(function () {
    cmail.init();
});

} catch(error){
    console.log(error.message);
}    

        