
var organigrama = function () {
    
    let datos; 
    let datos_aux; 
    let ndatos; 
    let tree;
    let idSeleccionado;
    let arbol = []; 
    let idx = 0; 
        
        let makeTree = function(nombre){ 
            datos_aux = datos; 
            let nodo = []; 
            let ide = buscaIndice(nombre); 
            let hijos = buscaHijos(nombre); 
            if (hijos.length>0) { 
                
                hijos.forEach( hijo => {
                let nombre_hijo = hijo.nombre; 
                makeTree(nombre_hijo);
                });
                
                arbol.push({name: nombre, type: Tree.FOLDER, children : ""});
            
            } else { 
            arbol.push({name: nombre, type: Tree.FILE});
            }
            
            
        }; 
        
    
    let guardarArea = function(){
        
        let id = document.querySelector("#frmaddarea #ide").value;
        let nm = document.querySelector("#frmaddarea #nombre").value;
        let cd = document.querySelector("#frmaddarea #codigo").value;
        let ds = document.querySelector("#frmaddarea #descripcion").value;
        let idus = "1"; 
        
        let p = {"1":id,"2":nm,"3":cd,"4":ds,"5":idus}; 
        post(4,p,(data)=>{
            alert(JSON.stringify(data));
        });
        
    };
    
    let eventos = function(){ 
        
        document.getElementById("btnaddarea").addEventListener('click', function(){
            guardarArea(); 
        });
        
        document.getElementById("btntester").addEventListener('click', function(){
//            let e = tree.active(); 
//            alert(e.innerHTML); 
            creeaArbol();
        });
        
        
        
        document.getElementById("btnaddar").addEventListener('click', function(){
            
            if (idSeleccionado===0) { 
                
                alert("No ha selecionado área");
                
            } else { 
                
            
            document.querySelector("#frmaddarea #ide").value = idSeleccionado;
            $('#modaladdarea').modal('show');
            }
            
        });
       
    };
    
    
    let inicializa = function () {

    };
    
    let buscaIndice = function(nombre){ 
      let valor; 
        datos.forEach( el => { 
         if ( nombre.indexOf(el.nombre)===0){
             valor=el.id; 
         } 
      });  
      return valor; 
    };
    
//    --------------------------------
        
    
    
    
//    --------------------------------
    let buscaHijos = function(nombre){ 
        datos_aux = datos; 
        let ide = buscaIndice(nombre); 
        let hijos = [];
        let counter=0;
        datos_aux.forEach( ele => { 
            let idPadre = ele.id_padre; 
            if ( ele.id_padre === ide ){ 
                let elemento = {name: ele.nombre, type: Tree.FOLDER}; 
                hijos.push(elemento);
                datos_aux.splice(counter,1);
            }
            
        });
        return hijos;
    };
    
    
    
    let creeaArbol = function(){ 
        
        idSeleccionado =0; 
        
        get("3",null, function(data){
            datos = data.data;
            ndatos = datos.length; 
            
            if (ndatos === 0) { 
            document.getElementById("tree").innerHTML = "<label>No existe una área principal, favor de agregar</label>";
            } else { 
            
//            alert(JSON.stringify(datos));
            
            tree = new Tree(document.getElementById('tree'), {
                navigate: true // allow navigate with ArrowUp and ArrowDown
            });
            
            let struct = [];
            
            datos.forEach( ele => {
                    makeTree(ele.nombre); 
//                    let hijos = buscaHijos(ele.nombre);
////                    alert(JSON.stringify(hijos));
//                    let elemento; 
//                    if (hijos.length>0){ 
//                        elemento = {name: ele.nombre, type: Tree.FOLDER, children: hijos };
//                    } else { 
//                        elemento = {name: ele.nombre, type: Tree.FOLDER};
//                    }
////                    alert(hijos.length);
//                    
//                    struct.push(elemento);
            });
            
            let structure = [{
                name: 'Oficina de la presidencia',
                type: Tree.FOLDER,
                children : [
                    {
                        name : "Dirección general de Organización"
                    }
                ]
                }];
            
//            tree.json(structure);
            tree.json(struct);
            
            tree.on('select', e =>{ 
                let valor = e.innerHTML; 
                let elemento = document.getElementById("areasel");
                elemento.innerHTML = valor; 
                let ids = buscaIndice(valor); 
                elemento.dataset.ide = ids
                idSeleccionado = ids;
                console.log(e.innerHTML);
            });
            
            
            
            
            }
        });
    };


    return {
        init: function () {
            eventos();
            inicializa();
            creeaArbol();
        }
    };
}();

jQuery(document).ready(function () {
    organigrama.init();
});

