var paper;

class organigrama{
  constructor(ancho, alto){
    this.areas = [];
    this.ruta = [];  
    
    this.elementos = []; 
    
    this.anchoPapel=ancho;
    this.altoPapel=alto;
    
    this.areas = [
            {'id':1, 'id_padre':0,'descripcion':'raiz','nivel':1},
            {'id':2, 'id_padre':1,'descripcion':'A','nivel':2},
            {'id':3, 'id_padre':2,'descripcion':'B','nivel':3},
            {'id':4, 'id_padre':2,'descripcion':'C','nivel':3}
        ]; 
  }
  
  anchoNivel(){
  return parseInt(this.altoPapel/this.numeroNiveles(),10)-8;  
  }
  
  muestra(){
    alert(JSON.stringify(this.areas));
  }
  
  hijosArea(id_area){ 
    let areas = [];   
    let c=0;
    this.areas.forEach( (el) => { 
        if (el.id_padre===id_area) { 
        areas.push(el.id);
        }
    });
    return areas;   
  }  
    
  numeroHijosArea(id_area){ 
    let c=0;
    this.areas.forEach( (el) => { 
        if (el.id_padre===id_area) { 
        c++;
        }
    });
    return c;   
  }
    
  numeroAreas(){ 
    //el número de areas total.    
    return this.areas.length;
  }
        
  numeroNiveles(){
    //el numero de niveles.
    let nivel=0;  
    this.areas.forEach( (el) => {
        if (el.nivel>nivel) { 
        nivel = el.nivel; 
        }
    }); 
    return nivel;   
  }
  nivelArea(id_area){
    let na; 
      this.areas.forEach( (el) => {
       if (id_area===el.id){ 
        na = el.nivel;
       } 
    });  
      return na; 
  };
  numeroAreasPorNivel(nivel){
    //el número de areas por nivel 
    let c=0; 
    this.areas.forEach( (el) => { 
        if (nivel===el.nivel) { 
        c++; 
        }
    } );    
    return c;   
  }
  agregaArea(id_area_padre, descripcion){
    //agrega nuevo nivel asociado a una area
    let nivel = nivelArea(id_area_padre); 
    let numeroareas = numeroAreas();
    let respuesta; 
    let mensaje_error = "";   
    if (id_area_padre===0 && numeroareas===0){
      //cuando no se tienen elementos  
      elementos.push({'id': numeroareas+1,'id_padre':id_area_padre ,'descripcion':descripcion,'nivel': nivel}) 
      respuesta  = {'error': '0', 'mensaje':mensaje_error};
    } else if (id_area_padre>0){ 
      //para cualquier elemento que no sea el elemento raiz  
      elementos.push({'id': numeroareas+1,'id_padre':id_area_padre ,'descripcion':descripcion,'nivel': nivel})  
      respuesta  = {'error': '0', 'mensaje':mensaje_error};  
    } else { 
      if (id_area_padre===0 && numeroareas>0) { 
      mensaje_error = "el area raiz ya existe";
      }  
      respuesta  = {'error': '1', 'mensaje': mensaje_error };  
    }
  }
  eliminarArea(id_area){
    let indice=0;
    let respuesta;   
    this.areas.forEach( (el) => { 
        if (el.id===id_area){ 
            if (hijosArea(id_area)===0){ 
                this.areas.slice(indice);     
                respuesta = {'error':0, 'mensaje':'borrado exitosamente'}; 
            } else { 
                respuesta = {'error':1, 'mensaje':'el area ha borrar tiene subareas'}; 
            }
        }
        indice++;
    });     
  return respuesta;   
  }
  obtenerPadre(id_area) { 
    let idpadre; 
    this.areas.forEach( (e) => {
       if (e.id===id_area){
           idpadre = e; 
       } 
    });
    return idpadre;
  }  
  limpiaRuta(){
    this.ruta = [];  
  }
  dameRuta() { 
  return this.ruta;
  }  
  obtenRuta(id_area, callback) { 
      let el = this.obtenerPadre(id_area);
      this.ruta.push(el.id);
      if (el.id_padre !== 0 ) { 
      this.obtenRuta(el.id_padre, callback);
      } else { 
      callback();
      }
  }
  
  muestraElementos(){
      console.log('arbol de componentes:',JSON.stringify(this.elementos));
  }
  
  agregaElemento(elemento){
 
  }
}
    var organ = function () {
        
        let inicializa = function () { 
            
            var aw = $("#paper1").width();
            var ah = screen.height-260;
            
            var paper = Raphael("paper1", aw,ah);
            let o = new organigrama(aw,ah);
            
            
            //el tipo 1 es niveles
            var el = [];
            
            
            
            for (x=0;x<o.numeroNiveles();x++){
                paper.rect(10,20+(o.anchoNivel()*x),aw-20,o.anchoNivel()-2).attr({fill: "orange"});
                el = {'tipo':1,'x':10,'y':20+(o.anchoNivel()*x),'w':aw-20,'h':o.anchoNivel()-2};
                let sd = o.numeroAreasPorNivel(x+1);
//                console.log('numero de nivel:' + (x+1) + ", areas de este nivel:" +sd );
            }
            
//            o.muestraElementos();
            
            
        };
        
        let eventos = function () {

       
        };

        return {
            init: function () {
                
                inicializa();
            }
        };
    }();

    jQuery(document).ready(function () {
        organ.init();
    });

