try { 
let perfiles = function(){
    
    
    var guarda_reg = function(nombre, descripcion){
        let idus = sessionStorage.getItem("idus"); 
        let pr = {1:nombre,2:descripcion,3:idus};
        get("15",pr,function(data){
            Swal.fire({
            type: 'success',
            text: 'Registro agregado',
            }).then( ()=>{
                tabla();
                $("#newprofile").modal("hide"); 
            });
        });
    }; 
    
    var borra_reg = function(id, callback){ 
        let pr = {1:id};
        get("12",pr,function(data){
            callback;
        });
    };
    
    var modifica_reg = function(id,nombre,descripcion){
    let idus = sessionStorage.getItem("idus"); 
        let pr = {1:nombre,2:descripcion,3:idus,4:id};
        get("13",pr,function(data){
            Swal.fire({
            type: 'success',
            text: 'Registro modificado',
            }).then( ()=>{
                tabla();
                $("#edtprofile").modal("hide"); 
            });
        });
    }; 
    
    let valida_form = function(){ 
            let c=0; 
            let e1 = document.getElementById("ide");
            var e2 = document.getElementById("nombreper");
            var e3 = document.getElementById("descperfil");

            // reglas de validacion
            c += valida_req(e2,"Este valor es requerido");
            c += valida_req(e3,"Este valor es requerido");
            
                if (c>0){ 
                    setTimeout(function(){
                        oculta("frmperfil");
                    },800); 
                } else{ 
                    ///////////////////////////////////////////////////////////////////////////////////////////
                    guarda_reg(e2.value,e3.value);
                    ///////////////////////////////////////////////////////////////////////////////////////////
                }
        };
        
    let valida_form_edt = function(){ 
            let c=0; 
            let e1 = document.getElementById("_ide");
            var e2 = document.getElementById("_nombreper");
            var e3 = document.getElementById("_descperfil");

            // reglas de validacion
            c += valida_req(e2,"Este valor es requerido");
            c += valida_req(e3,"Este valor es requerido");
            
                if (c>0){ 
                    setTimeout(function(){
                        oculta("frmedtperfil");
                    },800); 
                } else{ 
                    ///////////////////////////////////////////////////////////////////////////////////////////
                  modifica_reg(e1.value,e2.value,e3.value);  
                    ///////////////////////////////////////////////////////////////////////////////////////////
                }
        };
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    var limpia_form_cat = function(forma){
        var forma = document.getElementById(forma); 
        forma.reset();
    }; 
    
    
    
    var tabla = function () {
        var tbl = "umedida"; 
        var oTable = $('#'+tbl).DataTable({
            "destroy": true,
            "processing": true,
            "paging": true,
            "searching": true,
            "serverSide": true,
            "language": datatables(),
            "ajax": {
                "url": "datatables/dt2.jsp",
                "data": {
                    "columnas": "id,nombre,descripcion",
                    "queryid": "14"
                }
            },
            "columns": [
                {"data": "id",
                    "visible": false
                },
                {"data": "nombre", 
                    "visible": true
                },
                {"data": "descripcion", 
                    "visible": true
                },
                {"data": null,
                    "defaultContent": 
                            
                "<div class='btn-group' style='margin: 10px' role='group' aria-label='Basic example'>" +  
                "<button tipo='1' type='button' class='btn btn-info' data-toggle='tooltip-info' data-placement='top' title='Modificar'><i class='fa fa-edit'></i></button>" + 
                "<button tipo='2' type='button' style='margin-left:5px' class='btn btn-danger data-toggle='tooltip-info' data-placement='top' title='Eliminar'><i class='fa fa-eraser'></i></button>" + 
                "</div>"
                
                }
            ],
            "drawCallback": function (settings) {

                $('#'+tbl+'_filter input').off();

                $('#'+tbl+'_filter input').on('keyup', function (e) {
                    if (e.keyCode === 13) {
                        oTable.search(this.value).draw();
                    }
                });
                
                $('#'+tbl+' tbody tr').css('color','black');
                $('#'+tbl+' tbody tr').css('font-size','16px');
                $('#'+tbl+' tbody').off('click').on('click', 'tr button', function () {

                    var data = oTable.row($(this).parents('tr')).data();
                    var ttt = JSON.stringify($(this).attr("tipo"));
                    
                    if (ttt.indexOf("1")>0){
                        var desc = data.descripcion;
                        Swal.fire({
                            title: '¿desea modificar el registro: '+desc+'?',
                            text: "Esta acción no podra ser revertida",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, modificar'
                          }).then((result) => {
                            if (result.value) {
                                document.getElementById("_ide").value = data.id; 
                                document.getElementById("_nombreper").value = data.nombre; 
                                document.getElementById("_descperfil").value = data.descripcion; 
                                $("#edtprofile").modal("show");
                            }
                          })
                        
                        
                    } else if (ttt.indexOf("2")>0){ 
                        
                        var iden = data.id;
                        var desc = data.descripcion;
                        
                            
                        Swal.fire({
                            title: '¿desea eliminar el registro: '+desc+'?',
                            text: "Esta acción no podra ser revertida",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, borrar'
                          }).then((result) => {
                            if (result.value) {
                            
                                borra_reg(iden,function(){
                                tabla();
                                Swal.fire({
                                type: 'success',
                                text: 'Registro borrado',
                                })
                            })  
                            
                            }
                          })
                    }
                    
                });

            }
        });
    };
    
    
    let eventos = function(){
      
      document.getElementById("btn_edt_prof").addEventListener('click', function(){
                valida_form_edt();
      });
        
      document.getElementById("btn_guardar_prof").addEventListener('click', function(){
                valida_form();
      });
        
      document.getElementById("btn_add_profile").addEventListener('click',function(){
          limpia_form_cat("frmperfil");
          $("#newprofile").modal("show");
      });
        
    };
    
    return { 
        init: function() { 
        tabla(); 
        eventos();
        }
    };
}(); 

jQuery(document).ready(function () {
    perfiles.init();
});

} catch(error){
    console.log(error.message);
}    

        