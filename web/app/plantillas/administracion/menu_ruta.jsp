<%-- 
    Document   : menu-ruta
    Created on : 16 abr 2020, 18:03:50
    Author     : daniel
--%>

<%-- 
    Document   : municipios
    Created on : 26 sep 2019, 9:10:09
    Author     : dramos
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<div class="row">


    <div id="localestrs" class="col-lg-12">
        <div class="widget-2">
            <div class="card shadow-base overflow-hidden">
                <div class="card-header">
                    <h6 class="card-title">Lista de Menu sistema</h6>
                </div><!-- card-header -->
                <div class="btn-group" style="margin: 10px" role="group" aria-label="Basic example">
                    <button id='btn_add_cat' class="btn btn-success">Agregar</button>
                </div>
                <div class="card-body pd-0 bd-color-gray-lighter">

                    <div style="margin: 10px">

                        <div style="margin: 10px; overflow: auto">
                            <table id="localestbl" class="display dataTable">
                                <thead>
                                    <tr>
                                        <!--id,sector,giro,disposicion,establecimiento,area,convenio,ubicacion"-->
                                        <th>id</th>
                                        <th>sector</th>
                                        <th>giro</th>
                                        <th>disposicion</th>
                                        <th>establecimiento</th>
                                        <th>area</th>
                                        <th>convenio</th>
                                        <th>ubicacion</th>
                                        <th style="width: 30px" >...</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>sector</th>
                                        <th>giro</th>
                                        <th>disposicion</th>
                                        <th>establecimiento</th>
                                        <th>area</th>
                                        <th>convenio</th>
                                        <th>ubicacion</th>
                                        <th style="width: 30px" >...</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- widget-2 -->
    </div><!-- col-6 -->

    <div id='frm_nuevo' style="display: none" class="col-lg-12 mg-t-20 mg-lg-t-0">
        <div class="widget-2">
            <div class="card shadow-base overflow-hidden">
                <div class="card-header">
                    <h6 id='statelab' class="card-title">Local :  <strong id="namecolonia" > </strong> </h6>
                </div><!-- card-header -->
                <div class="btn-group" style="margin: 10px" role="group" aria-label="Basic example">
                    <button id='btn_cancel_cap1' class="btn btn-danger">Cancelar</button>
                </div>
                <div class="card-body">
                    <div class="form-layout form-layout-4">             
                            <form id="formcpmunedo">
                                
                                <div class="row mg-b-25">
                                
<!--                                    <div style="padding: 10px; display: none" class="col-lg-12">    
                                    <label class="form-control-label">ide<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='ide' type="text" name="ide" placeholder="IDE">
                                    <ul class="hide-error parsley-errors-list filled">
                                        <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div>  -->
                                    
                                    <div class="col-lg-6">    
                                    <label class="form-control-label">Sector<span class="tx-danger">*</span></label>
                                    <select id="sectorselect" name="sectorselect" class="form-control select2" data-placeholder="Elegir categoria">
                                    </select>  
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div>  
                                    
                                    <div class="col-lg-6">    
                                    <label class="form-control-label">Giro<span class="tx-danger">*</span></label>
                                    <select id="giroselect" name="giroselect" class="form-control select2" data-placeholder="Elegir categoria">
                                    </select>  
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div>  
                                    
                                    <div style="margin-top: 10px" class="col-lg-6">    
                                    <label class="form-control-label">Tipo de establecimiento<span class="tx-danger">*</span></label>
                                    <select id="estabsel" name="estabsel" class="form-control select2" data-placeholder="Elegir categoria">
                                    </select>  
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div>  
                                    
                                    <div style="margin-top: 10px" class="col-lg-6">    
                                    <label class="form-control-label">Disposición local<span class="tx-danger">*</span></label>
                                    <select id="disposelect" name="disposelect" class="form-control select2" data-placeholder="Elegir categoria">
                                    </select>  
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div> 
                                    
                                    <div style="margin-top: 10px" class="col-lg-4">    
                                    <label class="form-control-label">Area<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='area' type="text" name="area" placeholder="Denominación">
                                        <ul class="hide-error parsley-errors-list filled">
                                            <li class="parsley-required">Este valor es requerido</li>
                                        </ul>
                                    </div>  
                                    
                                    <div style="margin-top: 10px" class="col-lg-4">    
                                    <label class="form-control-label">Convenio<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='convenio' type="text" name="convenio" placeholder="Denominación">
                                        <ul class="hide-error parsley-errors-list filled">
                                            <li class="parsley-required">Este valor es requerido</li>
                                        </ul>
                                    </div>  
                                    
                                    <div style="margin-top: 10px" class="col-lg-4">    
                                    <label class="form-control-label">Ubicación<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='ubicacion' type="text" name="ubicacion" placeholder="Denominación">
                                        <ul class="hide-error parsley-errors-list filled">
                                            <li class="parsley-required">Este valor es requerido</li>
                                        </ul>
                                    </div>  
                                    
                                
                                </div>    
                                
                                <div class="row no-gutters">
                                    <button type="button" id="btn_guardar_add" class="btn btn-info">Guardar</button>
                                </div>
                            </form>
                        </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- widget-2 -->
    </div><!-- col-6 -->      
    
    
    
    <div id='frm_editar' style="display: none" class="col-lg-12 mg-t-20 mg-lg-t-0">
        <div class="widget-2">
            <div class="card shadow-base overflow-hidden">
                <div class="card-header">
                    <h6 id='statelab' class="card-title">Local :  <strong id="namecolonia" > </strong> </h6>
                </div><!-- card-header -->
                <div class="btn-group" style="margin: 10px" role="group" aria-label="Basic example">
                    <button id='btn_cancel_cap2' class="btn btn-danger">Cancelar</button>
                </div>
                <div class="card-body">
                    <div class="form-layout form-layout-4">             
                            <form id="formcpmunedo">
                                
                                <div class="row mg-b-25">
                                
                                    <div style="padding: 10px; display: none" class="col-lg-12">    
                                    <label class="form-control-label">ide<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='ide' type="text" name="ide" placeholder="IDE">
                                    <ul class="hide-error parsley-errors-list filled">
                                        <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div>  
                                    
                                    <div class="col-lg-6">    
                                    <label class="form-control-label">Sector::<span class="tx-danger">*</span></label>
                                    <select id="sectors" name="sectors" class="form-control select2" data-placeholder="Elegir categoria">
                                    </select>  
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div>  
                                    
                                    <div class="col-lg-6">    
                                    <label class="form-control-label">Giro<span class="tx-danger">*</span></label>
                                    <select id="giros" name="giros" class="form-control select2" data-placeholder="Elegir categoria">
                                    </select>  
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div>  
                                    
                                    <div style="margin-top: 10px" class="col-lg-6">    
                                    <label class="form-control-label">Tipo de establecimiento<span class="tx-danger">*</span></label>
                                    <select id="estab" name="estab" class="form-control select2" data-placeholder="Elegir categoria">
                                    </select>  
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div>  
                                    
                                    <div style="margin-top: 10px" class="col-lg-6">    
                                    <label class="form-control-label">Disposición local<span class="tx-danger">*</span></label>
                                    <select id="dispo" name="dispo" class="form-control select2" data-placeholder="Elegir categoria">
                                    </select>  
                                    <ul class="hide-error parsley-errors-list filled">
                                    <li class="parsley-required">Este valor es requerido</li>
                                    </ul>
                                    </div> 
                                    
                                    <div style="margin-top: 10px" class="col-lg-4">    
                                    <label class="form-control-label">Area<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='areae' type="text" name="areae" placeholder="Denominación">
                                        <ul class="hide-error parsley-errors-list filled">
                                            <li class="parsley-required">Este valor es requerido</li>
                                        </ul>
                                    </div>  
                                    
                                    <div style="margin-top: 10px" class="col-lg-4">    
                                    <label class="form-control-label">Convenio<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='convenioe' type="text" name="convenioe" placeholder="Denominación">
                                        <ul class="hide-error parsley-errors-list filled">
                                            <li class="parsley-required">Este valor es requerido</li>
                                        </ul>
                                    </div>  
                                    
                                    <div style="margin-top: 10px" class="col-lg-4">    
                                    <label class="form-control-label">Ubicación<span class="tx-danger">*</span></label>
                                    <input class="form-control" id='ubicacione' type="text" name="ubicacione" placeholder="Denominación">
                                        <ul class="hide-error parsley-errors-list filled">
                                            <li class="parsley-required">Este valor es requerido</li>
                                        </ul>
                                    </div>  
                                    
                                
                                </div>    
                                
                                <div class="row no-gutters">
                                    <button type="button" id="btn_guardar_edt" class="btn btn-info">Guardar</button>
                                </div>
                            </form>
                        </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- widget-2 -->
    </div><!-- col-6 -->      
    
    

</div>


 
    