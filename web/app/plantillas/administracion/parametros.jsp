<%-- 
    Document   : aplicaciones
    Created on : 26/01/2021, 09:51:38 AM
    Author     : drlimon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="accordion5" class="accordion accordion-head-colored accordion-danger mg-t-20" role="tablist" aria-multiselectable="true">
            <div class="card">
              <div class="card-header" role="tab" id="headingOne5">
                <h6 class="mg-b-0">
                  <a data-toggle="collapse" data-parent="#accordion5" href="#collapseOne5" aria-expanded="false" aria-controls="collapseOne5" >
                    Aplicación
                  </a>
                </h6>
              </div><!-- card-header -->

              <div id="collapseOne5" class="collapse" role="tabpanel" aria-labelledby="headingOne5" style="">
                <div class="card-block pd-20">
                    <div style="margin-bottom: 10px" class="row">
                        
                        <div class="col-4 text-dark">
                            Titulo pestaña aplicacion
                        </div>
                        <div class="col-7">
                            <input class="form-control" placeholder="Input box" type="text">
                        </div>
                        <div class="col-1">
                            
                            <a href="#" class="btn btn-outline-teal btn-icon mg-r-5"><div><i class="fa fa-save"></i></div>
                            </a>
                        </div>
                    </div>
                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-4 text-dark">
                            Titulo login
                        </div>
                        <div class="col-7">
                            <input class="form-control" placeholder="Input box" type="text">
                        </div>
                        <div class="col-1">
                            
                            <a href="#" class="btn btn-outline-teal btn-icon mg-r-5"><div><i class="fa fa-save"></i></div>
                            </a>
                        </div>
                    </div>
                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-4 text-dark">
                            Subtitulo login
                        </div>
                        <div class="col-7">
                            <input class="form-control" placeholder="Input box" type="text">
                        </div>
                        <div class="col-1">
                            <a href="#" class="btn btn-outline-teal btn-icon mg-r-5"><div><i class="fa fa-save"></i></div>
                            </a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" role="tab" id="headingTwo5">
                <h6 class="mg-b-0">
                  <a class="collapsed transition" data-toggle="collapse" data-parent="#accordion5" href="#collapseTwo5" aria-expanded="false" aria-controls="collapseTwo5">
                    Base de datos
                  </a>
                </h6>
              </div>
              <div id="collapseTwo5" class="collapse" role="tabpanel" aria-labelledby="headingTwo5">
                <div class="card-block pd-20">
                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-4 text-dark">
                            Recurso /jdbc
                        </div>
                        <div class="col-7">
                            <input class="form-control" placeholder="Input box" type="text">
                        </div>
                        <div class="col-1">
                            <a href="#" class="btn btn-outline-teal btn-icon mg-r-5"><div><i class="fa fa-save"></i></div>
                            </a>
                        </div>
                    </div>
                    
                    
                    
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" role="tab" id="headingThree5">
                <h6 class="mg-b-0">
                  <a class="collapsed transition" data-toggle="collapse" data-parent="#accordion5" href="#collapseThree5" aria-expanded="false" aria-controls="collapseThree5">
                    Cuentas de correo
                  </a>
                </h6>
              </div>
              <div id="collapseThree5" class="collapse" role="tabpanel" aria-labelledby="headingThree5">
                <div class="card-block pd-20">

                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-4 text-dark">
                            Host
                        </div>
                        <div class="col-7">
                            <input class="form-control" placeholder="Input box" type="text">
                        </div>
                        <div class="col-1">
                            <a href="#" class="btn btn-outline-teal btn-icon mg-r-5"><div><i class="fa fa-save"></i></div>
                            </a>
                        </div>
                    </div>
                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-4 text-dark">
                            Usuario
                        </div>
                        <div class="col-7">
                            <input class="form-control" placeholder="Input box" type="text">
                        </div>
                        <div class="col-1">
                            <a href="#" class="btn btn-outline-teal btn-icon mg-r-5"><div><i class="fa fa-save"></i></div>
                            </a>
                        </div>
                    </div>
                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-4 text-dark">
                            Password
                        </div>
                        <div class="col-7">
                            <input class="form-control" placeholder="Input box" type="text">
                        </div>
                        <div class="col-1">
                            <a href="#" class="btn btn-outline-teal btn-icon mg-r-5"><div><i class="fa fa-save"></i></div>
                            </a>
                        </div>
                    </div>

                </div>
              </div><!-- collapse -->
            </div><!-- card -->
          </div>
