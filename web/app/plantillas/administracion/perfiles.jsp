<%-- 
    Document   : municipios
    Created on : 26 sep 2019, 9:10:09
    Author     : dramos
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="row">


    <div class="col-lg-12">
        <div class="widget-2">
            <div class="card shadow-base overflow-hidden">
                <div class="card-header">
                    <h6 class="card-title">Perfiles de usuario</h6>
                </div><!-- card-header -->
                <div class="btn-group" style="margin: 10px" role="group" aria-label="Basic example">
                    <button id='btn_add_profile' class="btn btn-success">Agregar nuevo perfil</button>
                </div>
                <div class="card-body pd-0 bd-color-gray-lighter">
                    <div style="margin: 10px; overflow: auto">
                        <table id="umedida" class="display dataTable">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>nombre</th>
                                    <th>descripcion</th>
                                    <th style="width: 100px" >...</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- widget-2 -->
    </div><!-- col-6 -->
</div>


<!------------------------- modal editor ------------------------>
<div id="newprofile" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Agregar perfil de usuario</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <div style="margin:10px">
                <form id="frmperfil">
                    <div class="row" style="display: none">
                        <label class="col-sm-4 form-control-label">ide: <span class="tx-danger">*</span></label>
                        <input class="form-control" id='ide' type="text" name="ide" placeholder="IDE">
                        <ul class="hide-error parsley-errors-list filled">
                            <li class="parsley-required">Este valor es requerido</li>
                        </ul>
                    </div><!-- row -->

                    <div class="row">
                        <label class="col-sm-4 form-control-label">Nombre perfil<span class="tx-danger">*</span></label>
                        <input class="form-control" id='nombreper' type="text" name="nombreper" placeholder="">
                        <ul class="hide-error parsley-errors-list filled">
                            <li class="parsley-required">Este valor es requerido</li>
                        </ul>
                    </div>
                    
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Descripción de perfil<span class="tx-danger">*</span></label>
                        <textarea id="descperfil" name="descperfil" rows="3" class="form-control" placeholder=""></textarea>
                        <ul class="hide-error parsley-errors-list filled">
                            <li class="parsley-required">Este valor es requerido</li>
                        </ul>
                    </div>

                    <br>
                    <div class="row no-gutters">
                        <button type="button" id="btn_guardar_prof" class="btn btn-info">Guardar</button>
                        <!--<button type="button" id="btn_cancel_cap" style="margin-left: 5px" class="btn btn-secondary">Cancelar</button>-->
                        <button type="button" style="margin-left: 2px" class="btn btn-secondary tx-size-xs" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
                    </div>
            </div>
            <!-- modal-body -->
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
<!-- ---------------------------------------------------------- !>
<!------------------------- modal editor ------------------------>
<div id="edtprofile" class="modal fade">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Modificar perfil de usuario</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <div style="margin:10px">
                <form id="frmedtperfil">
                    <div class="row" style="display: none">
                        <label class="col-sm-4 form-control-label">ide: <span class="tx-danger">*</span></label>
                        <input class="form-control" id='_ide' type="text" name="_ide" placeholder="IDE">
                        <ul class="hide-error parsley-errors-list filled">
                            <li class="parsley-required">Este valor es requerido</li>
                        </ul>
                    </div><!-- row -->

                    <div class="row">
                        <label class="col-sm-4 form-control-label">Nombre perfil<span class="tx-danger">*</span></label>
                        <input class="form-control" id='_nombreper' type="text" name="_nombreper" placeholder="">
                        <ul class="hide-error parsley-errors-list filled">
                            <li class="parsley-required">Este valor es requerido</li>
                        </ul>
                    </div>
                    
                    <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Descripción de perfil<span class="tx-danger">*</span></label>
                        <textarea id="_descperfil" name="_descperfil" rows="3" class="form-control" placeholder=""></textarea>
                        <ul class="hide-error parsley-errors-list filled">
                            <li class="parsley-required">Este valor es requerido</li>
                        </ul>
                    </div>

                    <br>
                    <div class="row no-gutters">
                        <button type="button" id="btn_edt_prof" class="btn btn-info">Guardar</button>
                        <!--<button type="button" id="btn_cancel_cap" style="margin-left: 5px" class="btn btn-secondary">Cancelar</button>-->
                        <button type="button" style="margin-left: 2px" class="btn btn-secondary tx-size-xs" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
                    </div>
            </div>
            <!-- modal-body -->
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
<!-- ---------------------------------------------------------- !>