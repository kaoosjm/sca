<%-- 
    Document   : organigrama
    Created on : 21 mar 2020, 10:43:20
    Author     : daniel
--%>

<div class="row">
    <div class="col-6">
        <div class="col-12">
        <label>�rea seleccionada</label>
        </div>
        <div class="col-12">
        <label class="text-dark" data-ide="" id="areasel"></label>
        </div>
    </div>
    <div class="col-6">
        <button id="btnaddar" class="btn btn-primary">Agregar �rea</button>
        <button id="btntester" class="btn btn-primary">Test</button>
    </div>
</div>
<hr>
<div id="tree"></div>

<div id="modaladdarea" class="modal fade">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
          <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25">
              <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Agregar nueva �rea</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body pd-25">
                
                    <div class="row">
                <form id="frmaddarea">
                    
                            
                            <div class="col-12">
                                <label>Identificador</label>
                            </div>    
                            
                            <div class="col-12">
                                <input type="text" id="ide" name="ide" class="form-control wd-250" required>
                            </div>    
                            <div class="col-12">
                                <label>Nombre de �rea</label>
                            </div>    
                            
                            <div class="col-12">
                                <input type="text" id="nombre" name="nombre" class="form-control wd-250" required>
                            </div>    
                            
                            <div class="col-12">
                                <label>C�digo</label>
                            </div>    
                            
                            <div class="col-12">
                                <input type="text" id="codigo" name="codigo" class="form-control wd-250" required>
                            </div>    
                            <div class="col-12">
                                <label>Descripci�n del �rea</label>
                            </div>    
                            
                            <div class="col-12">
                                <textarea id="descripcion" name="descripcion" rows="3" class="form-control" ></textarea>
                            </div>    
                    
                    
                    
                </form>
                    </div>    


            </div>
            <div class="modal-footer">
              <button type="button" id="btnaddarea" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Agregar �rea</button>
              <button type="button" id="btnclosem" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div><!-- modal-dialog -->
      </div><!-- modal -->