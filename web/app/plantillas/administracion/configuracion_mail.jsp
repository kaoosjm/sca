<%-- 
    Document   : municipios
    Created on : 26 sep 2019, 9:10:09
    Author     : dramos
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="row">


    <div id="listamails" class="col-lg-12">
        <div class="widget-2">
            <div class="card shadow-base overflow-hidden">
                <div class="card-header">
                    <h6 class="card-title">Caratúlas mail</h6>
                </div><!-- card-header -->
                <div class="card-body pd-0 bd-color-gray-lighter">
                    <div style="margin: 10px; overflow: auto">
                        <table id="cmail" class="display dataTable">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>descripcion</th>
                                    <th>mensaje</th>
                                    <th style="width: 100px" >...</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>id</th>
                                    <th>descripcion</th>
                                    <th>mensaje</th>
                                    <th style="width: 100px" >...</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- widget-2 -->
    </div><!-- col-6 -->


    <div id="maileditor" class="col-lg-12" style="margin-top: 20px; display: none">
        <div class="widget-2">
            <div class="card shadow-base overflow-hidden">
                <div class="card-header">
                    <h6 class="card-title">Lista envios mail</h6>
                </div><!-- card-header -->
                <div class="card-body pd-0 bd-color-gray-lighter">
                    <div style="margin: 20px; overflow: auto">

                <form id="formmail">
                <div class="row" style="display: none">
                    <label class="col-sm-4 form-control-label">ide: <span class="tx-danger">*</span></label>
                    <input class="form-control" id='ide' type="text" name="ide" placeholder="IDE">
                    <ul class="hide-error parsley-errors-list filled">
                        <li class="parsley-required">Este valor es requerido</li>
                    </ul>
                </div><!-- row -->

                <div class="row">
                    <label class="col-sm-4 form-control-label">Descripcion<span class="tx-danger">*</span></label>
                    <input class="form-control" id='descripcion' type="text" name="descripcion" placeholder="Descripcion">
                    <ul class="hide-error parsley-errors-list filled">
                        <li class="parsley-required">Este valor es requerido</li>
                    </ul>
                </div>

                <div style="margin: 20px" class="row">
                    <!--<label class="col-sm-2 form-control-label">Mensaje<span class="tx-danger">*</span></label>-->
                    <textarea id="summernote1" name="editordata1"></textarea>
                </div>


                <!--<div class="row no-gutters" style="border-top: 1px solid #ced4da;">-->
                <br>
                <div class="row no-gutters">
                    <button type="button" id="btn_guardar" class="btn btn-info">Guardar</button>
                    <button type="button" id="btn_cancel_cap" style="margin-left: 5px" class="btn btn-secondary">Cancelar</button>
                </div>
            </form>

                    </div>

                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- widget-2 -->
    </div><!-- col-6 -->

</div>