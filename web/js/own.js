try { 
let intervalx = 1000; 


//console.log("valor de la variable:",_chkmsg);

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};



//Función para validar una CURP
function curpValida(curp) {
	var re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0\d|1[0-2])(?:[0-2]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
    	validado = curp.match(re);
	
    if (!validado)  //Coincide con el formato general?
    	return false;
    
    //Validar que coincida el dígito verificador
    function digitoVerificador(curp17) {
        //Fuente https://consultas.curp.gob.mx/CurpSP/
        var diccionario  = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ",
            lngSuma      = 0.0,
            lngDigito    = 0.0;
        for(var i=0; i<17; i++)
            lngSuma= lngSuma + diccionario.indexOf(curp17.charAt(i)) * (18 - i);
        lngDigito = 10 - lngSuma % 10;
        if(lngDigito == 10)
            return 0;
        return lngDigito;
    }
    if (validado[2] != digitoVerificador(validado[1])) 
    	return false;
        
	return true; //Validado
}



function manejaError(id_error, text_error){
    document.getElementById(id_error).innerHTML = text_error;
    setTimeout(function(){
    document.getElementById(id_error).innerHTML = "";
    },intervalx);
}

function cargaselect(i,p,select,valor,callback){ 
        
      get(i,p,function(data){
            combobox(data.data,select,true,'...');
            if(valor){
            document.getElementById(select).value = valor;
            }
            callback();
      });  
}

function btntable(tipo){
    let cadena=""; 
    switch(tipo){
        case 1: 
            cadena ="<div class='btn-group' style='margin: 10px' role='group' aria-label='Basic example'><button tipo='"+ tipo +"' type='button' class='btn btn-info' data-toggle='tooltip-info' data-placement='top' title='Eliminar'><i class='fa fa-edit'></i></button></div>";
            break;
        case 2: 
            cadena ="<div class='btn-group' style='margin: 10px' role='group' aria-label='Basic example'><button tipo='"+ tipo + "' type='button' class='btn btn-warning' data-toggle='tooltip-info' data-placement='top' title='Eliminar'><i class='fa fa-edit'></i></button></div>";
            break;
    }
    return cadena; 
}

function setParametros(parametros,callback ){ 
    $.get("funciones/funcionesProps.jsp", {"tipo":"2", "parametros": JSON.stringify(parametros) }, function(data){
        callback(data);
    }); 
}

function getParametros(parametros,callback){ 
    $.get("funciones/funcionesProps.jsp", {"tipo":"1", "parametros": JSON.stringify(parametros) }, function(data){
        callback(data);
    }); 
}


function get(i,p,callback){
    let parametro; 
    if (p){
        parametro = JSON.stringify(p);
    } else { 
        parametro = null;
    }
    $.get("funciones/funciones2.jsp", {"i":i,"p": parametro }, function(data){
        callback(data);
    });  
};

function getPrev(filename,callback){
    $.get("funciones/preview.jsp", {"filename": filename}, function(data){
        callback(data);
    });  
};

function gets(idx,callback){
    
    $.get("funciones/funciones2.jsp", {"i":"1000","c": idx }, function(data){
        callback(data);
    });  
};

function post(i,p,callback){
    $.post("funciones/funciones2.jsp", {"i":i,"p": JSON.stringify(p) }, function(data){
        callback(data);
    });  
};


function generaColumnasDT(colms, colms_vis, botones){ 
    let columns = []; 
    let cols = colms.split(","); 
    let cols_vis = colms_vis.split(",");

    if (cols.length !== cols_vis.length) { 
        alert("Diferencia arrays Tabla"); 
        return; 
    }
    let contador = 0; 
    cols_vis.forEach( (el) => { 
        if ( parseInt(el,10) ===0) { 
            visualizacion = false; 
        } else { 
            visualizacion = true; 
        }
        columns.push( {'data':cols[contador],'visible':visualizacion} );
        contador++; 
    });
    columns.push( {"data": null,"defaultContent":botones});
    return columns; 

}

function tablas(tabla, colms, colms_vis, queryid, botones,callbackdraw){
    let columns = generaColumnasDT(colms, colms_vis, botones); 
    var oTable = $('#'+tabla).DataTable({
            "destroy": true,
            "processing": true,
            "paging": true,
            "searching": true,
            "serverSide": true,
            "language": datatables(),
            "ajax": {
                "url": "datatables/dt2.jsp",
                "data": {
                    "columnas": colms,
                    "queryid": queryid
                }
            },
            "columns": columns,
            "drawCallback": function (settings) {

                $('#'+tabla+'_filter input').off();

                $('#'+tabla+'_filter input').off("keyup").on('keyup', function (e) {
                    if (e.keyCode === 13) {
                        oTable.search(this.value).draw();
                    }
                });
                
                $('#'+tabla+' tbody tr').css('color','black');
                $('#'+tabla+' tbody tr').css('font-size','16px');

                $('#'+tabla+' tbody').off('click').on('click', 'tr button', function () {
                    
                    var data = oTable.row($(this).parents('tr')).data();
                    var tipo = $(this).attr("tipo");
                    
                    callbackdraw(data, parseInt(tipo,10));
                    
                });

            }
        });
}


function alertsone(titulo,subtitulo,pie,tipo,callback){
    let valor_tipo = "success"; 
    switch (tipo) { 
        case 1: 
            valor_tipo = "success";
            break; 
        case 2: 
            valor_tipo = "error";
            break; 
        case 3:
            valor_tipo = "warning";
            break; 
        case 4: 
            valor_tipo = "info";
            break; 
        case 5: 
            valor_tipo = "question";
            break; 
    }
    Swal.fire({
    type: valor_tipo,
    title: titulo,
    text: subtitulo,
    footer: pie
    }).then( () => {
        callback();
    });
    
}


function alerts(titulo,texto,tipo,confirmacion, callback){ 
    let valor_tipo = "success"; 
    switch (tipo) { 
        case 1: 
            valor_tipo = "success";
            break; 
        case 2: 
            valor_tipo = "error";
            break; 
        case 3:
            valor_tipo = "warning";
            break; 
        case 4: 
            valor_tipo = "info";
            break; 
        case 5: 
            valor_tipo = "question";
            break; 
    }
    Swal.fire({
            title: titulo,
            text: texto,
            type: valor_tipo,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: confirmacion
            }).then((result) => {
            if (result.value) {
                callback();
            }
            });  
}


function NumText(string, longitud){//solo letras y numeros
    var out = '';
    for (x=0;x<string.length;x++){
        let filt = [32,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,241,209,46,95,225,233,237,243,250,193,201,205,211,219,44,48,49,50,51,52,53,54,55,56,57];
        let lastString = string.charAt(x);
        let vlstring = lastString.charCodeAt(); 
    
        for (i=0; i<filt.length; i++){ 
            if (vlstring === filt[i] && out.length<=longitud-1){
            out += lastString;  
            }
        }
    }
    return out;
}

function NumNum(string, longitud){//solo letras y numeros
    var out = '';
    for (x=0;x<string.length;x++){
        let filt = [44,46,48,49,50,51,52,53,54,55,56,57];
        let lastString = string.charAt(x);
        let vlstring = lastString.charCodeAt(); 
    
        for (i=0; i<filt.length; i++){ 
            if (vlstring === filt[i] && out.length<=longitud-1){
            out += lastString;  
            }
        }
    }
    return out;
}
function NumTextArroba(string, longitud){//solo letras y numeros
    var out = '';
    for (x=0;x<string.length;x++){
        let filt = [46,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,48,49,50,51,52,53,54,55,56,57];
        let lastString = string.charAt(x);
        let vlstring = lastString.charCodeAt(); 
    
        for (i=0; i<filt.length; i++){ 
            if (vlstring === filt[i] && out.length<=longitud-1){
            out += lastString;  
            }
        }
    }
    return out;
}


// patron del RFC, persona moral
var _rfc_pattern_pm = "^(([A-ZÑ&]{3})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|" +
                  "(([A-ZÑ&]{3})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|" +
                  "(([A-ZÑ&]{3})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|" +
                  "(([A-ZÑ&]{3})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$";
 // patron del RFC, persona fisica
var _rfc_pattern_pf = "^(([A-ZÑ&]{4})([0-9]{2})([0][13578]|[1][02])(([0][1-9]|[12][\\d])|[3][01])([A-Z0-9]{3}))|" +
                       "(([A-ZÑ&]{4})([0-9]{2})([0][13456789]|[1][012])(([0][1-9]|[12][\\d])|[3][0])([A-Z0-9]{3}))|" +
                       "(([A-ZÑ&]{4})([02468][048]|[13579][26])[0][2]([0][1-9]|[12][\\d])([A-Z0-9]{3}))|" +
                       "(([A-ZÑ&]{4})([0-9]{2})[0][2]([0][1-9]|[1][0-9]|[2][0-8])([A-Z0-9]{3}))$";
    

function breadcru(valor1,valor2){ 
    document.querySelector("#breadcru1").innerHTML = valor1; 
    document.querySelector("#breadcru2").innerHTML = valor2; 
};    
    
    
function datatables(){
    var config = {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Sin datos disponibles en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "",
                searchPlaceholder: 'Buscar...',
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            };
    return config; 
}; 

//// funciones de validacion ///////////////////////////////////////////////////////////////////////////

var oculta = function(form) {
    var f = document.getElementById(form); 
    var elems = f.querySelectorAll(".parsley-errors-list");
    elems.forEach(function(xe){
        xe.classList.add("hide-error");
    });
}; 
 

function valida_mail(elemento, msg_error){
    var c=0; 
    if (validamail(elemento.value)===false){ 
        valida_msg(elemento,msg_error);  
        c++;
    }
    return c;    
}

var ocultaele = function(elemento) {
    var f = elemento.parentElement;
    var elems = f.querySelectorAll(".parsley-errors-list");
    elems.forEach(function(xe){
        xe.classList.add("hide-error");
    });
}; 

function valida_msg(elemento,etiqueta){
          var parent = elemento.parentElement;
          var elep = parent.getElementsByTagName("ul")[0];
          elep.getElementsByTagName("li")[0].innerHTML= etiqueta;
          elep.classList.remove('hide-error');
}

function valida_void(elemento, msg_error){
        valida_msg(elemento,msg_error);  
        setTimeout(function(){
            ocultaele(elemento);
        },800); 
}


function valida_captcha(elemento, valorCaptcha, msg_error){
    var c=0; 
    if ( parseInt(valorCaptcha.length,10)===0){ 
        valida_msg(elemento,msg_error);  
        c++;
    }
    return c;    
}

function valida_rfc(elemento, msg_error){
    var c=0;
    var rfc = elemento.value;
    if (rfc.match(_rfc_pattern_pm) || rfc.match(_rfc_pattern_pf)){
    }else {
        valida_msg(elemento,msg_error); 
        c++;
    }
    return c;
}

function valida_cp(elemento, msg_error){
    var c=0;
    var cp = elemento.value;
    var valida_num = !/^([0-9])*$/.test(cp);
    if (cp.length<=5 && valida_num===true){
    } else { 
        c++; 
    }
    return c;
}

  
function valida_num(elemento,valor_ref, msg_error){ 
    var c=0;  
    if (elemento===null){
        c++;
    }
    if (parseInt(elemento.value,10)===parseInt(valor_ref,10)){ 
        valida_msg(elemento,msg_error);
        c++; 
     } 
    return c; 
};



function valida_vacio(elemento,msg_error){ 
    var c=0;  
    if (parseInt(elemento.value.length,10)===0){ 
        valida_msg(elemento,msg_error);
        c++; 
     } 
    return c; 
};

function valida_num_men(elemento,valor_ref, msg_error){ 
    var c=0;  
    if (elemento===null){
        c++;
    }
    if (parseInt(elemento.value,10)<parseInt(valor_ref,10)){ 
        valida_msg(elemento,msg_error);
        c++; 
     } 
    return c; 
};

    function valida_equal(elemento1,elemento2, msg_error){ 
    var c=0; 
    var el1 = elemento1.value; 
    var el2 = elemento2.value; 
    if ( el1.indexOf(el2)<0 ){ 
        valida_msg(elemento1,msg_error);
        c++; 
     } 
    return c; 
};

function valida_req(elemento, msg_error){ 
    var c=0;  
    if (elemento.value.length===0){ 
        valida_msg(elemento,msg_error);    
        c++;
    }
    return c; 
};

function valida_hist(elemento, msg_error){ 
    var c=0;  
    if (String(elemento.value)===String(elemento.dataset.hist)){ 
        valida_msg(elemento,msg_error);    
        c++;
    }
    return c; 
};

function valida_val(val, elemento, msg_error){ 
    var c=0;  
    if (val>0){ 
        valida_msg(elemento,msg_error);    
        c++;
    }
    return c; 
};


function evalua(c, form){
    let respuesta = false; 
    if (c>0){ 
        setTimeout(function(){
            oculta(form);
        },800); 
    } else{ 
    respuesta = true;
    }
    return respuesta;
}

function valida_req_may_lenght(elemento, lenght, msg_error){ 
    var c=0;  
    if (elemento.value.length<lenght){ 
    valida_msg(elemento,msg_error);    
    c++;
    }
    return c; 
};

function validamail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


//// fin de funciones de validacion ////////////////////////////////////////////////////////////////////

function combobox(datos, contenedor, inicio, tag_inicio){
    var contenedor = document.getElementById(contenedor); 
    var cadena=""; 
    
    if (inicio===true){
    cadena += "<option value='0'>"+tag_inicio+"</option>";      
    }
    datos.forEach(function(elemento){
        var a = elemento.id; 
        var b = elemento.valor;
        cadena+="<option value='"+a+"'>"+b+"</option>";
    }); 
    contenedor.innerHTML = cadena; 
}




var own = function(){
    
    
    
    let carga_contenido = function(indice){ 
        let idx = parseInt(indice,10);
        console.log(idx);
        switch(idx){ 
            case 1: 
                $( "#principalc").load( "plantillas/panel/panel.jsp" ,  function(  ) {
                        $.getScript( "plantillas/panel/js/panel.js", function() {
                            breadcru("SCA","Panel");
                        });
                    });
                break; 
            case 2: 
                $( "#principalc").load( "plantillas/usuario/profile.jsp" ,  function(  ) {
                        $.getScript( "plantillas/usuario/js/profile.js", function() {
                            breadcru("SCA","Perfil de usuario");
                        });
                    });  
                break; 
        }
    };
    
    
    var finaliza_ses = function(){ 
        
        var s = document.querySelector('body').dataset.ses;
        var param = {"1":s };
        $.get("funciones/funciones_esp.jsp", {'i':41, 't':3,'p': JSON.stringify(param)}, function(data){
            sessionStorage.removeItem("chkmsg");
            window.location.replace("../");
            $('#modalfinses').modal('hide'); 
        });
    }; 
    
    let limpiaMenuItems = function(){ 
        let menulauncher = document.querySelectorAll(".itemmnu"); 
        menulauncher.forEach( (ele)=>{ 
            ele.classList.remove("active");
        });
    };
    
    let eventos = function(){ 
        
        
        $("#chkmsgx").off("change").on("change", function(){ 
            unckeckmes = 1;
        });
        
        
        let menulauncher = document.querySelectorAll(".mnuapplaunch"); 
        menulauncher.forEach( (ele)=> { 
            ele.addEventListener('click',function(){
                
                limpiaMenuItems();
                ele.classList.add("active"); 

                let cod = ele.dataset.code; 
                console.log(cod);
                carga_contenido(cod); 
            });
        });
        
        let elx = document.getElementById("panel"); 
        if (elx){
        elx.addEventListener('click', function(){
            carga_contenido(1);
        });
        }

        
        
        let fins = document.getElementById("finses"); 
        if (fins){
        fins.addEventListener('click',function(){
            $('#modalfinses').modal('show'); 
        });
        }
        
        
        let endses = document.getElementById("btnendses"); 
        if (endses){
        endses.addEventListener('click',function(){
                finaliza_ses(); 
        });
        }
        
        let element = document.querySelectorAll('.itemmnu'); 
        element.forEach(function(element){
            element.addEventListener('click', function(){
                
                $.blockUI({ message: null });
                
                limpiaMenuItems();
                element.classList.add("active"); 
                
               let item = element.dataset.item;
                get(4, {"1": item}, function(data){
                    let control = data.data.length;
                    if ( parseInt(control,10)>0){
                        
                    let r1 = data.data[0].ruta1;
                    let r2 = data.data[0].ruta2;
                    let b1 = data.data[0].bread1;
                    let b2 = data.data[0].bread2;
                    breadcru(b1,b2);
                    $( "#principalc").load( r1, function(  ) {
                            $.getScript(r2, function() {
                                setTimeout($.unblockUI, 1000); 
                            });
                    });
                    
                    } else { 
                        setTimeout($.unblockUI, 1000); 
                    }
                    
                });
            });
        });
    };
    
    var minimizeMenu = function() {
          if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
          } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
          }
        }; 
    
    var asigna_variables = function(){ 
        
        //asigna los valores! 
        var _avatar_ = document.querySelector('body').dataset.avatar;
        var _app_ = document.querySelector('body').dataset.app;
        var _perfil_ = document.querySelector('body').dataset.perfil;
        var _id_ = document.querySelector('body').dataset.id;
        var _email_ = document.querySelector('body').dataset.email;
        
        sessionStorage.setItem("idus", _id_);
        sessionStorage.setItem("perfil", _perfil_);
        sessionStorage.setItem("email", _email_);
        
    };
    
    
    var inicializa = function(){ 
        
            
        minimizeMenu(); 
        
        carga_contenido(1);
        
        var btn_menu = document.getElementById('axdf43'); 
        if (btn_menu){ 
            btn_menu.addEventListener('click', function(){ 
                carga_contenido(1);
            });
        }
        
        $(window).resize(function(){
        minimizeMenu();
        });
        
        
        let _chkmsg = sessionStorage.getItem("chkmsg");
        console.log("-->", _chkmsg);
        if (parseInt(_chkmsg,10)===0){ 
            $("chkmsgx").prop( "checked", false );
            $("#modaldisclaimer").modal("show");
            sessionStorage.setItem("chkmsg",1);
        }
        
       
    };
    
    return { 
        init: function() { 
        inicializa();  
        asigna_variables();
        eventos();
        }
    };
    
}(); 

jQuery(document).ready(function () {
    own.init();
});
} catch(error){
    alert(error);
}   

        