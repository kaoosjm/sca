var recov = function(){ 
    
    
    var envia_mail = function(){
        var a = document.getElementById("emailrcvry").value;
        var param = { "1":a};
        $.get("app/funciones/recovery.jsp", {'p':JSON.stringify(param) }, function(data){
            var r = data.respuesta;
            if (parseInt(r,10)===1){ 
                Swal.fire({
                type: 'success',
                text: data.mensaje.mensaje,
                });
            } else if (parseInt(r,10)===2){
                Swal.fire({
                type: 'error',
                text: 'la cuenta de correo no existe en la base de datos',
                });
            }
            document.getElementById('emailrcvry').value = "";
        });
    };
    
   
    
    var valida_form = function(){ 
            var c=0; 
            var e2 = document.getElementById("emailrcvry");
            // reglas de validacion
            
            
//            var response = grecaptcha.getResponse();
            var response = true;
            
            c += valida_mail(e2,"Cuenta de mail no valida");
            c += valida_captcha(document.getElementById('rexcaptcha'),response,"No se ha validado el CAPTCHA");
            
            if (c>0){ 
                setTimeout(function(){
                    oculta("frmrecovery");
                },800); 
            } else{ 
                envia_mail();
            }
        };
    
    
    var inicializa = function() { 

        
        document.querySelector("#frmrecovery #emailrcvry").addEventListener('keyup',function(){
           this.value = NumTextArroba(this.value,50); 
        });
        
        var btn1 = document.getElementById("btn_cancel");
        if (btn1) { 
            btn1.addEventListener('click', function(){
                window.location.href = "index.jsp";
            });
        }
        
        var btn2 = document.getElementById("btn_email");
        if (btn2) { 
            btn2.addEventListener('click', function(){
                valida_form();
            });
        }
        
    };
    
    return {
        init: function () {
            inicializa();
        }
    };
    
    
}(); 

jQuery(document).ready(function () {
    recov.init();
});
