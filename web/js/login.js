try { 
var login = function(){ 
    
    var inicializa = function() {        

        $('#frmloginin input').keypress(function(e) {
            if (e.which === 13) {
                var r  = valida_form();
                if (r===true){
                    window.location.replace("app/index.jsp");
                }
            }
        });
        
        var btn = document.getElementById("btn_login");
        if (btn) { 
            btn.addEventListener('click', function(){
                var r  = valida_form();
                if (r===true){ 
                    window.location.replace("app/index.jsp");
                }
            });
        }        
    };
    
    var constraints = function(){      
        let usr = document.querySelector("#frmloginin #user");
        if (usr){
            usr.addEventListener('keyup',function(){
                this.value = NumTextArroba(this.value,60); 
            });
        }        
      
        let pas = document.querySelector("#frmloginin #password");
        if (pas){
            pas.addEventListener('keyup',function(){
                this.value = NumTextArroba(this.value,12); 
            });
        }
    };
    
    var muestra_msg = function(mensaje) { 
        var msg = document.getElementById("msgerror"); 
        msg.style.display = "block";
        msg.innerHTML = mensaje;
        setInterval(function(){
            msg.style.display = "none";
        },2000);
    }; 
    
    var valida_form = function() {
        var r  = false; 
        var u = document.getElementById("user");  
        var p = document.getElementById("pass"); 
        var c=0; 
       
        c += valida_req(u,"Este valor es requerido");
        c += valida_req(p,"Este valor es requerido");
                    
        if (c>0){ 
            setTimeout(function(){
                oculta('frmloginin');
            },800); 
        } else{      
            $('#btn_login').html('Procesando Solicitud...');
            $('#btn_login').attr('disabled','false');
            // aqui se procesa si la validacion es exitosa. 
            ///////////////////////////////////////////////////////////////////////////////////////////
            var param = {"1" : u.value, "2" : p.value };
            var param1 = {"1" : u.value};
            $.get("app/funciones/fnc.jsp", {'p':JSON.stringify(param1),t:1}, function(data){                
                var t = data.tu;                
                if (t>0){
                    $.get("app/funciones/usuarios.jsp", {'p':JSON.stringify(param),t:t,'p1':JSON.stringify(param1)}, function(data){
                        var t = data.token; 
                        if (t.length>10){
                            document.getElementById("loader").style.display = "block"; 
                                sessionStorage.setItem("chkmsg",0);
                            window.location.href = "app/index.jsp?tkn="+data.token;
                            r = true;
                        } else if (t==-1){
                            Swal.fire({
                                html: data.mg,
                                type: data.type,
                                confirmButtonText: 'Cerrar'
                            });
                            $('#btn_login').html('Entrar');
                            $('#btn_login').removeAttr('disabled');
                        } else {
                            document.getElementById("alerterr").style.display = "block"; 
                            setTimeout(function(){
                                document.getElementById("alerterr").style.display = "none"; 
                            },800);
                            r = false;    
                        }
                    });
                }else {                    
                    Swal.fire({
                        html: data.mg,
                        type: data.type,
                        confirmButtonText: 'Cerrar'
                    });
                    $('#btn_login').html('Entrar');
                    $('#btn_login').removeAttr('disabled');
//                    valida_msg(document.getElementById("user"),"Usuario no registrado"); 
                }
            }).fail(function(jqXHR) {
                if(jqXHR.status===500){      
                    $('#btn_login').html('Entrar');
                    $('#btn_login').removeAttr('disabled');
                    Swal.fire({
                            title: 'Error Interno',
                            text: 'Favor de comunicarse con el administrador del sistema',
                            type: 'warning',
                            confirmButtonText: 'Cerrar'
                    });
                }
            });
            ///////////////////////////////////////////////////////////////////////////////////////////
        }
        return r; 
    }; 
    
    return {
        init: function () {
            inicializa();
            constraints();
        }
    };
}(); 

jQuery(document).ready(function () {
    login.init();
});
} catch(error){ 
    alert(error.message);
}