try { 
    
    var idus = "1"; 
    var forms = "formprove"; 
    var mapa; 
    
var validaciones = function(){ 
    var forms;  
    
    var guarda_reg = function(denominacion,latitud,longitud,id_asentamiento,rfc,emai_contacto,nombre_contacto,apellido_contacto,calle,num_exterior,num_interior,referencias){
        var param = { 
            "a":denominacion,
            "b":latitud,
            "c":longitud,
            "d":latitud,
            "e":longitud,
            "f":id_asentamiento,
            "g":rfc,
            "h":emai_contacto,
            "i":nombre_contacto,
            "j":apellido_contacto,
            "k":calle,
            "l":num_exterior,
            "m":num_interior,
            "n":referencias,
            "o": idus
            };
            
            
            $.get("app/funciones/funciones.jsp", {'i':66, 't':2,'p': JSON.stringify(param) }, function(data){
            if (data.error_msg.length<1){ 
                
                registro.inicia_estado();
                registro.limpia_form_cat();
                registro.deshabilita_form(false);
                
                wizforms.reset();
                
                Swal.fire({
                type: 'success',
                text: 'Registro agregado',
                });
            } else { 
                Swal.fire({
                type: 'error',
                text: data.error_msg,
                }); 
            }
            });
            
            
    }; 
    
    
    
    var valida_form = function(form){ 
        forms = form;
            var c=0; 
            var e2 = document.getElementById("denominacion");
            var e3 = document.getElementById("rfc");
            var e4 = document.getElementById("ncontacto");
            var e5 = document.getElementById("acontacto");
            var e6 = document.getElementById("email");
            var e7 = document.getElementById("edop");
            var e8 = document.getElementById("munip");
            var e9 = document.getElementById("cpp");
            var e10 = document.getElementById("colp");
            var e11 = document.getElementById("calle");
            var e12 = document.getElementById("nexterior");
            var e13 = document.getElementById("ninterior");
            var e14 = document.getElementById("reflocal");
            var e15 = document.getElementById("latlon");

            // reglas de validacion
            c += valida_req(e2,"Este valor es requerido");
            c += valida_rfc(e3,"RFC no valido");
            c += valida_req(e4,"Este valor es requerido");
            c += valida_req(e5,"Este valor es requerido");
            c += valida_mail(e6,"Este valor es requerido");
            c += valida_num(e7,0,"Debe de seleccionar");
            c += valida_num(e8,0,"Debe de seleccionar");
            c += valida_num(e9,0,"Debe de seleccionar");
            c += valida_num(e10,0,"Debe de seleccionar");
            c += valida_req(e11,"Este valor es requerido");
            c += valida_req(e12,"Este valor es requerido");
            c += valida_req(e13,"Este valor es requerido");
            c += valida_req(e14,"Agregue referencias de localizacion");
            c += valida_req(e15,"Debe selecionar un punto geografico");
            
            var response = grecaptcha.getResponse();
            
            c += valida_captcha(document.getElementById('rexcaptcha'),response,"No se ha validado el CAPTCHA");
            
            var ll = e15.value; 
            ll = ll.replace('(','');
            ll = ll.replace(')','');
            var xx = ll.split(',')[0];
            var yy = ll.split(',')[1];


            var param = { "1":e3.value};
            $.get("app/funciones/funciones.jsp", {'i':124, 't':1,'p': JSON.stringify(param) }, function(data){
                let cuan = parseInt(data.data[0].cuantos,10);
                c += valida_val(cuan,e3,'Este valor se encuentra ya en base de datos');
                var param1 = { "1":e6.value};
                $.get("app/funciones/funciones.jsp", {'i':125, 't':1,'p': JSON.stringify(param1) }, function(data1){
                    let cuan1 = parseInt(data1.data[0].cuantos,10);
                    c += valida_val(cuan1,e6,'Este valor se encuentra ya en base de datos');
                    let rx = evalua(c, forms); 
                    if (rx){
                    guarda_reg(e2.value,xx,yy,e10.value,e3.value,e6.value,e4.value,e5.value,e11.value,e12.value,e13.value,e14.value,e15.value);
                    }
                });
            });
    };
    
    var valida_etapa1 = function(form, callback){
        forms = form;
        var respuesta = false; 
        var c=0; 
        var e2 = document.getElementById("denominacion");
        var e3 = document.getElementById("rfc");
        var e4 = document.getElementById("ncontacto");
        var e5 = document.getElementById("acontacto");
        var e6 = document.getElementById("email");
        
        
        c += valida_req(e2,"Este valor es requerido");
        c += valida_rfc(e3,"RFC no valido");
        c += valida_req(e4,"Este valor es requerido");
        c += valida_req(e5,"Este valor es requerido");
        c += valida_mail(e6,"Este valor es requerido");

        
        var param = { "1":e3.value};
        $.get("app/funciones/funciones.jsp", {'i':124, 't':1,'p': JSON.stringify(param) }, function(data){
            let cuan = parseInt(data.data[0].cuantos,10);
            c += valida_val(cuan,e3,'Este valor se encuentra ya en base de datos');

            var param1 = { "1":e6.value};
            $.get("app/funciones/funciones.jsp", {'i':125, 't':1,'p': JSON.stringify(param1) }, function(data1){
                let cuan1 = parseInt(data1.data[0].cuantos,10);
                 c += valida_val(cuan1,e6,'Este valor se encuentra ya en base de datos');
                let resp = evalua(c, forms);
                callback(resp);
            });
        });  
    };
    
    var valida_etapa2 = function(form){
        forms = form;
        var respuesta; 
        var c=0; 
       var e7 = document.getElementById("edop");
       var e8 = document.getElementById("munip");
       var e9 = document.getElementById("cpp");
       var e10 = document.getElementById("colp");
       var e11 = document.getElementById("calle");
       var e12 = document.getElementById("nexterior");
       var e13 = document.getElementById("ninterior");
       var e14 = document.getElementById("reflocal");

       c += valida_num(e7,0,"Debe de seleccionar");
       c += valida_num(e8,0,"Debe de seleccionar");
       c += valida_num(e9,0,"Debe de seleccionar");
       c += valida_num(e10,0,"Debe de seleccionar");
       c += valida_req(e11,"Este valor es requerido");
       c += valida_req(e12,"Este valor es requerido");
       c += valida_req(e13,"Este valor es requerido");
       c += valida_req(e14,"Agregue refernecias de localizacion");
       
       let rf = evalua(c, forms);
       respuesta = rf;
        
        return respuesta;
    };
    
    return { 
        valida1: function(callback){ 
            valida_etapa1('formprove',callback);
        },
        valida2: function(){
            return valida_etapa2('formprove');
        },
        valida3: function(){
            return valida_form('formprove');
        },
        borra_reg(id,callback){
            borra_reg(id,callback);
        }
    };
    
    
    
}(); 
    
    
var wizforms = function(){ 
    var divw; 
    var divbtsig; 
    var nele;
    var indice; 
    
    
    var inicializa = function(elemento){ 
        divw = document.querySelectorAll(elemento + ' section');
        divbtsig = document.querySelector(elemento + ' #wizsiguiente');
        divbtsig.innerHTML = "siguiente";
        nele =  divw.length;
        eventos(elemento);
        indice=0;
        cambiaEstados(indice);
    };  
    
    var displayBloque = function(elemento, estado) { 
        if (estado===true){ 
            elemento.style.display = 'block';
            $(elemento).prev().show(); 
        } else { 
            elemento.style.display = 'none'; 
            $(elemento).prev().hide(); 
        }
    };
    
    var cambiaEstados = function(indice){
        let divw = document.querySelectorAll('#wizard1 section');
        let co=0;
        divw.forEach(function(ele){
            if (co===indice){ 
            displayBloque(ele, true);
            } else { 
            displayBloque(ele, false);
            }
        co++; 
        });
    };
    
    var resetea = function(){
            cambiaEstados(0);
    };
    
    var navegacion = function(direccion){ 
        let val = parseInt(direccion,10); 
        switch (val){ 
            case -1:
                indice--;  
                break; 
            case 1:
                indice++; 
                break; 
        }
        if(indice<0){ 
            indice=0; 
        } else if (indice>nele-1){ 
            indice=nele-1;  
        }
        cambiaEstados(indice);
    }; 
    
    var evento_anterior = function(){ 
        //alert('anterior');
        //navegacion(-1); 
        switch(indice){
            case 0:
                navegacion(-1);
            case 1: 
                navegacion(-1);
            case 2:
                divbtsig.innerHTML = "siguiente";
                navegacion(-1);
            break;
        }    
    }; 
    
    var evento_siguiente = function(){ 
        switch(indice){
            case 0:
                validaciones.valida1(function(rsp){
                    if (rsp){
                    navegacion(1);
                    }
                });
                break;
            case 1: 
                let rt = validaciones.valida2();
                if (rt){
                    divbtsig.innerHTML = "finalizar";
                    navegacion(1);
                }
                break; 
            case 2:
                validaciones.valida3();
            break;
        }
        
    }; 
    
    var eventos = function(elemento){
        var ele = document.querySelector(elemento);
        ele.querySelector("#wizanterior").addEventListener('click',evento_anterior); 
        ele.querySelector("#wizsiguiente").addEventListener('click',evento_siguiente);
    }; 
    
    return { 
        init: function(elemento) { 
                inicializa(elemento); 
        }, 
        reset: function(){
            resetea();
        }
    };
    
    
    
}(); 

    
var registro = function(){
    
    var limpia_form_cat = function(){
        var forma = document.getElementById(forms); 
        forma.reset();
    }; 
    
    
    var inicia_estado = function(){ 
       estado = 0;  
    }; 
    
    var deshabilita_form = function(valor) { 
        var forma = document.getElementById(forms);
        var elements = forma.elements;
        
        if (valor===true){ 
            for (var i = 0, len = elements.length; i < len; ++i) {
                elements[i].readOnly = true;
            }
            document.getElementById("edop").disabled = true;
            document.getElementById("munip").disabled = true;
            document.getElementById("cpp").disabled = true;
            document.getElementById("colp").disabled = true;
            
            document.getElementById("wizanterior").disabled = true;
            document.getElementById("wizsiguiente").disabled = true;
            
            
        } else if (valor===false){ 
            for (var i = 0, len = elements.length; i < len; ++i) {
                elements[i].readOnly = false;
            }
            document.getElementById("edop").disabled = false;
            document.getElementById("munip").disabled = false;
            document.getElementById("cpp").disabled = false;
            document.getElementById("colp").disabled = false;
            
            document.getElementById("wizanterior").disabled = false;
            document.getElementById("wizsiguiente").disabled = false;
            
        }
    };
    
    
    var carga_estados = function(){ 
        $.get("app/funciones/funciones.jsp", {'i':"42", 't': "1" }, function(data){
            combobox(data.data,"edop",true,'...');
        });
    };
    
    var carga_municipios = function(estado){ 
        var param = { "1":estado};
        $.get("app/funciones/funciones.jsp", {'i':"51", 't': "1",'p': JSON.stringify(param) }, function(data){
            combobox(data.data,"munip",true,'...');
        });
    };
    
    var carga_cp = function(municipio){ 
        var param = { "1":municipio};
        $.get("app/funciones/funciones.jsp", {'i':"57", 't': "1",'p': JSON.stringify(param) }, function(data){
            combobox(data.data,"cpp",true,'...');
        });
    };
    
    var carga_cols = function(municipio){ 
        var param = { "1":municipio};
        $.get("app/funciones/funciones.jsp", {'i':"65", 't': "1",'p': JSON.stringify(param) }, function(data){
            combobox(data.data,"colp",true,'...');
        });
    };
    
    
    ////////////////////////////////////////////////////////////////////////////
    
    var carga_estadosc = function(valor,callback){ 
        $.get("app/funciones/funciones.jsp", {'i':"42", 't': "1" }, function(data){
            combobox(data.data,"edop",true,'...');
            document.getElementById("edop").value = valor;
            callback(); 
        });
    };
    
    var carga_municipiosc = function(estado,valor,callback){ 
        var param = { "1":estado};
        $.get("app/funciones/funciones.jsp", {'i':"51", 't': "1",'p': JSON.stringify(param) }, function(data){
            combobox(data.data,"munip",true,'...');
            document.getElementById("munip").value = valor;
            callback();
        });
    };
    
    var carga_cpc = function(municipio,valor,callback){ 
        var param = { "1":municipio};
        $.get("app/funciones/funciones.jsp", {'i':"57", 't': "1",'p': JSON.stringify(param) }, function(data){
            combobox(data.data,"cpp",true,'...');
            document.getElementById("cpp").value = valor;
            callback();
        });
    };
    
    var carga_colsc = function(municipio,valor,callback){ 
        var param = { "1":municipio};
        $.get("app/funciones/funciones.jsp", {'i':"65", 't': "1",'p': JSON.stringify(param) }, function(data){
            combobox(data.data,"colp",true,'...');
            document.getElementById("colp").value = valor;
            callback();
        });
    };
    
    
    ////////////////////////////////////////////////////////////////////////////
    
    var carga_poligono = function(id_colonia) { 
        var centro; 
        
        var param = { "1":id_colonia};
         $.get("app/funciones/funciones.jsp", {'i':63, 't':1,'p': JSON.stringify(param) }, function(data){
            var puntos = data.data;
            if (puntos.length===0){ 
                centro = {"lat": parseFloat('19.4326018'), "lng":parseFloat('-99.1332049')};
                initMap(centro); 
            } else { 
            var x1,x2,y1,y2 = 0;
            var c=0;
            centro; 
            
            puntos.forEach(function(punto){
                
                if (c===0){
                    x1 = parseFloat(punto.x);
                    x2 = parseFloat(punto.x);
                    y1 = parseFloat(punto.y);
                    y2 = parseFloat(punto.y);
                    
                } else { 
                    
                    if (parseFloat(punto.x) < x1){
                        x1 = parseFloat(punto.x); 
                    }
                    if (parseFloat(punto.x) > x2){
                        x2 = parseFloat(punto.x); 
                    }
                    
                    if (parseFloat(punto.y) < y1){
                        y1 = parseFloat(punto.y); 
                    }
                    if (parseFloat(punto.y) > y2){
                        y2 = parseFloat(punto.y); 
                    }
                }
                
            });
                var _x = x1 + ((x2 - x1) / 2);
                var _y = y1 + ((y2 - y1) / 2);
                var centro = { "lat": parseFloat(_y), "lng":parseFloat(_x)};
                initMap(centro); 
                
                
                
            }
                
                
         });
    }; 
    
    
    var initMap = function(centro) {
        
        mapa = new google.maps.Map(document.getElementById('map_loc'), {
          center: centro,
          zoom: 15
        });
        
        var marker = new google.maps.Marker({
            position: centro,
            title:"Hello World!",
            draggable:true
        });
        
        marker.addListener('dragend', function(){
            document.getElementById("latlon").value = marker.getPosition().toString();
        });
        
        marker.addListener('dragend', function(){
        });
        
        marker.setMap(mapa);
    };  
    
    
    var constraints = function(){
      
      document.querySelector("#formprove #denominacion").addEventListener('keyup',function(){
         this.value = NumText(this.value,90); 
      });
      
      document.querySelector("#formprove #ncontacto").addEventListener('keyup',function(){
         this.value = NumText(this.value,90); 
      });
      
      document.querySelector("#formprove #acontacto").addEventListener('keyup',function(){
         this.value = NumText(this.value,90); 
      });
      
      document.querySelector("#formprove #email").addEventListener('keyup',function(){
         this.value = NumTextArroba(this.value,90); 
      });
      
      document.querySelector("#formprove #calle").addEventListener('keyup',function(){
         this.value = NumText(this.value,90); 
      });
      
      document.querySelector("#formprove #nexterior").addEventListener('keyup',function(){
         this.value = NumText(this.value,30); 
      });
      
      document.querySelector("#formprove #nexterior").addEventListener('keyup',function(){
         this.value = NumText(this.value,30); 
      });
      
      document.querySelector("#formprove #ninterior").addEventListener('keyup',function(){
         this.value = NumText(this.value,30); 
      });
      
      document.querySelector("#formprove #reflocal").addEventListener('keyup',function(){
         this.value = NumText(this.value,70); 
      });
      
        
    };
    
    var eventos = function(){
      
      
      
        limpia_form_cat();
        var a = wizforms;
        a.init("#wizard1"); 
      
        /////////////////////////
        
        var cs = document.getElementById("edop"); 
        cs.addEventListener('change', function(){
                carga_municipios(cs.value);
        });
        
        var cpost = document.getElementById("munip"); 
        cpost.addEventListener('change', function(){
                carga_cp(cpost.value);
        });
        
        var cpt = document.getElementById("cpp"); 
        cpt.addEventListener('change', function(){
                carga_cols(cpt.value);
        });
        
        var copt = document.getElementById("colp"); 
        copt.addEventListener('change', function(){
        carga_poligono(copt.value);
        });
        
    };
    
    return { 
        init: function() { 
        constraints();
        var a = wizforms;
        a.init("#wizard1"); 
        carga_estados(); 
        eventos();
        deshabilita_form(false);
        document.getElementById("formdiv").style.display = "block";
        
        },
        inicia_estado(){
            inicia_estado();
        },
        limpia_form_cat(){
            limpia_form_cat();
        },
        deshabilita_form(valor){
            deshabilita_form(valor);
        }
        
        
    };
}(); 

jQuery(document).ready(function () {
    registro.init();
});

} catch(error){
    console.log(error.message);
    alert(error.message);
}    

        