<%-- 
    Document   : index
    Created on : 1/09/2019, 08:28:41 PM
    Author     : daniel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>DGTI</title>
    <!-- Bracket CSS -->
    <link href="lib/sweetalert/sweetalert2.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bracket.css">
    <link rel="stylesheet" href="css/own.css">
  </head>

  <body>

    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">

      <div style="background-image: url('img/texturamx.png'); background-repeat: no-repeat" class="login-wrapper wd-300 wd-xs-400 pd-25 pd-xs-40 bg-white rounded shadow-base">
         
       <div class="signin-logo tx-center tx-28 tx-bold text-white"><span class="text-white">|</span>SIININ <span class="tx-info"></span> <span class="tx-normal">|</span></div>
        <div class="tx-center mg-b-40 text-white">Sistema Integral de Información</div>
        <div class="tx-center mg-b-40 text-dark">Se enviará a su correo electronico las instrucciones para reestablecer su contraseña</div>
        
       <div class="form-layout form-layout-4"> 
        <form id="frmrecovery">
            
            
            <div class="row">
            <!--<label class="col-sm-4 form-control-label">email<span class="tx-danger">*</span></label>-->
            <input class="form-control" id='emailrcvry' type="text" name="emailrcvry" placeholder="email">
            <ul class="hide-error parsley-errors-list filled">
            <li class="parsley-required">Este valor es requerido</li>
            </ul>
            </div>
            <!--
            <div style="margin-top: 20px" class="row">
                <div id="rexcaptcha" class="g-recaptcha" data-sitekey="6Lf_4MsUAAAAAFbbIjZaY-JEr3b26Jl1R9BoOrHw"></div>
                <ul class="hide-error parsley-errors-list filled">
                <li class="parsley-required">Este valor es requerido</li>
                </ul>
            </div>
            -->
            
            <br/>
            <button id="btn_email" type="button" class="btn btn-info btn-block">Enviar</button>
            <button id="btn_cancel" type="button" class="btn btn-danger btn-block">Regresar a página principal</button>
        </form>
       </div>    

        

        <!--<div class="form-group tx-12">Se enviará a su correo electronico las instrucciones para reestablecer su contraseña</div>-->
        
        

      </div><!-- login-wrapper -->
    </div><!-- d-flex -->

    <script src="lib/jquery/jquery.js"></script>
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="lib/sweetalert/sweetalert2.all.min.js"></script>
    <script src="js/own.js"></script>
    <script src="js/recovery.js"></script>
  </body>
</html>

